3 sum
========
concept:
--------
- sort; check and remove impossbile cases
- iterate i,j,tail and adjust j and tail according to sum (like two-sum)
- skip dup to save time
- TLE!!


others' solution (main2)
-------
- use set to hold unpaired (a,b) for future c match, save time
- gurantee not miss: if there is (a+b+c=0) is matched, then before c is iterated, (a,b) pair must be in the set; and if there is matche, c need not be in the set, since there must no other value d to make a+c+d=0.
- lastc and lasta element to skip dup ??