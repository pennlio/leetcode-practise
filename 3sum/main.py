# 3 sum
# a O(n^2) solution but not accepted by OJ
class Solution:
     # @return a list of lists of length 3, [[val1,val2,val3]]
    def threeSum(self, num): 
        n = len(num)
        if n < 3: return []
        lists = []
        ht = {} # hash table
        num.sort()        
        lasta = None
        lastb = None
        for i in range(n):
            if num[i] == lasta:
                continue
            else:
                lasta = num[i]
            for j in range(i, n):
                if i != j:
                    if num[j] == lastb:
                        continue
                    lastb = num[j]
                    tl = n-1
                    while j<tl: # two pointers do not meet
                        tsum = num[i] + num[j] + num[tl]
                        if tsum == 0:
                            if (num[i],num[j],num[tl]) not in ht:
                                olist = [num[i],num[j],num[tl]]
                                olist.sort()
                                lists.append(olist)
                                ht[(num[i],num[j],num[tl])] = 1
                            # ajust pointers
                            tl -= 1 
                            j += 1
                        elif tsum > 0:
                            tl -= 1
                        else:
                            j += 1
        return lists

    # def buildHashset(self, num):
    #     hasht = {}
    #     for n in num:
    #         if n not in hasht:
    #             hasht[n] = 1
    #         else:
    #             hasht[n] += 1
    #     return hasht

if __name__ == '__main__':
    num = [-10,-7,-3,-9,-8,-9,-5,6,0,6,4,-15,-12,3,-12,-10,-5,-5,2,-4,13,8,-9,6,-11,11,3,-13,-3,14,-9,2,14,-5,8,-9,-7,-12,5,1,2,-6,1,5,4,-4,3,7,-2,12,10,-3,6,-14,-12,10,12,7,12,-14,-2,11,4,-10,13,-11,-4,-8,-15,-14,8,-6,-12,-14,6,7,-3,-14,-1,11,14,-6,-15,5,-13,-12,0,14,2,-11,-14,8,-15,-3,13,14,-7,-14,13,-15,10,-2,-14,13]
    # num = [4,0,2,3,-1]
    # num = [-4,-1,-1,0,2,9]
    # num = [-1,-1,0,2,1,-1,2]
    sl = Solution()
    result = sl.threeSum(num)
    print result
    # print hashset[2], hashset[-1]