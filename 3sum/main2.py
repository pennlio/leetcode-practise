class Solution:
    # @return a list of lists of length 3, [[val1,val2,val3]]
    def threeSum(self, num):
        returnlists = []
        #let num be sorted, ....
        num.sort()
        lasta = None
        #let a = num[i], this solution is to find (b + c) = -a
        for i in range(len(num)):
            #use lasta to avoid redundant a
            if num[i] == lasta:
                continue
            #use lastc to avoid redundant c
            lastc = None
            lasta = num[i]
            #this is -a
            nega = 0 - num[i]
            #make a set to store the b, then to find c
            newSet = set()
            #use range(i,..) to avoid (a, b)/(b, a) redundancies.
            for j in range(i, len(num)):
                #skip self
                if j != i:
                    #check if  -a - c is in set
                    theval = nega - num[j]
                    #not in and no repeating, get (a, b, c).
                    if theval in newSet and lastc != num[j]:
                        thislist = [theval, num[i], num[j]]
                        lastc = num[j]
                        thislist.sort()
                        returnlists.append(thislist)
                    #put b in set
                    else:
                        newSet.add(num[j])
        return returnlists

if __name__ == '__main__':
    num = [-10,-7,-3,-9,-8,-9,-5,6,0,6,4,-15,-12,3,-12,-10,-5,-5,2,-4,13,8,-9,6,-11,11,3,-13,-3,14,-9,2,14,-5,8,-9,-7,-12,5,1,2,-6,1,5,4,-4,3,7,-2,12,10,-3,6,-14,-12,10,12,7,12,-14,-2,11,4,-10,13,-11,-4,-8,-15,-14,8,-6,-12,-14,6,7,-3,-14,-1,11,14,-6,-15,5,-13,-12,0,14,2,-11,-14,8,-15,-3,13,14,-7,-14,13,-15,10,-2,-14,13]
    # num = [4,0,2,3,-1]
    # num = [-4,-1,-1,0,2,9]
    # num = [-1,-1,0,2,1]
    sl = Solution()
    result = sl.threeSum(num)
    print result