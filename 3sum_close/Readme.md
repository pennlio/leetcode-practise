3 Sum closest
============
concept:
----------
- convert to `two sum` with one dangling elements
- total time O(n^2)
- guarantee cover all cases