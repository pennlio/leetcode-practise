# 3 sum close
''' 12/23/2014'''

class Solution:
    # @return an integer
    def threeSumClosest(self, num, target):
        n = len(num)
        if n<3: return None
        num.sort()
        hashset = self.buildHashset(num)
        closest = None
        cp = num[0]-1 # init closest_prev and closest_next
        cn = num[-1]+1
        maxn = max(num)
        minn = min(num)
        # case 1 : (-n, t, n)
        i = 0
        while (num[i] < 0):
            if -num[i] in hashset:
                if target in hashset:
                    return target
                else:
                    prev = target-1
                    while(prev not in hashset):
                        prev -= 1
                        if prev < minn:
                            prev = minn
                            break
                    if prev > cp:
                        cp = prev
                    next = target+1
                    while(next not in hashset):
                        next += 1
                        if next > maxn:
                            next = maxn
                            break
                    if next < cn:
                        cn = next
                    if (cn-target < target - cp):
                        closest = cn
                    else:
                        closest = cp
            i += 1

        '''case 2'''    
        # ....


        return closest

    def buildHashset(self, num):
        hasht = {}
        for n in num:
            if n not in hasht:
                hasht[n] = 1
            else:
                hasht[n] += 1
        return hasht

if __name__ == '__main__':
    sl = Solution()
    # num = [-1,1,0,4,5,2,3]
    num = [-1, 2, 1, -4]
    target = 3
    print sl.threeSumClosest(num,target)


        