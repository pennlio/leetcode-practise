# 3 sum close
''' 12/23/2014'''
# learn from other's code
class Solution:
    # @return an integer
    def threeSumClosest(self, num, target):
        n = len(num)
        if n < 3 : return None
        num.sort()
        ans = num[0] + num [1] + num[2]  # basic case
        for i in range(n):
            j = i+1
            k = n-1
            while(j<k):
                tsum = num[i]+num[j]+num[k]
                if tsum == target:
                    ans = tsum
                    return ans
                if abs(tsum - target) < abs(ans - target): # record current value
                    ans = tsum
                if tsum < target:
                    j += 1
                else:
                    k -= 1
        return ans


if __name__ == '__main__':
    sl = Solution()
    num = [-1,1,0,4,5,2,3]
    # num = [-1, 2, 1, -4]
    target = 3
    print sl.threeSumClosest(num,target)