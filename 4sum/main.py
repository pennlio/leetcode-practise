class Solution:
    # @return a list of lists of length 4, [[val1,val2,val3,val4]]
    def fourSum(self, num, target):
        n = len(num)
        if n < 4: return []
        lists = []
        num.sort()
        lasta = None
        lastb = None
        lastc = None
        for i in range(n):
            if num[i] == lasta:
                continue
            lasta = num[i]
            for j in range(i,n):
                if j == i or num[j]==lastb:
                    continue   
                lastb = num[j]
                newSet = set()
                for k in range(j,n):
                    if k == j or num[k]==lastc:
                        continue
                    lastc = num[k]
                    theval = target -lasta - lastb - lastc
                    if theval in newSet:
                        olist = [num[i],num[j],num[k],theval]
                        olist.sort()
                        lists.append(olist)
                    else:
                        newSet.add(num[k])
        return lists

if __name__ == '__main__':
    sl = Solution()
    num = [1,0,-1,0,-2,2]
    target = 0
    print sl.fourSum(num,target)



