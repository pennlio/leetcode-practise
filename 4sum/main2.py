class Solution():
    def fourSum(self, num, target):
        n = len(num)
        if n < 4: return []
        _lists = []
        _basket = {}
        num.sort()
        for i in range(n):
            for j in range(i+1, n):
                _ijsum = num[i] + num[j]
                theval = target - _ijsum
                if theval in _basket:  # if there is match
                    for [a,b] in _basket[theval]:
                        if b<i and [num[a],num[b],num[i],num[j]] not in _lists: # must have a<b<i<j
                            _lists.append([num[a],num[b],num[i],num[j]])
                ## here still need to store _ijsum and [num[i], num[j]]
                if _ijsum not in _basket:
                    _basket[_ijsum] = []
                _basket[_ijsum].append([i,j])
        
        return _lists


if __name__ == '__main__':
    sl = Solution()
    num = [-3,-2,-1,0,0,1,2,3]
    target = 0
    print sl.fourSum(num,target)

