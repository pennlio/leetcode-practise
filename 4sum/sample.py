class Solution:
    def fourSum(self, num, target):
        _result = []
        _dic = {}

        # rank for the non-descending-order of the result
        num.sort()

        for i in range(len(num)):
            for j in range(i+1, len(num)):
                _ijsum = num[i] + num[j]

                #judge whether pairs exist in hash structure
                if target - _ijsum in _dic:
                    for (a,b) in _dic[target - _ijsum]:
                        # a must be smaller than i, so b<i guarantee (a,b,i,j)
                        # follow the non-descending order
                        if b<i and [num[a], num[b],num[i], num[j]] not in _result:
                            _result.append([num[a],num[b],num[i],num[j]])

                if _ijsum in _dic:
                    _dic[_ijsum].append((i,j))
                else:
                    _tem = []
                    _tem.append((i,j))
                    _dic[_ijsum] = _tem
        return _result 

if __name__ == '__main__':
    sl = Solution()
    num = [1,0,-1,0,-2,2]
    target = 0
    print sl.fourSum(num,target)