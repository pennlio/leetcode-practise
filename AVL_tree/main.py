class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None
        self.p = None
        self.h= 0 #height of the tree
class Solution:
    # @param root, a tree node
    # @return a list of integers
    # g_output_node_list
    g_output_node_list = []   # save output nodes

    def preorderTraversal(self, root):
        if root == None:
            return self.__class__.g_output_node_list
            # return []
        else:
            self.__class__.g_output_node_list.append(root.val)
            print root.h
            # print root.val
            self.preorderTraversal(root.left);
            self.preorderTraversal(root.right);
            return self.__class__.g_output_node_list
    
    def addNode(self, root_node, value): 
        if root_node==None:
            newnode = TreeNode(value)
            root_node = newnode
            return newnode  # change from root_node

        if value < root_node.val:
            newnode = self.addNode(root_node.left, value)
            root_node.left = newnode  # link with parent!
            newnode.p = root_node
        else:
            newnode = self.addNode(root_node.right,value)
            root_node.right = newnode
            newnode.p = root_node
        root_node.h = root_node.h + 1
        return root_node

    def DeleteNode(self, root_node, value):
        if root_node == None:
            print "Empty tree, no such value"
            return root_node
        if root_node.val == value:
            if root_node.left == None and root_node.right == None:
                root_node = None
            elif root_node.left == None and root_node.right != None:
                root_node = root_node.right
            elif root_node.left != None and root_node.right == None:
                root_node = root_node.left
            else:
                successor_node = self.FindSuccessor(root_node)
                root_node.val = successor_node.val
                self.DeleteNode(successor_node, successor_node.val)

        elif root_node.val > value:
            newnode = self.DeleteNode(root_node.left, value)
            root_node.left = newnode #likn updated to root_node
        else:
            newnode = self.DeleteNode(root_node.right, value)
            root_node.right = newnode #likn updated to root_node
        return root_node

    def FindSuccessor(self, origin_node):
        if origin_node.right == None: 
            iternode = origin_node.p
            while iternode != iternode.p.left:
                iternode = iternode.p
        else:
            iternode = origin_node.right
            while iternode.left != None :
                iternode = iternode.left
        return iternode

    def RightRotate(self, origin_node):
        x = origin_node.left
        origin_node.left = x.right
        try:
            origin_node.left.p = origin_node  #if it's None
        except Exception, e:
            print "leaf is null"
        else:
            pass
        x.right = origin_node
        x.p = origin_node.p
        try :
            if origin_node == origin_node.p.left:
                origin_node.p.left = x
            else:
                origin_node.p.right = x
        except Exception, e:
            print "origin_node is root, so root changed"
            root = x
        else:
            root = None
        origin_node.p = x
        x.right = origin_node
        return root # the root node is changed

    def LeftRotate(self, origin_node):
        y = origin_node.right
        origin_node.right = y.left
        try:
            origin_node.right.p = origin_node
        except AttributeError, e:
            print "leaf is null"
        else:
            pass
        y.left = origin_node
        y.p = origin_node.p 
        try:
            if origin_node.p.left == origin_node:
                origin_node.p.left = y
            else:
                origin_node.p.right = y #!!!
        except AttributeError, e:
            print "origin_node is root, so root changed"
            root = y  #change root to y
        else:
            root = None # does not change the root
        origin_node.p = y
        y.left = origin_node
        return root


if __name__ == "__main__":
    array = [3,1,5,7,9]
    # array = [7,9,5,3,1]
    sl = Solution()
    for i in array:
        try:
            root_node = sl.addNode(root_node, i) 
        except Exception, e:
            root_node = TreeNode(i)
        else:
            pass
    result = sl.preorderTraversal(root_node)
    print result
    new_root = sl.LeftRotate(root_node)
    # new_root = sl.RightRotate(root_node)
    if new_root != None:
        root_node = new_root
    sl.__class__.g_output_node_list = []
    result = sl.preorderTraversal(root_node)
    print result