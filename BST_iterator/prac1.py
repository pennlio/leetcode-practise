# Definition for a  binary tree node
# from Tree import Tree, Node

class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class BSTIterator:
    # @param root, a binary search tree's root node
    def __init__(self, root):
        # if root == None: return ## never define conditional return in __init__ function
        p, self.stack = root, []
        while p:
            self.stack.append(p)
            p = p.left
        
    # @return a boolean, whether we have a next smallest number
    def hasNext(self):
        return True if self.stack else False

    # @return an integer, the next smallest number
    def next(self):
        node = self.stack.pop() #last one
        if node.right: # if it has right
            p = node.right
            while p:
                self.stack.append(p)
                p = p.left
        return node.val


# Your BSTIterator will be called like this:
if __name__ == '__main__':
    root = TreeNode(4)
    root.left = TreeNode(3)
    root.right = TreeNode(5)
    # root = None
    i, v = BSTIterator(root), []
    while i.hasNext(): v.append(i.next())
    print v
    # print map(lambda x: x.val, v)
