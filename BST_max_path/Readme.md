concept:
--------
- DP, so discuss cases:
 - what consist a path? two sub-branches across a root node
 - how to get the max of left or right branch
 - compare three cases: (sum-of-left-branch+sum-of-right-branch + root, root as left child, root as right child)



imple:
-------
- should return None instead of 0 if any child node does not exist
- when calculat branc-max: compare (0, node.val, node.val+leftmax, node.val+rightmax) as the cases
- O(n) space and time??


summary
--------
- when you feel the problem is too difficult to think, you are on a wrong way.
