# Definition for a  binary tree node
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    # @param root, a tree node
    # @return an integer
    def __init__(self):
        self._hash ={}
        self._maxSum = - float('inf')

    def maxPathSum(self,root):
        if not root: return None
        self.calMaxPathSum(root)
        return self._maxSum

    def calMaxPathSum(self, root):
        if not root:
            return None # None can compare with num; use None not 0 ; if there is no subtree, it should return no result not 0; 
        curSum = max(self.findMaxSinglePath(root.left) + self.findMaxSinglePath(root.right) + root.val, self.calMaxPathSum(root.left), self.calMaxPathSum(root.right))
        self._maxSum = max(self._maxSum, curSum)

    def findMaxSinglePath(self, root):
        # find max single path down from root:
        if not root: return 0 # since it must have parent node, so it can return 0
        if root not in self._hash:
            self._hash[root] = max(0, root.val, root.val+self.findMaxSinglePath(root.left), root.val + self.findMaxSinglePath(root.right))
        return self._hash[root]

if __name__ == '__main__':
    root = TreeNode(-3)
    # root.left = TreeNode(-3)
    root.left = TreeNode(2)
    root.right = TreeNode(-1)
    root.right.left = TreeNode(9)
    root.right.right = TreeNode(-7)
    sl = Solution()
    print sl.maxPathSum(root)




        