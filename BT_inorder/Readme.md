concept:
--------
- push; checkleft; print; pop; checkright (as left)
- append parent in stack; but in pre-order append right-sibling in stack



imple:
------
- push parents in stack until the deepest left leaf, then output leftest leaf
- pop parent and run above to its right branch iteratively
