# Definition for a  binary tree node
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    # @param root, a tree node
    # @return a list of integers
    def inorderTraversal(self, root):
        if not root: return []
        p = root
        stack = []
        result = []
        while p is not None: # push left branch into stack, including root
            stack.append(p)
            p = p.left

        while len(stack)>0:
            p = stack.pop()
            result.append(p.val) # append value from stack
            p = p.right # must have p.right, but maybe None
            while p is not None: # if p == None, continue with upper level
                stack.append(p)
                p = p.left
        return result

if __name__ == '__main__':
    sl = Solution()
    # root = None
    # root = TreeNode(2)
    root = TreeNode(9)
    root.left = TreeNode(5)
    # root.right = TreeNode(3)
    root.right = TreeNode(10)
    root.left.left = TreeNode(4)
    root.left.right = TreeNode(7)
    root.left.right.left = TreeNode(6)
    root.right.right = TreeNode(11)
    print sl.inorderTraversal(root)




        