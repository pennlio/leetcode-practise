# Definition for a  binary tree node
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    # @param root, a tree node
    # @return a list of lists of integers
    def levelOrder(self, root):
        result = []
        if not root:
            return result
        stack1 = [] # current 
        stack2 = [] # buffer for nodes
        levelValue = []
        stack1.append(root)
        result.append([root.val])
        while len(stack1) > 0:
            p = stack1.pop()
            if p.left:
                stack2.append(p.left)
                levelValue.append(p.left.val)
            if p.right:
                stack2.append(p.right)
                levelValue.append(p.right.val)
            if len(stack1) == 0: # this level is over
                stack1 = stack2
                stack2 = []
                stack1.reverse()  # use it as a queue
                result.append(levelValue)
                levelValue = [] # reset value buffer
        return result[:-1]

if __name__ == '__main__':
    sl = Solution()
    root = None
    root = TreeNode(5)
    root.left = TreeNode(3)
    root.left.left = TreeNode(37)
    # root.left.right = TreeNode(4)
    


    root.right = TreeNode(6)
    # root.right.left = TreeNode(9)
    root.right.right = TreeNode(8)

    print sl.levelOrder(root)

