class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    # @param root, a tree node
    # @return a list of lists of integers
    '''implemented by queue'''
    def levelOrder(self, root):
        result = []
        if not root:
            return result
        # queue1 = [] # current 
        import Queue
        queue1 = Queue.Queue()
        queue2 = Queue.Queue() # buffer for nodes
        levelValue = []
        queue1.put(root)
        result.append([root.val])
        while not queue1.empty():
            p = queue1.get() # FIFO
            if p.left:
                queue2.put(p.left)
                levelValue.append(p.left.val)
            if p.right:
                queue2.put(p.right)
                levelValue.append(p.right.val)
            if queue1.empty(): # this level is over
                queue1 = queue2
                queue2 = Queue.Queue() # reset queue
                result.append(levelValue)
                levelValue = [] # reset value buffer
        return result[:-1]

if __name__ == '__main__':
    sl = Solution()
    root = None
    root = TreeNode(5)
    root.left = TreeNode(3)
    root.left.left = TreeNode(37)
    root.left.right = TreeNode(4)
    


    root.right = TreeNode(6)
    # root.right.left = TreeNode(9)
    root.right.right = TreeNode(8)

    print sl.levelOrder(root)