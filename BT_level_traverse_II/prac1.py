# Definition for a  binary tree node
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    # @param root, a tree node
    # @return a list of lists of integers
    def levelOrderBottom(self, root):
        if not root:
            return []
        stack1 = []
        stack2 = [] # as queue
        result = []
        stack1.append(root)
        levelValue = []
        while len(stack1)>0:
            p = stack1.pop()
            levelValue.append(p.val)
            if p.left:
                stack2.append(p.left)
            if p.right:    
                stack2.append(p.right)
            if len(stack1) == 0:
                stack1 = stack2
                result.append(levelValue)
                levelValue = []
                stack2 = []
                stack1.reverse()
        result.reverse()
        return result

if __name__ == '__main__':
    sl = Solution()
    root = TreeNode(1)
    root.left = TreeNode(2)
    root.right = TreeNode(3)
    # root.right.left = TreeNode(15)
    root.left.left = TreeNode(4)
    root.right.right = TreeNode(5)
    print sl.levelOrderBottom(root)