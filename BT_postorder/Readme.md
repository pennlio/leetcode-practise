concept:
---------
- post-order: l-r-t; pre-order(l): t-l-r; pre-order(r): t-r-l; so post-order is reverse of pre-order(r)
- implement pre-order(r) and then reverse

imple
--------
- practise traditional two-stack method
- use `stack.top()` to check top element without poping it out 
