# Definition for a  binary tree node
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    # @param root, a tree node
    # @return a list of integers
    def postorderTraversal(self, root):
        result = []
        if not root:
            return result
        # reverse method: post-order = l - r - t; reverse: t - r - l
        stack = []
        p = root
        while p!= None: # reach to right-most
            result.append(p.val)
            stack.append(p)
            p = p.right
        while len(stack)>0:
            p = stack.pop()
            p = p.left
            while p!=None:
                # if p.left != None:
                result.append(p.val)
                stack.append(p)
                p = p.right
        result.reverse()        
        return result



if __name__ == '__main__':
    sl = Solution()
    # root = None
    # root = TreeNode(2)
    root = TreeNode(9)
    root.left = TreeNode(5)
    # root.right = TreeNode(3)
    # root.right = TreeNode(10)
    # root.left.left = TreeNode(4)
    # root.left.right = TreeNode(7)
    # root.left.right.left = TreeNode(6)
    # root.right.right = TreeNode(11)
    print sl.postorderTraversal(root)

