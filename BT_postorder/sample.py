def postorderTraversal(self, root):
        node_stack = stack()
        node_stack.push(root)
        cur = root
        pre = None
        r_list = []
        while cur is not None:
            cur = node_stack.top()
            if cur is None :
                break
            if (cur.left is None and cur.right is None) \
                or (pre == cur.right and cur.right is not None) \
                or (pre == cur.left and cur.left is not None and cur.right is None):
                r_list.append(cur.val)
                node_stack.pop()
            else:
                if cur.right is not None:
                    node_stack.push(cur.right)
                if cur.left is not None:
                    node_stack.push(cur.left)

            pre = cur
        return r_list