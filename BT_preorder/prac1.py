# Definition for a  binary tree node
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    # @param root, a tree node
    # @return a list of integers
    def preorderTraversal(self, root):
        result = []
        if not root:
            return result
        p = root
        rc = [] # store right children using stack!!!
        # go deep to the left
        while p != None:
            result.append(p.val)
            if p.right != None:
                rc.append(p.right)
            p = p.left
        # deal with the right
        while len(rc)>0:
            p = rc.pop()
            while p!=None:
                result.append(p.val)
                if p.right != None:
                    rc.append(p.right)
                p = p.left
        return result

if __name__ == '__main__':
    sl = Solution()
    root = None
    # root = TreeNode(1)
    # root.left = TreeNode(2)
    # root.left.left = TreeNode(3)
    # root.right = TreeNode(4)
    # root.right.left = TreeNode(5)
    # root.right.left.left = TreeNode(6)
    # root.right.right = TreeNode(7)
    
    # root.left.right = TreeNode(5)
    print sl.preorderTraversal(root)



        