# Definition for a  binary tree node
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

from collections import deque
class Solution:
    # @param root, a tree node
    # @return a list of lists of integers
    def zigzagLevelOrder(self, root):
        if not root: return []
        cnt = 0
        result = []
        queue = deque([root])
        buf = []
        while queue: # BFS
            x = queue.popleft() # get the latest element
            if x == '#' : # tag for level end
                if buf: # buf has elements
                    if cnt%2==0: 
                        result.append(buf)
                    else:
                        result.append(buf[::-1]) # reverse
                    buf = [] # clear buf
                cnt += 1
            else:
                if not buf: # buf is empty
                    queue.append('#') # label end of last level
                buf.append(x.val)
                if x.left: 
                    queue.append(x.left)
                if x.right:
                    queue.append(x.right)
        return result

if __name__ == '__main__':
    sl = Solution()
    root = TreeNode(1)
    root.left = TreeNode(2)
    root.right = TreeNode(3)
    root.left.left = TreeNode(4)
    root.left.right = TreeNode(5)
    print sl.zigzagLevelOrder(root)




        