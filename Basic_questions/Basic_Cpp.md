Basics for Cpp
===========

##Declare vs Define in C and C++

- declare:
    - compiler know it exists; but not for detail right now
    - particular useful in working in multiple files

- define:
 - defining something means providing all of the necessary information to create that thing in its entirety

- 'extern int': declare a int outside the scope of current file; 
> Using extern to declare a global variable is pretty much the same thing as using a function declaration to declare a function in a header file

## inheritance

- what to inherit from parents:
 - 'public', 'protected', 'private'
- how to inherit from parents:
 - 'public', 'protected', 'private'

- inheritance table

| C-class Inheritance\P-class Access Level  | public        | protected     | private   |
|------------------------------------------ |-----------    |-----------    |---------  |
| public                                    | public        | protected     | -         |
| protected                                 | protected     | protected     | -         |
| private                                   | private       | private       | -         |

>  - `private` can never be inherited nor accessed from outside
   - `protected` can be inherited but not accessed from outside



- types of inheritance:
 - single inheritanc
 - multilevel
 - multiple inheritance:

> private data is never inherited


### `contructor` and `detructor` in inheritance:
 
constructor calling:
- if B is inherited from A, so when constructing B, A's constructor will be first called; then B's
- always define a default constructor when creating new class

destructor calling: reverse case

### Virtual Base class:

- deal with multiple inheritance from more than one base classes that share some common interface (like A->B, A-->C, B-->D, C-->D)
- use virtual to tell compiler that there is only one needed

### `using` keywords: 
- help to inherite method or vars as in public section by private way
- equals to conver the domain to as such


## dynamic memory management and pointers:

- `new` variable:  return the address
- pointers can reference such new objects
- garbarge: objects that is allocated but lost in the memory stack
- `delete` to avoid garbage: delete the contente the pointer is pointing to and the pointer is not available (becomes daggling ptrs)
- garbage is esp. easy created after calling a function

## Override and overload

- Overload: a function interface is 'over-loaded' with more 
than one methods
 - same name function but different parameter types or numbers

- Override: child redefine methods that have been defined in parent, 'write-above' so it is override

## Thread design:

###multiprocess vs multi-thread

- multiprocess :
 - higher overhead
 - interprocess communication
 - distributed system

- multi-thread :
 - share memory
 - better performance
 - difficult to implement

- join() and detach():
 - detach and join is only once

## OOD design Concepts

- objects:
 - data & logic

- class: blueprint but not house
 - name;
 - attr;
 - behaviour

- Abstraction: focus on things essenital to current demand and current application
 
- polymophisim:
 - operator polymophisim: '+'
 - compile poly: function overload
 - run-time poly: virtual function 

- Encapsulation:
 - bundling related contents together
 - restrict access to outside part of applications



## OOD Programming Language

- abstract class: 
 - an template for a series of classes
 - only to be extended, no initiated
 - repre. a generalization and provides some funcitonality
 - may contain abstract method, static methods or vars

 > `static` define the methods and vars for the class


- Interface: can only contain `static` and `abstract` methods and vars


### abstract method and virtual function:

- An abstract function can have no functionality. You're basically saying, any child class MUST give their own version of this method, however it's too general to even try to implement in the parent class.

- A virtual function, is basically saying look, here's the functionality that may or may not be good enough for the child class. So if it is good enough, use this method, if not, then override me, and provide your own functionality.


## design patterns

