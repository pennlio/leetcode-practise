class Solution:
    # @param num, a list of integer
    # @return a list of lists of integers
    def permute(self, num):
        if not num:
            return []
        n = len(num)
        if n == 1:
            return [num]
        if n == 2:
            b = list(num)
            b.reverse()
            return [num, b]
        else:
            result = []
            for i in range(n):
                # a = num[:i] + num[i+1:]
                for x in self.permute(num[:i]+num[i+1:]):
                    x.append(num[i])
                    result.append(x)
            return result

if __name__ == '__main__':
    sl = Solution()
    # num = [1, 2,3,4]
    # num = [1]
    num = [1,2,3]
    # num = [1,2]
    print sl.permute(num)