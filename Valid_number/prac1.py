class Solution:
    # @param s, a string
    # @return a boolean
    '''stopped at trailing too many cases'''
    def isNumber(self, s):
        if not s: return False
        i, n = 0, len(s)
        while i < n and s[i]==' ':
            i += 1
        while n>0 and s[n-1]==' ':
            n -= 1
        s = s[i:n]
        # s = s.replace(" ", "")  #remove spaces, not in space
        # if not s: return False
        stack1 = []
        stack2 = []
        actStack = stack1
        eindx = 0
        for x in s:
            if x=='e':
                if eindx == 1:
                    return False
                else:
                    actStack = stack2
                    eindx = 1 # start part after e
                    continue
            actStack.append(x)
        if actStack == stack1:
            return self.verifyNum(stack1)
        else:
            return self.verifyNum(stack1) and self.verifyNum(stack2)

    def verifyNum(self, s):
        num = ['0','1','2','3','4','5','6','7','8','9']
        if not s:
            return False
        pointind = 0
        validp = 0 # verifyNum point
        numindx = 0
        for i in range(len(s)):
            if s[i] not in num + ['-','.','+']:
                return False
            if s[i] in ['-','+']:
                if i != 0: # - can only at begin
                    return False
            if s[i] == '.':
                if pointind == 1:
                    return False
                else:
                    pointind = 1
                    if i > 0 and s[i-1] in num:
                        validp = 1
            if s[i] in num:
                numindx = 1
                if i>0:
                    if s[i-1] == '.':
                        validp = 1
        if not numindx:
            return False
        elif (validp^pointind):  # ^ == XOR
            return False
        else:
            return True
if __name__ == '__main__':
    sl = Solution()
    # s = 'a 0'
    # s = '.4e -3'
    # s = '-3.22222   1'
    # s = '32e.'
    # s = 'x'
    # s = '2e-'
    s = '.'
    s = ' 0.0001e-.4 '
    s = '23e2e'
    # s = '0e'
    # s = '11'
    print sl.isNumber(s)
