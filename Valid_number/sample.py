class Solution:
    def isNumber(self, s):
        if s == None or len(s) == 0:
            return False
        n, i = len(s), 0
        while i < n and s[i] == " ":
            i += 1
        if i < n and (s[i] == '-' or s[i] == '+'):
            i += 1
        while n > 0 and s[n-1] == " ":
            n -= 1
        epos = i
        while epos < n and s[epos] != 'e' and s[epos] != 'E':
            epos += 1
        npos = epos + 1
        if npos < n and (s[npos] == '-' or s[npos] == '+'):
            npos += 1
        if epos == 0 or epos < n and not s[npos:n].isdigit():
            return False
        t = s[i:epos]
        n = len(t)
        ppos = 0
        while ppos < n and t[ppos] != '.':
            ppos += 1
        if not t[:ppos].isdigit() and not t[ppos+1:].isdigit():
            return False 
        if t[:ppos] != "" and not t[:ppos].isdigit():
            return False
        if t[ppos+1:] != "" and not t[ppos+1:].isdigit():
            return False    
        return True