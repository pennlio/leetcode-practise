README.md
=======
concept
----
1. reverse string
2. padding string
3. add string with same length
4. reverse sum

Attention
-------
1. `string` is not `list` in python
2. cannot re-arrange elments of string in python, but can access
3. no more `append`, 'remove'
4. `replace`, `+` and using `+` to remove elements
5. reverse string : a[::-1]
