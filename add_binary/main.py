class Solution:
	def addBinary(self,a,b):
		la = len(a)
		lb = len(b)
		if la + lb == 0:
			return None
		elif la == 0:
			return b
		elif lb == 0:
			return a
				
		ra = self.reverseString(a)
		rb = self.reverseString(b)
		
		if la > lb:
			length = la
			rb = self.paddingStringAtBack(rb, length)
		elif lb > la:
			length = lb
			ra = self.paddingStringAtBack(ra,length)
		else:
			length = la
		i = 0
		e = 0
		rsum=""
		while i < length:
			(digitsum, new_e) = self.addTwoDigits(ra[i], rb[i], e)
			rsum += digitsum
			e = new_e
			i += 1
		# print rsum + "1"
		if e == '1':
			rsum += e
		return self.reverseString(rsum)


	def reverseString(self,a):
		return a[::-1]

	def paddingStringAtBack(self, a, ll):
		for i in range(ll-len(a)):
			a = a + '0'
		return a
		
	def addTwoDigits(self, a, b, e):
		a = int(a)
		b = int(b)
		e = int(e)
		int_sum = a + b + e
		if int_sum == 0:
			sum = '0'
			new_e = '0'
		elif int_sum == 1:
			sum = '1'
			new_e = '0'
		elif int_sum == 2:
			sum = '0'
			new_e = '1'
		else:
			sum = '1'
			new_e = '1'
		return (sum, new_e)

if __name__ == '__main__':
	sl = Solution()
	a = "1"
	b = "1101"
	c = sl.addBinary(a,b)
	print a
	print b
	print c
