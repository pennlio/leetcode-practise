# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None


class Solution:
    # @return a ListNode
    def addTwoNumbers(self, l1, l2):
    	p1 = l1
    	p2 = l2
    	if p1 == None and p2 == None:
    		return None
    	elif p1 == None:
    		return p2
    	elif p2 == None:
    		return p1
        p3 = l3 = ListNode(-1)  # starting node
        carrier = 0
        while p1!=None and p2!=None:
            p1p2sum = p1.val + p2.val + carrier
            new_node = ListNode(-1)
            p3.next = new_node
            p3 = new_node
            if p1p2sum >= 10:
               carrier = 1
            else:
               carrier = 0
            p3.val = p1p2sum % 10
            p1 = p1.next
            p2 = p2.next

        if p1 == None and p2 == None and carrier == 1:
            new_node = ListNode(1)
            p3.next = new_node
            carrier = 0
            return l3.next
        elif p1 == None:
            p = p2
        else:
            p = p1

        while p!=None:
            p1p2sum = p.val + carrier
            if p1p2sum >= 10:
                carrier = 1
            else:
                carrier = 0
            new_node = ListNode(p1p2sum%10)
            p3.next = new_node
            p3 = new_node
            p = p.next

        if carrier:
            new_node = ListNode(1)
            p3.next = new_node
            carrier = 0
        return l3.next



    def BuildLinkList(self, number_list):
		i = 0;
		while(True):
			nl = number_list
			try:
				n = nl.pop()
			except IndexError, e:
				# print "end of list"
				break;
			else:
				if i==0:
					head_node = ListNode(n)
					head_node.next = None
					prev_node = head_node
				else:
					new_node = ListNode(n)
					prev_node.next = new_node
					prev_node = new_node
				# print prev_node.val
				i += 1
		return head_node

    def PrintLinkList(self, head_node):
		node = head_node
		while (node!=None):
			print node.val
			node = node.next
		pass


if __name__ == '__main__':
    # construct a link list
    # num1 = [1,2,3,5,1,0];
    # num in reverse order
    num1 = [9,3,9]
    num2 = [9,9]
    # num2 = [2,3,6,8,0,0,0,0,0];
    solution = Solution()
    h1 = solution.BuildLinkList(num1)
    h2 = solution.BuildLinkList(num2)
    h3 = solution.addTwoNumbers(h1,h2)

    solution.PrintLinkList(h3)




