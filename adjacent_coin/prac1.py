def solution(A):
    n = len(A)
    result = 0
    r = 0
    ''' p2'''
    for i in xrange(n-1): # calc # of current paris
        if (A[i] == A[i+1]):
            result += 1
    
    for i in xrange(n): # use greedy to test flip of each postion
        count = 0
        if i > 0:
            if A[i-1] != A[i]:
                count += 1
            else:
                count -= 1
        if i < n-1:
            if A[i+1] != A[i]:
                count += 1
            else:
                count -= 1
        r = max (r, count)

    return result + r

def main():
    # A = [0,0,1,1,0,1,0,0,1,1,1,0,1]
    # A = [0,1,0,1,0,0,1,1,0,0,1,1,0]
    # A = [0,0,0,1,1,1,1,0,0]
    # A = [0,1]
    A = [0,1,0,1,0,0,1,1,0,0,1,1,0]
    print solution(A)

if __name__ == '__main__':
    main()