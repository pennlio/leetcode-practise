class Solution:
    # @param strs, a list of strings
    # @return a list of strings
    def anagrams(self, strs):
        if len(strs)==0:
            return []
        hasht = {}
        for i in range(len(strs)):
            # y = strs[i].replace(' ','')
            # y = strs[i].sort()
            y = ''.join(sorted(strs[i]))
            if y not in hasht:
                hasht[y] = []
            hasht[y].append(i)  # append index 

        # aglabel = 0 # aglabel for anagram
        outindex = []
        for x in hasht:
            if len(hasht[x]) >1:
                outindex += hasht[x]
        result = []
        if outindex:
            for i in outindex:
                result.append(strs[i])
        return result
        # return result
        # chars = [x.replace(' ','') for x in strs]
        # print chars




if __name__ == '__main__':
    sl = Solution()
    # strs = ['ad dc','addc','ad c','a dd c','addd']
    strs = [""]
    strs = ['and', 'dan']
    strs = ["tea","and","ate","eat","dan"]
    print sl.anagrams(strs)
