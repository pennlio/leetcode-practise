class Solution:
    # @return an integer
    '''too strong to be a converter'''
    def atoi(self, str):
        if not str:
            return 0
        allSpace = 1
        i = 0
        for i in range(len(str)):
            if str[i] != ' ':
                allSpace = 0
                break
        if allSpace: return 0
        reStr = str[i:] #get rid of starting spaces
        inState = 0
        sign = 0
        startIndex = -1
        for i in range(len(reStr)):
            if reStr[i] == '+' or reStr[i] == '-':
                if sign != 0:
                    if startIndex != -1:
                        num = self.convert(reStr, startIndex, i-1, sign)
                        inState = 0
                        return num
                    else:
                        return 0 # wrong 
                else:
                    inState = 1
                sign = 1 if reStr[i] == '+' else -1 #in state
            elif ord(reStr[i]) in range(ord('0'), ord('0')+10): # a number
                if startIndex == -1: # first number
                    inState = 1
                    if sign == 0:
                        sign = 1 # give it to pos if no sign before
                    startIndex = i
                else:
                    continue
            else:
                if inState:
                    if startIndex != -1:
                        num = self.convert(reStr, startIndex, i-1, sign)
                        inState = 0
                        return num
                return 0
        if startIndex != -1:
            num = self.convert(reStr, startIndex, i, sign)
            return num
        else:
            return 0

    def convert(self, reStr, startIndex, i, sign):
        # if within max
        num = int(reStr[startIndex:i+1])
        if sign==1 and num > 2147483647:
            num = 2147483647
        elif sign == -1 and num > 2147483648:
            num = 2147483648
        return num if sign == 1 else -num


if __name__ == '__main__':
    sl = Solution()
    # str =  'ad s f+ +-10 d10 3'
    # str = '+-2'
    str = '   +0 123'
    str = ' b212'
    str = "    010"
    # str = '-2147483648'
    print sl.atoi(str)
# -+21
# --22-
# -ad-21