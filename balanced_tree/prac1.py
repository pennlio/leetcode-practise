# Definition for a  binary tree node
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    # @param root, a tree node
    # @return a boolean
    def isBalanced(self, root):
        if not root:
            return False

        label, depth = self.calDepth(root)
        return False if label else True

    def calDepth(self, root):
        if not root:
            return 0,0
        ll, leftDpth = self.calDepth(root.left)
        rl, rightDpth = self.calDepth(root.right)
        if ll or rl:  # subtree is unbalanced:
            label = 1
        else:
            if leftDpth - rightDpth > 1 or leftDpth - rightDpth < -1:
                label = 1
            else:
                label = 0
        return label, max(leftDpth+1,rightDpth+1)

if __name__ == '__main__':
    sl = Solution()
    # root = TreeNode(1)
    # root.left = TreeNode(2)
    # root.left.left = TreeNode(3)
    # root.left.left.left = TreeNode(3)
    print sl.isBalanced(root)
    # def calDepthDiff(self, root):
    #     # if root.left == None and root.right == None:
    #     if root.left:
    #         leftDpthDiff = self.calDepthDiff(root.left)
    #         # if leftDpthDiff == -1:

    #     else:
    #         leftDpthDiff = 0
    #     if root.right:
    #         rightDpthDiff = self.calDepthDiff(root.right)
    #     else:
    #         rightDpthDiff = 0

    #     DepthDiff = max(leftDpthDiff,rightDpthDiff)
    #     return DepthDiff if DepthDiff <= 1 else -1

            # rightDpthDiff = 0
        # if leftDpthDiff - rightDpthDiff > 1 or leftDpthDiff - rightDpthDiff < -1:
            # return -1
        # else:
            # return max(leftDpthDiff,rightDpthDiff)
