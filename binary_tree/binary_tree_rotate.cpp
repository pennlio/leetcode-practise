#include <iostream>
#include <string.h>
using namespace std;


// * define the BST structure

typedef struct BinaryTreeNode{
	char data;
	int balance_factor;
	struct BinaryTree * left_child, * right_child;
}BinaryTreeNode, *BinaryTree; //define node and node_ptr, which is also the pointer of BinaryTree


void RightRotate(BinaryTree * root_node)  //do rotate from root_node node 
{
	BinaryTree * left_child_tree;

	left_child_tree = (*root_node).left_child; //left_child_tree's right tree connects to the root node 
	(*root_node).left_child = (* left_child_tree).right_child;
	(*left_child_tree).right_child = root_node; //move left_child_tree to the center
	
	root_node = left_child_tree; //update root node
}


void LeftRotate(BinaryTree * root_node)
{
	BinaryTree * right_child_tree;
	
	right_child_tree = (*root_node).right_child;
	(*root_node).right_child = *(right_child_tree).left_child;
	*(right_child_tree).left_child = root_node;

	root_node = right_child_tree; //update root node 

}


void PreOderTraverse(BinaryTree * T)
{
	if(T==NULL){
		cout << "This is an empty tree\n";
		return;  //this return is important in recursion
	}
	else{
		cout << (*T).data << endl;
		PreOderTraverse((*T).left_child); //first left then right
		PreOderTraverse((*T).right_child);
	}

}


void CreateBinaryTree(BinaryTree * root_node){
	if(root_node == NULL){
		cout << "it is an empty tree\n";
		return;
	}
	char input_value;
	cin >> input_value;
	const char kTreeEndSignal ='#';
	if (strcmp(input_value, kTreeEndSignal)
	{
		cout << "empty \n";
		return;
	}
	else
	{
		(* root_node).data = input_value;
		CreateBinaryTree((*root_node).left_child);
		CreateBinaryTree((*root_node).right_child);	
	}
}


// }

int main()
{
	BinaryTree * head_node;
	CreateBinaryTree(head_node);
	/* code */
	return 0;
}