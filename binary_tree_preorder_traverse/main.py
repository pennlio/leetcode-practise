
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None
        self.p = None

class Solution:
    # @param root, a tree node
    # @return a list of integers
    # g_output_node_list
    # g_output_node_list = []   # save output nodes

    def preorderTraversal(self, root):
        if root == None:
            # return self.__class__.g_output_node_list
            return
        else:
            # self.__class__.g_output_node_list.append(root.val)
            print root.val
            self.preorderTraversal(root.left);
            self.preorderTraversal(root.right);
            return
    def addNode(self, root_node, value): 
        if root_node==None:
            newnode = TreeNode(value)
            root_node = newnode
            return newnode  # change from root_node

        if value < root_node.val:
            newnode = self.addNode(root_node.left, value)
            root_node.left = newnode  # link with parent!
            newnode.p = root_node
        else:
            newnode = self.addNode(root_node.right,value)
            root_node.right = newnode
            newnode.p = root_node
        return root_node

    def DeleteNode(self, root_node, value):
        if root_node == None:
            print "Empty tree, no such value"
            return root_node
        if root_node.val == value:
            if root_node.left == None and root_node.right == None:
                root_node = None
            elif root_node.left == None and root_node.right != None:
                root_node = root_node.right
            elif root_node.left != None and root_node.right == None:
                root_node = root_node.left
            else:
                successor_node = self.FindSuccessor(root_node)
                root_node.val = successor_node.val
                self.DeleteNode(successor_node, successor_node.val)

        elif root_node.val > value:
            newnode = self.DeleteNode(root_node.left, value)
            root_node.left = newnode #likn updated to root_node
        else:
            newnode = self.DeleteNode(root_node.right, value)
            root_node.right = newnode #likn updated to root_node
        return root_node

    def FindSuccessor(self, origin_node):
        if origin_node.right == None: 
            iternode = origin_node.p
            while iternode != iternode.p.left:
                iternode = iternode.p
        else:
            iternode = origin_node.right
            while iternode.left != None :
                iternode = iternode.left
        return iternode



if __name__ == "__main__":
    array = [1,4,3,9,7]
    sl = Solution()
    for i in array:
        try:
            root_node = sl.addNode(root_node, i) 
        except Exception, e:
            root_node = TreeNode(i)
        else:
            pass
    result = sl.preorderTraversal(root_node)
    print result
    sl.DeleteNode(root_node,4)
    # sl.DeleteNode(root_node,4)
    result2 = sl.preorderTraversal(root_node)
    print result2



