import collections
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None
# BST with postive values
class Solution:
    def buildTree(self, pairs):
        x = [link[0] for link in pairs] # parent nodes
        y = [link[1] for link in pairs] # child nodes
        cnt = [0]*max(x)
        for i in range(len(x)):
            cnt[x[i]-1] = 1
        for j in y:
            if j <= max(x):
                cnt[j-1] = 0
        roots = []
        for i in range(len(cnt)):
            if cnt[i]!= 0:
                roots.append(i+1)
                if len(roots) > 1:
                    return None
        if not roots: return None # corner case: when there is cycle
        # here BFS , build map 
        maphash = collections.defaultdict(list)
        for x in pairs:
            if x[0] in maphash:
                maphash[x[0]].append(x[1])
            else:
                maphash[x[0]] = [x[1]]

        rootNode = TreeNode(roots[0]) # build root
        queue = [rootNode, None] # init queue

        while True:
            x = queue.pop(0)
            if not x:
                if not queue: # end of eque
                    return rootNode
                queue.append(None) # level end
                continue
            else:
                if x.val not in maphash: # leaf node
                    maphash[x.val] = []
                elif maphash[x.val]:  # which means has not been visited
                    for y in maphash[x.val]:
                        newNode = TreeNode(y)
                        if y <= x.val:
                            x.left = newNode
                        else:
                            x.right = newNode
                        queue.append(newNode) # append
                    maphash[x.val] = []
                elif not maphash[x.val]: # has been visited!!!
                    return None

if __name__ == '__main__':
    sl = Solution()
    pairs = [ [5,3],[5,8],[8,7],[8,19],[19,5]]
    print sl.buildTree(pairs)




