class Solution:
    # @param prices, a list of integer
    # @return an integer
    def maxProfit(self, prices):
        if not prices:
            return 0
        profit = [0]
        n = len(prices)
        for i in range(1,n):
            profit.append(prices[i]-prices[i-1])
        # find max-sub array
        low = 0
        high = n-1
        # mid = (low+high)/2
        return self.findMaxSubarray(profit,low,high)

    def findMaxSubarray(self, profit, low, high):
        if low == high:
            return profit[low]
        mid = (low + high)/2
        # d&c method for different cases
        lmax = self.findMaxSubarray(profit, low, mid)
        rmax = self.findMaxSubarray(profit, mid+1, high)
        mmax = self.findCrossMax(profit, low, high)

        return max(lmax,rmax,mmax)

    def findCrossMax(self, profit, low, high):
        mid = (low+high)/2
        # left
        lpmax = float("-inf") 
        i = mid
        lsum = 0
        while i >= low:
            lsum += profit[i]
            if lsum > lpmax:
                lpmax = lsum
            i -= 1
        # right
        rpmax = float("-inf") # initialize
        i = mid+1
        rsum = 0
        while i <= high:
            rsum += profit[i]
            if rsum > rpmax:
                rpmax = rsum
            i += 1
        return rpmax + lpmax


if __name__ == '__main__':
    sl = Solution()
    prices = [3,6,4,8,7]
    print sl.maxProfit(prices)