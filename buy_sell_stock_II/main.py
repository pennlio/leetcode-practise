class Solution:
    # @param prices, a list of integer
    # @return an integer
    def maxProfit(self, prices):
        n = len(prices)
        if n == 0:
            return 0
        dayProfit = [0] # first day
        for i in range(1,n):
            dayProfit.append(prices[i]-prices[i-1])
        dayProfit.sort()
        dayProfit.reverse()
        profit = 0
        i = 0
        while dayProfit[i]>0:
            profit += dayProfit[i]
            i += 1
        return profit

if __name__ == '__main__':
    sl = Solution()
    # prices = [2,5,1,3,2]
    prices = [9,8,7]
    print sl.maxProfit(prices)
