class Solution:
    # @param prices, a list of integer
    # @return an integer
    def maxProfit(self, prices):
        n = len(prices)
        if n == 0:
            return 0
        dayProfit = [0]*n
        for i in range(1,n):
            dayProfit[i] = prices[i]-prices[i-1]
        profit = 0
        for i in range(n):
            if dayProfit[i]>0:
                profit += dayProfit[i]
        return profit