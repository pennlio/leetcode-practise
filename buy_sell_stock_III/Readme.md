Readme.

1st
=======
concept:
-------
- nearly like 2nd solution's DP concept; the max in obtained in two ways: the max obtained by first trans + 1. possbilely 2nd tran out of 1st's period; 2. possiblely 2nd trans within 1st' period

imple:
--------
- imple 1st trans first; then run 2 on top of 1st
- too much pointer, label to return; 
- O(nlogn) time with DP


2nd
======
concept:
-----
- standard DP: 
    - define function: f(k,i) = maxProfit up to i with k trans
    - write the recursion function: f(k,i) = max(f(k,i-1),f(k-1,j) + p[i] - p[j]); add one more trans within i period
    - define boudanry condition: f(0,i) and f(k,0)
    - O(m*k)

- imple:
-------
- bottom up
- since the matrix length is predetermined, so we can use list to store the DP soluitons rather than hashtable

3rd
=====
concept:
-------
- Also ni DP, but consider the price data not the differential data
- two scans : from left to right get max(1st trans); from right to left get min (2nd trans); 
- combine the trans result together

imple:
---
- O(n) time and space