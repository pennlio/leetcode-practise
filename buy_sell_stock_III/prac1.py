class Solution:
    # @param prices, a list of integer
    # @return an integer
    def __init__(self):
        self._hash = {}
    def maxProfit(self, prices):
        # basic idea: find best_by_and_sell one time solution; then the 2nd solution is :
        # - 1st solution - reverse-first time solution in 1st
        # - 1st solution + 2nd solution among front/end elements 
        if not prices:
            return 0
        dailyProfit = [0]*len(prices)
        for i in range(1, len(prices)):
            dailyProfit[i] = prices[i] - prices[i-1]
        maxP, subLow, subHigh = self.findMaxProfit(dailyProfit,0,len(prices)-1)
        if subLow and subHigh:
            leftProfit, _,_ = self.findMaxProfit(dailyProfit,0, subLow-1)
            rightProfit,_,_ = self.findMaxProfit(dailyProfit, subHigh+1, len(prices)-1)
            minusProfit = [-dailyProfit[i] for i in range(subLow, subHigh+1) ] # build -value profit array
            minP,_,_ = self.findMaxProfit(minusProfit,0,len(minusProfit)-1)
            return max(leftProfit, rightProfit, minP) + maxP
        else:
            return maxP 


    def findMaxProfit(self, dailyProfit, low, high):
        if low > high: return (None, None, None)
        if max(dailyProfit) <= 0: return (0, None, None) # if highest is 0
        if (low, high) not in self._hash:
            if low == high : self._hash[(low,high)] = dailyProfit[low]
            mid = (low + high)/2
            (mPC, lp0, rp0) = self.findMaxCrossProfit(dailyProfit, low, mid, high)
            (mPL, lp1, rp1) = self.findMaxProfit(dailyProfit, low, mid-1)
            (mPR, lp2, rp2) = self.findMaxProfit(dailyProfit, mid+1, high)
            if mPC >= mPL and mPC >= mPR: # center win
                self._hash[(low,high)] = (mPC, lp0, rp0)
            elif mPL >= mPC and mPL >= mPR:
                self._hash[(low,high)] = (mPL, lp1, lp1)
            else:
                self._hash[(low,high)] = (mPR, lp2, rp2)
        return self._hash[(low, high)]


    def findMaxCrossProfit(self, dailyProfit, low, mid, high):
        if low > mid or mid > high : return (None, None, None)
        if low == mid == high:
            return (dailyProfit[mid], mid, mid)
        leftPole = rightPole = mid
        leftSum = rightSum = leftMax = rightMax = 0 # assign value not ptr
        p1 = mid - 1
        while p1 >= low:
            leftSum += dailyProfit[p1]
            if leftSum >= leftMax: # expand as far as possible
                leftMax = leftSum
                leftPole = p1
            p1 -= 1

        p2 = mid + 1
        while p2 <= high:
            rightSum += dailyProfit[p2]
            if rightSum >= rightMax:
                rightMax = rightSum
                rightPole = p2
            p2 += 1
        return (rightMax + leftMax + dailyProfit[mid], leftPole, rightPole)

if __name__ == '__main__':
    prices = [5,4,6,7,10,9,5]
    sl = Solution()
    print sl.maxProfit(prices)
