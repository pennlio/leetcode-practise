# learn from sample
class Solution:
    # @param prices, a list of integer
    # @return an integer
    # def __init__(self):s
    def maxProfit(self, prices):
        if not prices:
            return 0
        # define f(k,i) as maxProfit of performing k trans up until i
        K = 2
        l = len(prices)
        self._hash = [ [0]*l for x in range(K+1)] 
        maxProfit = -float('inf')
        # bottom up
        for i in range(l):
            for k in range(K+1):
                maxProfit = max(self.calMaxprofit(prices, k,i), maxProfit)
        return maxProfit

    def calMaxprofit(self, prices, k, i) : #using k trans to rich up until i inclusive
        if (k,i) not in self._hash:
            if k == 0 or i ==0:  
                self._hash[k][i] = 0
            else:
                maxProf = - float('inf')
                for j in range(i+1):
                    maxProf = max(self.calMaxprofit(prices, k-1,j) - prices[j], maxProf)
                self._hash[k][i] = max(self.calMaxprofit(prices, k, i-1), maxProf + prices[i])
        return self._hash[k][i]

if __name__ == '__main__':
    prices = [5,4,6,7,10,9,5]
    sl = Solution()
    print sl.maxProfit(prices)