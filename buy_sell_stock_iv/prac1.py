class Solution:
    # @return an integer as the maximum profit 
    def __init__(self):
        self._profit_dict = {}
    def maxProfit(self, k, prices):
        if not prices: return 0
        if not k: return 0 # base case
        prcDiff = [prices[i+1]-prices[i] for i in xrange(len(prices)-1)]
        prcDiff = [0] + prcDiff # add beginning
        # print prcDiff
        return self.profit(prcDiff, 0, len(prices)-1, k)

    def profit(self, prcDiff, start, end, trans):
        # use DP: profit(start, end, trans): max profit can be obtained at (start, end) making at most trans hands of transactions
        if start >= end or start < 0 or end >= len(prcDiff):
            return 0
        if trans <= 0:
            return 0
        if (start, end, trans) not in self._profit_dict:
            if trans == 1:
                mid = (start+end)/2
                crossMax = self.getCrossMax(prcDiff, start, end, mid)
                # print crossMax
                self._profit_dict[(start,end,trans)] = max(self.profit(prcDiff, start, mid-1, 1), crossMax, self.profit(prcDiff, mid+1, end, 1), 0) 
            else:
                curmax = 0
                for stop1 in xrange(start, end+1): # find optimal split
                    for start2 in xrange(stop1+1, end+1): # stop1 and start2 no overlap
                        pr = self.profit(prcDiff, start, stop1, trans-1) + self.profit(prcDiff, start2, end, 1)
                        curmax = max(curmax, pr)
                self._profit_dict[(start,end,trans)] = max(curmax, 0)
        return self._profit_dict[(start, end, trans)]

    def getCrossMax(self, prcDiff, start, end, mid):
        rightMax = leftMax = lsum = rsum = 0 
        for i in xrange(mid-1, start-1, -1):
            lsum = lsum + prcDiff[i]
            leftMax = max(lsum, leftMax)
        for i in xrange(mid+1, end+1):
            rsum = rsum + prcDiff[i]
            rightMax = max(rsum, rightMax)
        return leftMax + prcDiff[mid] + rightMax

if __name__ == '__main__':
    sl = Solution()
    k = 2
    prices = [1,5,6]
    prices = [2,3,1,5,6,4]
    # prices = [2,6]
    print sl.maxProfit(k, prices)