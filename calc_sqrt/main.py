#!\bin\python

# def find_sqrt(x):
# 	if x == 0 or x==1:
# 		return x
# 	else:
# 		delta = 0.001
# 		high = x
# 		low = 0
# 		y = float((high + low)/2)
# 		while abs(y*y - x) > delta:
# 			if y*y < x:
# 				low = y
# 				y = (low + high)/2
# 				continue
# 			else:
# 				high = y
# 				y = (low + high)/2 
# 	return y

def find_sqrt(x): #integer
	if x == 0 or x == 1:
		return x
	elif x == 2:
		return 1  ### careful when x = 2!!
	else:
		low = 0
		high = x
		y = (high+low)/2
		while 1:
			if (y-1)*(y-1) < x and y*y < x:
				low = y
				y = (low+high)/2
			elif y*y > x and (y-1) *(y-1) >x:
				high = y
				y = (low + high)/2
			elif (y-1)*(y-1) <= x and y*y > x:
				return y-1
			else:	
				return y


if __name__ == '__main__':
	x = 4;
	print find_sqrt(x) 


