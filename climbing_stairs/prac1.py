class Solution:
    # @param n, an integer
    # @return an integer
    def __init__(self):
        self._hash = {}

    def climbStairs(self, n):

        if n in self._hash:
            return self._hash[n]
        else:
            if n == 0 or n == 1 or n == 2:
                self._hash[n] = n
                return n
            self._hash[n] = self.climbStairs(n-1) + self.climbStairs(n-2)
            return self._hash[n]

if __name__ == '__main__':
    n = 5
    sl = Solution()
    print sl.climbStairs(n)