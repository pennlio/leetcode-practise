# Definition for a undirected graph node
class UndirectedGraphNode:
    def __init__(self, x):
        self.label = x
        self.neighbors = []

class Solution:
    # @param node, a undirected graph node
    # @return a undirected graph node
    def __init__(self):
        self._visited = {}
    def cloneGraph(self, node):
        if not node: return node
        rootnode = UndirectedGraphNode(node.label)
        self._visited[rootnode.label] = rootnode # visited
        for x in node.neighbors:
            if x.label in self._visited:
                rootnode.neighbors.append(self._visited[x.label]) # add already created node
            else: # create new node
                newnode = self.cloneGraph(x) # clone graph
                rootnode.neighbors.append(newnode)
        return rootnode

        