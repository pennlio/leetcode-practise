class Solution:
    # @param candidates, a list of integers
    # @param target, integer
    # @return a list of lists of integers
    def __init__(self):
        self._result = []
        self._hash = {}
    def combinationSum(self, candidates, target):
        ''' RECURSION '''
        if not candidates:
            return [[]]
        candidates.sort()
        counter = [0]*len(candidates) # finally use cnt sort
        counter = self.DPcombSum(candidates,target,counter)
        return self.outputArray(candidates)
        # return self._result

    def DPcombSum(self, candidates, target, counter):
        if target not in self._hash:
            if target < 0: return
            if target == 0:
                if counter not in self._result:
                    self._result.append(counter)
                return   
            for i in range(len(candidates)):
                # counter[i] += 1  # +1 indivicially
                selfcounter = list(counter)
                selfcounter[i] += 1
                self.DPcombSum(candidates, target - candidates[i], selfcounter)
                # should store counter in hashtable
        else:
            pass

    def outputArray(self, candidates):
        output = []
        for x in self._result:
            comb = []
            for i in range(len(x)):
                while x[i]!=0:
                    comb.append(candidates[i])
                    x[i] -= 1
            output.append(comb)
        return output

if __name__ == '__main__':
    sl = Solution()
    candidates = [92,71,89,74,102,91,70,119,86,116,114,106,80,81,115,99,117,93,76,77,111,110,75,104,95,112,94,73]
    target = 310
    print sl.combinationSum(candidates,target)



