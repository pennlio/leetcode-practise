class Solution:
    # @param candidates, a list of integers
    # @param target, integer
    # @return a list of lists of integers
    def __init__(self):
        self._hash= {}
    def combinationSum(self, candidates, target):
        ''' DP Version'''
        if not candidates:
            return [[]]
        candidates.sort()
        combSet = self.DPcombSum(candidates,target)
        return self.outputArray(candidates,combSet)

    def DPcombSum(self, candidates, target):
        '''return combSet for each target'''
        if target not in self._hash:
            # counter =  # finally use cnt sort
            if target < 0: return []
            if target == 0:
                return [[0]*len(candidates)]
            else:
                combSet = []
                for i in range(len(candidates)):
                    for x in self.DPcombSum(candidates, target - candidates[i]):
                        y = x[:] # here do a copy from x to y
                        y[i] += 1 # modify y 
                        if y not in combSet:
                            combSet.append(y)
                self._hash[target] = combSet
        return self._hash[target]

    def outputArray(self, candidates, combSet):
        output = []
        for x in combSet:
            comb = []
            for i in range(len(x)):
                while x[i]!=0:
                    comb.append(candidates[i])
                    x[i] -= 1
            output.append(comb)
        return output

if __name__ == '__main__':
    sl = Solution()
    # candidates = [92,71,89,74,102,91,70,119,86,116,114,106,80,81,115,99,117,93,76,77,111,110,75,104,95,112,94,73]
    target = 7
    candidates = [2,3,6,7]
    print sl.combinationSum(candidates,target)



