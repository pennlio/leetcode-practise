concept:
-----------
- use DP
- the key of DP includes candidates: since candidates changes over time, so even for the same target, the result changes for different candidates; so DP key should include candidates

imple:
-------
- 'a+x' and 'a.append(x)': if a and x are both lists, then a+x just combine the elements of x and a; while a.append(x) add x (list) as a element of a, so that makes a a list of lists.


corner case:
-------
- candidate=[1], target = 1: when target = 0, always return [[]] even if candidate=[]


improve:
--------
- shall improve time