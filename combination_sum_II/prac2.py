class Solution:
    # @param candidates, a list of integers
    # @param target, integer
    # @return a list of lists of integers
    def __init__(self):
        self._hash = {}
    def combinationSum2(self, candidates, target):
        '''Nested DP'''
        if not candidates or target < 0:
            return [[]]# return None since it fail
        candidates.sort()
        return self.DPCombSum(candidates, target)

    def DPCombSum(self, candidates, target):
        if target == 0:
            return [[]] # return an empty set
        if not candidates or target < 0: 
            return [] # return None since it fail
        comSet = []
        if target not in self._hash:
            for i in range(len(candidates)):
                tmp = list(candidates)
                tt = tmp.pop(i)
                # print tmp
                y = self.DPCombSum(tmp, target-tt)
                # for x in y:
                #     xx = list(x) + [tt]
                #     xx.sort()  # need sort to avoid dup
                #     if xx not in comSet:
                #         comSet.append(xx) # must use append(xx) not comSet+xx, 
                comSet = self.addXtoY(y,tt,comSet)
            self._hash[target] = comSet
        # return comSet
        return self._hash[target]

    def addXtoY(self, y, tt, comSet):
        for x in y:
            xx = list(x) + [tt]
            xx.sort()  # need sort to avoid dup
            if xx not in comSet:
                comSet.append(xx) # must use append(xx) not comSet+xx, since 2nd add xx's elements not the list
        return comSet 


    # def insertTT(self, x, tt):
    #     if not x:
    #         return [tt]
    #     xx = list(x)
    #     n = len(xx)




if __name__ == '__main__':
    sl = Solution()
    target  = 1
    # candidates = [10,1,2,7,6,1,5]
    candidates = [1]
    # candidates = [14,18,19,30,6,5,14,23,28,18,26,21,12,15,29,18,32,23,6,21,19,30,6,28,17,13,29,28,10,34,26,11,10,32,7,11,32,8,21,18,22,5,34,21,7,20,26,5,9,28,21,23,23,15,8,27,23,32,12,20,31,33,27,28,30,21,34,19] 
    # target = 29
    target = 6
    candidates = [4,2,1,4,2,1,3]
    candidates = [1,1,2,2,3,4,4]
    # candidates = [1,2,2,3,4,5] # why add one!!
    candidates = [1,2,2,3]
    # target = 7
    # candidates = [2,2,3,6]
    print sl.combinationSum2(candidates, target)


