concept:
--------
- k pointer moving forward in n
- if last one reaches end, then check-back the 2nd last one ...until the first pointer


impel:
------
- multiple index: current = map(num.__getitem__,ptrs) #
- pay attention to strPtr: each time actPtr reaches end, back check from actPtr itself, so have stPtr == actPtr each time
- alignPtr(): align from the 'reset' position from stPtr 

corner case
--------
- n>0 and k == 0, return []
- k == 1


improve
-------
- can be done easier if maintain array value [] ins. of pointers


