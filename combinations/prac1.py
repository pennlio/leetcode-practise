class Solution:
    # @return a list of lists of integers
    '''failed or too complicated to use pointers, use index directcly'''
    def combine(self, n, k):
        if n <= 0 or n < k:
            return []
        if n > 0 and k == 0:
            return []
        num =  range(1, n+1)
        result = []
        actPtr = stPtr = k-1 # stPtr dec from k-1 to 0
        ptrs = range(0,k)

        while stPtr >= 0:
                while ptrs[actPtr] <= n-k+actPtr: 
                    result.append(map(num.__getitem__,ptrs)) #
                    if ptrs[actPtr] == n-k+actPtr:
                        break
                    ptrs[actPtr] += 1
                stPtr = actPtr  
                while ptrs[stPtr] == n-k+stPtr: #  check from last to left to find the un-ended ptr
                    stPtr -= 1 #o/w check prev pointer
                ptrs,actPtr = self.alignPtr(stPtr,ptrs)
        return result

    def alignPtr(self,stPtr,ptrs):
        stPtr_base = ptrs[stPtr] + 1
        for i in range(stPtr, len(ptrs)):
            ptrs[i] = stPtr_base
            stPtr_base += 1
        return ptrs, len(ptrs)-1 # return the updated ptr and still active in k-1 position

if __name__ == '__main__':
    sl = Solution()
    n,k = 5,1
    print sl.combine(n,k)

