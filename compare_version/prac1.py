class Solution:
    # @param version1, a string
    # @param version2, a string
    # @return an integer
    def compareVersion(self, version1, version2):
        if version1 == version2:
            return 0
        # v1 = float(version1)
        # v2 = float(version2)
        v1 = version1.split('.') # split into intergers
        v2 = version2.split('.')

        n1 = len(v1)
        n2 = len(v2)
        for i in range(min(n1,n2)):
            if int(v1[i]) > int(v2[i]):
                return 1
            elif int(v1[i]) < int(v2[i]):
                return -1
        if n1 < n2:
            for x in v2[n1:]:
                if int(x) != 0:
                    return -1
        elif n1 > n2:
            for x in v1[n2:]:
                if int(x) != 0:
                    return 1
        return 0


        # return 1 if v1>v2 else -1

if __name__ == '__main__':
    sl = Solution()
    v1 = '1.00.0'
    v2 = '1.0'
    print sl.compareVersion(v1,v2)