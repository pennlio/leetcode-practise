# Definition for a  binary tree node
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    # @param inorder, a list of integers
    # @param postorder, a list of integers
    # @return a tree node
    def buildTree(self, inorder, postorder):
        if not inorder: 
            return None # no input
        else:
            return self.build(inorder, 0, len(inorder)-1, postorder, 0, len(postorder)-1)
        
    def build(self, inorder, li, hi, postorder, lp, hp):
        if li > hi or lp > hp: return None # no such node
        if li == hi and lp == hp: # only one
            return TreeNode(inorder[li])
        root = TreeNode(postorder[hp]) # find root
        mid = inorder.index(postorder[hp])
        root.left = self.build(inorder, li, mid-1, postorder, lp, lp+(mid-1-li))
        root.right = self.build(inorder, mid+1, hi, postorder, lp+mid-li, hp-1)
        return root