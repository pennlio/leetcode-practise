# Definition for a  binary tree node
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    # @param preorder, a list of integers
    # @param inorder, a list of integers
    # @return a tree node
    def buildTree(self, preorder, inorder):
        if not preorder: 
            return None # empty tree
        else:
            return self.build(preorder, 0, len(preorder)-1, inorder, 0, len(inorder)-1)
    
    def build(self, preorder, lp, hp, inorder, li, hi):
        if lp > hp or li > hi: return None 
        if lp == hp and li == hi:
            return TreeNode(preorder[lp])
        root = TreeNode(preorder[lp])
        mid = inorder.index(root.val)
        root.left = self.build(preorder, lp+1, lp+mid-li, inorder, li, mid-1)
        root.right = self.build(preorder, lp+mid-li+1, hp, inorder, mid+1, hi)
        return root
        
        