concept:
-------
- two for iterations
- TLE, O(n^2)

- way 2: move shorter of height[i] and height[j] to shrink the distance (from one basic case to correct to optimal)