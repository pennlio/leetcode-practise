class Solution:
    # @return an integer
    '''two for iteration, TLE'''
    def maxArea(self, height):
        if not height:
            return 0
        maxwater = maxx = 0
        n = len(height)
        for i in range(n):
            if height[i] <= maxx:
                continue
            else:
                maxx = height[i]
                maxj = 0
                for j in range(n-1, i, -1):  # from back, save a little time
                    if height[j] <= maxj:
                        continue
                    else:
                        maxj = height[j]
                        area = min(height[i],height[j]) * (j-i)
                        maxwater = max(maxwater,area)
        return maxwater

if __name__ == '__main__':
    sl = Solution()
    # height = [0,2,6,3,2,3]
    # height = [1,2,3,2,2]
    height = range(15000-1, 1 ,-1)
    # height.reverse()
    print sl.maxArea(height)