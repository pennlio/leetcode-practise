class Solution:
    # @return an integer
    '''two for iteration, TLE'''
    def maxArea(self, height):
        if not height:
            return 0
        i = 0
        j = len(height)-1
        maxwater = 0
        while(i<j):
            maxwater = max(maxwater, (j-i)*min(height[i],height[j]))
            if height[i]>height[j]:
                j -= 1
            else:
                i += 1
        return maxwater

if __name__ == '__main__':
    sl = Solution()
    # height = [0,2,6,3,2,3]
    # height = [1,2,3,2,2]
    # height = range(15000-1, 1 ,-1)
    height= range(15000)
    # height.reverse()
    print sl.maxArea(height)