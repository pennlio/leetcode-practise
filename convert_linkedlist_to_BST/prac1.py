# Definition for a  binary tree node
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    # @param head, a list node
    # @return a tree node
    def sortedListToBST(self, head):
        if not head: return None
        val, hl, hh = self.cutHalfNoMid(head) # cut half and get rid of mid
        # if hl or hh:
        root = TreeNode(val)
        if hl:
            root.left = self.sortedListToBST(hl)
        if hh:
            root.right = self.sortedListToBST(hh)
        return root
    
    def cutHalfNoMid(self, head): # cut half and get rid of mid
        if not head: return (-1, None, None)
        p0 = start = ListNode(-1)
        p0.next = p1 = p2 = head
        while p2.next != None and p2.next.next != None:
            p2 = p2.next.next
            p0 = p1
            p1 = p1.next
        # half found
        h2 = p1.next 
        mid = p1.val
        p0.next = None
        # p1 = None # get rid of p1 for next value
        return (mid, start.next, h2)

    def buildList(self, value_array):
        if len(value_array) == 0:
            return None
        else:
            value = value_array
            head = ListNode(value[0])
            latest_node = head
            for i in range(1, len(value)):
                new_node = ListNode(value[i])
                latest_node.next = new_node
                latest_node = new_node
        return head

if __name__ == '__main__':
    sl = Solution()
    head = sl.buildList([1,2,4,5,6])
    sl.sortedListToBST(head)
    
        
        
        
        