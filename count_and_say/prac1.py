class Solution:
    # @return a string
    def countAndSay(self, n):
        if n <=0 : return ''
        if n == 1: return '1'
        string = '1' + 'S' # sentinel 
        buf = []
        while n > 1:
            result = []
            for i in string:
                if buf and i != buf[-1]:
                    result.extend([str(len(buf)), buf[-1]]) # add to result
                    buf = []
                buf.append(i)
            # prepare for generating next number
            buf = [] # empty for next round
            string = ''.join(result+['S']) # add sentinel and generate string for next round
            n -= 1
        return string[:-1]

if __name__ == '__main__':
    sl = Solution()
    n = 6 
    print sl.countAndSay(n)