class Solution:
    # @return an integer
    ''' top down use recursion'''
    def __init__(self):
        self._hash = {}
    def numDistinct(self, S, T):
        return self.dpDistance(S, T, len(S)-1, len(T)-1) if S else 0

    def dpDistance(self, S, T, m ,n):
        if n == -1: # start of match
            return 1
        if m < 0 :
            return 0
        if (m,n) not in self._hash:
            if S[m] != T[n]:
                self._hash[m,n] = self.dpDistance(S,T,m-1, n)
            else:
                self._hash[m,n] = self.dpDistance(S,T,m-1, n) + self.dpDistance(S, T, m-1, n-1) 
        return self._hash[(m,n)]
            

if __name__ == '__main__':
    sl = Solution()
    S = 'abbabb'
    T = 'ba'
    print sl.numDistinct(S,T)