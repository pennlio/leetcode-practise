class Solution:
    ''' bottom up'''
    def numDistinct(self, S, T):
        hasht = {} # hash table
        for s in range(-1, len(S)):
            for t in range(-1, len(T)):
                if s == -1:
                    hasht[(s,t)] = 0 # boudary condition
                elif t == -1:  
                    hasht[(s,t)] = 1 # boudary condition
                elif s < t:
                    hasht[(s,t)] = 0
                else:
                    if S[s] != T[t]:
                        hasht[(s,t)] = hasht[(s-1,t)]
                    else:
                        hasht[(s,t)] = hasht[(s-1,t)] + hasht[(s-1,t-1)]
        return hasht[(len(S)-1, len(T)-1)]

if __name__ == '__main__':
    sl = Solution()
    S = 'abbabb'
    T = 'b'
    print sl.numDistinct(S,T)