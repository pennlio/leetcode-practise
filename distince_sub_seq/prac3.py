class Solution:
    ''' bottom up'''
    def numDistinct(self, S, T):
        # hasht = {} # hash table
        num = [[0 for x in range(len(T)+1)] for x in range(len(S)+1)]
        for m in range(len(S)+1):
            for n in range(-1, len(T)):
                if m == -1:
                    hasht[(m,n)] = 0
                elif n == -1:  # take m and n
                    hasht[(m,n)] = 1
                elif m < n:
                    hasht[(m,n)] = 0
                else:
                    if S[m] != T[n]:
                        hasht[(m,n)] = hasht[(m-1,n)]
                    else:
                        hasht[(m,n)] = hasht[(m-1,n)] + hasht[(m-1,n-1)]
        return hasht[(len(S)-1, len(T)-1)]

if __name__ == '__main__':
    sl = Solution()
    S = 'abbabb'
    T = 'b'
    print sl.numDistinct(S,T)