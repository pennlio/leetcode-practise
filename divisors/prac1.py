import math
from operator import itemgetter
import pdb

class Solution:
    def sortDivisors(self, numlist):
        a = sorted(numlist, key = itemgetter(0))
        return sorted(a, key = itemgetter(1))

    def getNoOfDivisors(self, number):
        # get divisors of that number
        # easier way is to decompose x = p^a q^b r^c ...
        # and # of Divisor is (a+1)(b+1)(c+1)
        # here use linear search up to sqrt(number)
        cnt = 0
        for x in range(1, int(math.sqrt(number))+1):
            if number % x == 0: 
                cnt += 1
        return cnt

def main():
    # main function
    sl = Solution()
    numlist = []
    while True:
        n = raw_input('enter a number, o/w quit:\n')
        # pdb.set_trace()
        try:
            n = int(n)
        except Exception, e:
            print ("finish input")
            break
        else:
            numlist.append(n)

    if not numlist: # no input
        print 'no input'
        return
    divisor_dict = {}
    
    map_traverse = []
    for x in numlist:
        if x not in divisor_dict:
            divisor_dict[x] = (x, sl.getNoOfDivisors(x))
            map_traverse.append(divisor_dict[x])
    # traverse map
    print sl.sortDivisors(map_traverse)


if __name__ == '__main__':
    main()
    # test case: 23,5, 17, 24, 48, 51
