class Solution:
    # @param s, a string
    # @return a list of strings
    def findRepeatedDnaSequences(self, s):
        if not s or len(s) < 10: return []
        hash = {} # hash table
        result = set()
        for i in range(len(s)-10+1):
            print s[i:i+10]
            if s[i:i+10] not in hash:
                hash[s[i:i+10]] = True
            else:
                result.add(s[i:i+10])
        return list(result)

if __name__ == '__main__':
    sl = Solution()
    s = 'AAAAAAAAAAAAAA'
    print sl.findRepeatedDnaSequences(s)