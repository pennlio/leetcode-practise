concept:
-------
- DP and bottom up way
- function DPDistance is defined as the distance between x[0:i] and y[0:j], i, j exclusive
- DP : 
    - define function; 
    - find recursive relation (case discussion); 
    - define boundary condition; 
    - bottom up

imple:
---------
- boundary conditon
- when doing build up, the base case i=0 and j=0 should be recorded; o/w program does not continue; but for top-down case not need; since just need final results

improve:
-----
- less time