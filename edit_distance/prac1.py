class Solution:
    # @return an integer
    def __init__(self):
        self._hash = {}
    def minDistance(self, word1, word2):
        n = len(word1)
        m = len(word2)
        if n == 0:
            return m
        elif m==0:
            return n

        # for i in range(0,n+1):  bottom-up
        #     for j in range(0,m+1):
        #         self.DPDistance(i,j,word1,word2)
        # return self._hash[n,m]

        return self.DPDistance(n,m,word1,word2) # top-down

    def DPDistance(self, i, j, word1, word2): # i, j are last index of word1 and word2
        if i == 0:
            self._hash[(i,j)] = j
        elif j == 0:
            self._hash[(i,j)] = i
        elif (i,j) not in self._hash:
            if word1[i-1] == word2[j-1]: # note that i and j is the prefix from 0 to i (not inclusive)
                self._hash[(i,j)] = min(self.DPDistance(i-1,j-1,word1,word2), self.DPDistance(i, j-1, word1, word2)+1, self.DPDistance(i-1, j, word1, word2)+1)
            else:
                self._hash[(i,j)] = min(self.DPDistance(i-1,j-1,word1,word2)+1, self.DPDistance(i, j-1, word1, word2)+1, self.DPDistance(i-1, j, word1, word2)+1)
        return self._hash[(i,j)]

if __name__ == '__main__':
    sl = Solution()
    # word1 = 'ahad'
    # word2 = 'adfd'
    word2 = 'b'
    word1 = 'bbba'
    print sl.minDistance(word1, word2)