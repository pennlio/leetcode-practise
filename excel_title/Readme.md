Readme
====
concept:
-----------
- divide and mod to get every digit


difficulty: start from `1`
------------
- think if not start from 1: MD(my design) expression:
- MD: A->Z:0->25; starting from  `0` -> `A`; excel: A->Z:0->26
- MD to Excel: -1 at each digit  


summary:
---------
- staring from 1: find the `mod` value; set section 1 to mod; use `num-1` to do `D&M`