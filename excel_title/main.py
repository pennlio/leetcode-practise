'''12/24/2014'''
''' Wrong!!!!'''
class Solution:
    # @return a string
    def convertToTitle(self, num):
        if num < 1:
            return None
        a = num/26 
        b = num%26
        return self.outputA(a, b) + self.outputLetter(b)

    def outputA(self, time, b):
        if b == 0:  # pay attention when b==0, so one less A
            time -= 1
        output1 = time * 'A'
        # while(time > 0):
            # output1 += 'A'
            # time -= 1
        return output1

    def outputLetter(self, b):
        if b == 0:
            output2 = 'Z'
        else:
            base = ord('A')
            output2 = chr(base + b - 1)
        return output2


if __name__ == '__main__':
    sl = Solution()
    # num = 27
    # num = 27
    
    # print sl.convertToTitle(num)
    for num in range(100):
        print num ,sl.convertToTitle(num)