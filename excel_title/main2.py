class Solution:
    # @return a string
    def convertToTitle(self, num):
        result = ''
        char = [chr(x) for x in range(ord('A'), ord('Z')+1)]
        while num>0:
            result += (char[(num-1) % 26])  #
            num = (num-1) // 26
        return result[::-1]  # reverse string in py


if __name__ =='__main__':
    sl = Solution()
    num = [26,27,52,53]
    result = [sl.convertToTitle(x) for x in num]
    print result