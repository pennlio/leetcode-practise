'''12/30/2014'''

class Solution:
    # @return an integer
    def trailingZeroes(self, n):
        if n <= 0:
            return 0
        result = 0
        _mod = 5
        while (n//_mod)>0: # get floor
            result += n//_mod
            _mod *= 5
        return result

if __name__ == '__main__':
    sl = Solution()
    n = 32
    print sl.trailingZeroes(n)

