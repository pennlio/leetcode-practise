class Solution:
    # @param num, a list of integer
    # @return an integer
    def findMin(self, num):
        if not num:
            return None
        n = len(num)
        if n == 1:
            return num[0]
        return self.BinaryFindMin(num, 0, n-1)

    def BinaryFindMin(self, num, low, high):
        if low > high:
            return float('inf') # error
        if low == high:
            return num[low]
        if num[low] == num[high]:
            return min(num[low], self.BinaryFindMin(num,low+1, high-1))
        mid = (high+low)/2
        if num[low] <= num[mid] <= num[high]:
            return num[low]
        elif num[high] <= num[low] <= num[mid]:
            return self.BinaryFindMin(num, mid+1, high)
        elif num[mid] <= num[high] <= num[low]:
            return self.BinaryFindMin(num, low+1, mid)

if __name__ == '__main__':
    sl = Solution()
    num = [4,0,1,1,1,1,1,1,1,1,1,2,3]
    print sl.findMin(num)
