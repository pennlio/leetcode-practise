class Solution:
    def findGap(self, A):
        if not A: return str(0)+ '-' + str(99)
        lastnum = -1
        result = []
        A.append(100)  # as sentinel
        for x in A:
            if x == lastnum + 1:
                lastnum = x # record the last number visited
            else:
                if x == lastnum + 2: # only missed one
                    result.append(str(lastnum+1))  # append as the number
                else: # missing a range
                    string = str(lastnum+1) + '-' + str(x-1)
                    result.append(string)  # append the range
                lastnum = x
        return ','.join(result) # string

if __name__ == '__main__':
    sl = Solution()
    A = [1,3,4,99]
    print sl.findGap(A)