# find max of an half mono-inc half mono-dec array
class Solution:
    def findMax(self, num):
        if not num: return None
        return self.binSearch(num, 0, len(num)-1)

    def binSearch(self, num, low, high):
        if low > high:
            return None # overflow
        elif low == high:
            return num[low]
        else:
            mid = (low+high)/2
            if num[mid] < num[high]: # no == sign, o/w when mid==high there will be overflow
                return self.binSearch(num, mid+1, high) # right
            elif num[mid] < num[low]:
                return self.binSearch(num, low, mid-1) # left
            else: # search both
                return max(self.binSearch(num,low,mid), self.binSearch(num,mid+1,high))

if __name__ == '__main__':
    sl = Solution()
    # num = [1,2,9,4,2,1]
    num = [0,1,3,4,3,2]
    print sl.findMax(num)