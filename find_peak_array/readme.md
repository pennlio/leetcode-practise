readme

concept:
------
- rule: when cmp with mid: larger behind --> right; larger before --> left
- binary search

imple:
--------
- compare when use `==`, must be careful, in case mid = low or mid == high, there will be over-flow
- when uncertain, search both way
- O(lgn)