README.md

#Find Single Number II
=============

##concept
------
- using hash table in python `dict`

##Debug
--------
- items in `dict` == `dict.items()`
- loop the `items` using `for k, v in dict.items()`

##How to without extra memory? 
binary repres.

##change represetation: to bit manuplation so that:
----------
 - all values are either `0` or `1`
 - any value appears 3 times can be cancled out using `mod 3`, no matter `1` or `0`
 - Change the question to :"given a bunch of `0` and `1` series, where they emerges in 3-pairs, execept one `1` or `0`; find that value?" This question repeats 32 times for each bit of orignal problem.
 - make use of nice binary prpoerty that XOR, left-move, right-move, ....

 ##Corner Case
 ---------
 - minus numbers

2nd
=======
- state machine: use `ones` and `twos` to record the counting of each bit 
- remove bit in ones and twos when a num emerges three times

