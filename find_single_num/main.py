'''it uses sort,  a O(nlogn) solution'''
class Solution:
    # @param A, a list of integer
    # @return an integer
    def singleNumber(self, A):
        if len(A) == 0 or len(A) == 1 or len(A) ==2:
            return 0
        else:
            # A = self.MergeSort(A,0,len(A)-1)
            self.QuickSort(A, 0, len(A)-1)
            i = 0
            while i < len(A)-1:
                if A[i] == A[i+1]:
                    i = i + 2
                    continue
            	else:
            		return A[i]
            return A[i]  # if i = len(A), then i is the one

    def MergeSort(self, A, start, end):
    	if end < start:
    		print "Error"
    		return None
    	if end == start:
    		return [A[start]]
    	else:
    		mid = (start + end)/2 # get the floor
    		left_half = self.MergeSort(A,start, mid)
    		right_half = self.MergeSort(A, mid+1, end)
    		output = [];
    		p = 0
    		q = 0
    		while  (p <len(left_half) and q < len(right_half)):
    			if left_half[p] <= right_half[q]:
    				output.append(left_half[p])
    				p= p + 1
    			else:
    				output.append(right_half[q])
    				q = q+1
    		if p == len(left_half):
    			output = output + right_half[q:]
    		else:
    			output = output + left_half[p:]
    		return output

    def QuickSort(self, A, start, end):
    	# other case Quicksort does not work
    	# quicksort is in-place sort, so no return
    	# and no all function use relative index!!
    	if start < end:
	    	pivot_position = self.Partition(A,start,end)
	    	low_half = self.QuickSort(A, start, pivot_position-1)
	    	high_half = self.QuickSort(A, pivot_position+1, end)


    def Partition(self, A, start, end):
    	pivot = A[end];
    	i = start
    	for j in range(start, end+1):
    		if A[j] < pivot: # ex. A[j] and A[i]
    			temp = A[j]
    			A[j] = A[i]
    			A[i] = temp
    			i = i + 1   # i is the position index
 		# ex. pivot and A[i]
 		temp = A[i]
    	A[i] = A[end]
    	A[end] = temp
    	return i
        
if __name__ == '__main__':
	soltion = Solution()
	A = [9,8,8,9,5,20,2,1,1,2,4,4,5]
	ai = soltion.singleNumber(A)
	print ai
