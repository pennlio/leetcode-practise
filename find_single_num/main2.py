class Solution:
	def singleNumber(self,A):	#find single number among two-time repelicates
		if len(A) == 0:
			return 
		elif len(A) ==1:
			return A[0]  #here is A[0]
		else:
			sum = A[0]
			for i in range(1, len(A)):
				sum = sum^A[i]
		return sum

if __name__ == '__main__':
	soltion = Solution()
	A = [9,8,8,9,5,20,2,1,1,2,4,4,5] #output 20
	A = [10,2,2,1,1,10,0,8,9,9,8] # output 0
	A = [2,1,1,2,3] #output 3
	print soltion.singleNumber(A)