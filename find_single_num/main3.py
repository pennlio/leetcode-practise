
class Solution(): #find single number among 3-time replicates

 # method 1: use binary counting: can only deal with int.

	def singleNumber(self,A):
		n = len(A)
		if n==0:
			return A[0]
		else:
			result = 0
			for j in range(32): # assume each integer is in 32-bits
				mask = 1 << j  
				sum = 0
				for i in range(n):
					if (A[i] >> j) & 1:
						sum = sum + 1 #calculate time of emergence of each digit
					# sum = (A[i] & mask) >> j + sum  #calculate bit by bit sum
				output = sum%3
				result = (output << j) | result
			# if result >> 31 == 1:  #a minus number
				# result = (result)^(2^(31)-1)
				# return result
			# else:	
			return result

#  method 2: use hash table by dictionary : deal with int and chars

	# def singleNumber(self, A):
	# 	n = len(A)
	# 	if n==0:
	# 		return
	# 	elif n==1:
	# 		return A[0]
	# 	elif n < 4:
	# 		return  #base case 
	# 	else:
	# 		count_dic = {}
	# 		for i in range(n):
	# 			try:
	# 				count = count_dic[A[i]] #use dictionary in python as hash table
	# 			except KeyError, e:
	# 				count_dic[A[i]] = 1
	# 			else:
	# 				count_dic[A[i]] += 1
	# 		for k,v in count_dic.items():
	# 			if v==1:
	# 				return k

if __name__ == '__main__':
	sl = Solution()
	a = [1,2,3,2,2,1,1,4,4,4,4,4,4] # anwser is 3
	a = [1,1,1,2,2,2,0,0,0,4,4,4,9] # anwser is 9
	a = [0] #anwser is 0
	print sl.singleNumber(a)