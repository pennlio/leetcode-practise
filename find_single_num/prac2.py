class Solution:
    # @param A, a list of integer
    # @return an integer
    '''Goal find bits that appear 3K+1 times'''
    def singleNumber(self,A):
        ones = 0
        twos = 0 # use ones, twos to record bits that appear one time and two times, remove any bits in one and two that appear 3 times
        # three_mask = 0
        for x in A:
            twos = twos|(ones & x) # add to twos
            ones ^= x # add ones
            three_mask = ~(ones&twos)
            ones &= three_mask
            twos &= three_mask
        return ones 

if __name__ == '__main__':
    sl = Solution()
    A = [3,4,4,4,-1,3,3]
    print sl.singleNumber(A)