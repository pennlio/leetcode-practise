#debug: https://oj.leetcode.com/discuss/5122/how-does-python-work-on-bits

class Solution:
    def singleNumber(self, A):
        ''' cannot handle negative numbers'''
        if not A: return 0
        result = 0
        for j in range(32):
            mask = 1 << j # move mask right to j digits
            cnt = 0
            for x in A:
                if (x & mask) >> j:
                    cnt += 1
            if cnt % 3 == 1:
                output = 1 << j # Move
                result = result | output # OR
        return result

if __name__ == '__main__':
    sl = Solution()
    A = [1,2,3,3,3,2,2,1,-4,1]
    print sl.singleNumber(A)



