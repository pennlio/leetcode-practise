class Solution:
    def singleNumber(self, A):
        # we need a state transfer machine:
        # from (bit2, bit1)  0,0->0,1->1,0->0,0
        bit1=0
        bit2=0
        for i in A:
            oldbit1=bit1
            bit1=bit1^(i&(~bit2))
            bit2=(oldbit1&i)|(bit2&(~i))

        return bit1

if __name__ == '__main__':
    sl = Solution()
    A = [1,2,3,3,3,2,2,1,-4,1]
    print sl.singleNumber(A)    