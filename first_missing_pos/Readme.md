concept:
--------
- for A with k integers, first missing must be within [1, k+1], k numbers
- there are exactly k space in A to store such information (swap )
- O(n) when sort is needed, emulate the counting sort !!!

imple:
------
- swap in python: a, b = b, a; pay attention when a and b are correlated
- while a and b: if b refers a, then a must be put first (checked first)

corner case:
--------
- input is [], return 1