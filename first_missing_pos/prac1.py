class Solution:
    # @param A, a list of integers
    # @return an integer
    def firstMissingPositive(self, A):
        if not A:
            return 1
        n = len(A)
        # for i in range(n): # cannot use for, since index will be mdofied during oper
        i = 0
        while i < n:
            if 1 <= A[i] <= n and A[A[i]-1] != A[i]:
                A[A[i]-1], A[i] = A[i], A[A[i]-1]  # swap in python, order counts since two vars are related!!
                i = 0 # restore i
            else:
                i += 1
        i = 0
        while i < n and A[i] == i+1:  # must be A[i] followed by i <n; o/w A[i] causes error
            i += 1
        return i + 1 # also compatable with A= [1,2,3]

    def swap(self, A, i ,j):
        temp = A[i]
        A[i] = A[j]
        A[j] = temp
        return

if __name__ == '__main__':
    sl = Solution()
    # A = [-2,3,-1,1,2,0,4,4,5,6]
    A = [1]
    # A = [1,2,0]
    # A = [3,4,-1,1]
    print sl.firstMissingPositive(A)