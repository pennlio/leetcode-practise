flatten BT
========
concept:
--------
- flatten the whole tree is composed of two parts: flattern left first, then right !
- then use recursion to deal with that recursively level by level


imple:
-----
- need itertae to the deepest node to transplant right to flattened left

improvement:
-------
- `move entire tree to right` can be improved
- learn the `generator` function?? 

2nd
======
concept:
-------
- for each node, must flatten left first; but before that stack its right
- when left is all done, then go back recursively deal with right
- avoid move entire tree from left to right; since the moving is executed at each step

