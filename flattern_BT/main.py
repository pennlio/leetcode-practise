# Definition for a  binary tree node
# '''time exceed: O(n^2)'''
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    # @param root, a tree node
    # @return nothing, do it in place
    # def flatten(self, root):
    def flatten(self, root):
        if not root:
            return None
        else:
            self.flatten(root.left)
            rl = root.left
            if rl:
                while rl.right:
                    rl = rl.right # go to the deepest place of rl
                rl.right = root.right # attach right branch to left branch
                root.right = root.left # move entirely to right branch
                root.left = None
            self.flatten(root.right) # flatten right branch
        return

if __name__ == '__main__':
    sl = Solution()
    root = TreeNode(1)
    root.left = TreeNode(2)
    root.right = TreeNode(5)
    root.left.left = TreeNode(3)
    root.left.right = TreeNode(4)
    root.right.right = TreeNode(6)

    sl.flatten(root)
