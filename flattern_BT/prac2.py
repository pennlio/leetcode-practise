# Definition for a  binary tree node
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    # @param root, a tree node
    # @return nothing, do it in place
    def __init__(self):
        self._stack = []
    def flatten(self, root):
        if not root: return root
        p1 = root
        self._stack.append(p1.right) # save node's right and then search left
        while p1.left: # search to the left deepest node
            p1.right = p1.left # move left to right
            p1.left = None
            p1 = p1.right 
            self._stack.append(p1.right)
        while self._stack: # recursively flatten save nodes
            p0 = self._stack.pop()
            if p0: # only concatentate no-None nodes
                p1.right = self.flatten(p0)
        return root
        
if __name__ == '__main__':
    sl = Solution()
    root = TreeNode(1)
    # root.left = TreeNode(2)
    root.right = TreeNode(2)
    # root.left.left = TreeNode(3)
    # root.left.right = TreeNode(4)
    # root.right.right = TreeNode(6)
    root.right.left = TreeNode(3)
    sl.flatten(root)