#!/bin/python

class Solution():
    def filpCoin(self, A):
        A = [0] + A + [0] # add sentinel
        B = [0]* len(A)
        result, buf = 0, []
        for i in xrange(1,len(B)):
            if A[i-1] == 0 and A[i] == 1:  # start counting
                buf.append(i) # starting pos
            elif A[i-1] == 1 and A[i] == 0: #end counting
                B = self.fillB(B, buf.pop(), i)
        B = B[1:len(B)-1] # only take final part
        r = B[0] # at least from the start
        print B
        for i in xrange(len(B)):
            if B[i]!= 0:
                continue
            count = 0
            if i > 0:
                count += B[i-1]
            if i < len(B)-1:
                count += B[i+1]
            r = max(r, count+1)
        return r

    def fillB(self, B, start, end): # fill B's correspondent postions with count value
        for i in xrange(start, end):
            B[i] = end - start
        return B

if __name__ == '__main__':
    # A = [0,0,1,1,0,1,0,0,1,1,1,0,1]
    # A = [0,1,0,1,0,0,1,1,0,0,1,1,0]
    A = [1,1,0,1,0,0]
    # A = [0,0]
    sl = Solution()
    print sl.filpCoin(A)






