def solution(A):
    n = len(A)
    count = 0
    found = False

    for i in xrange(n-1):
        if A[i] == A[i+1]:
            count += 1
        elif ((i+2) < n - 1) and not found:
            if (A[i] == 0 and A[i+1] == 1 and A[i+2] ==0) or (A[i] == 1 and A[i+1] == 0 and A[i+2] == 1):
                found = True
                count += 2
            elif (A[i] == 1 and A[i+1] == 0 and A[i+2] == 0) or (A[i] == 0 and A[i+1] == 1 and A[i+2] == 1) :
                found = True
                count += 1
    return count


def main():
    # A = [0,0,1,1,0,1,0,0,1,1,1,0,1]
    # A = [0,1,0,1,0,0,1,1,0,0,1,1,0]
    # A = [0,1]
    # A = [0,0,0,1,1,1,0,1]
    # A = [1,1,0,1,0,0]
    A = [0,0,1]
    # A = [0,1,0,1,0,0,1,1,0,0,1,1,0]
    print solution(A)

if __name__ == '__main__':
    main()