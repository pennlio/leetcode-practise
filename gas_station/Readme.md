concept:
--------
- if sum(gas) >= sum(cost), then must have solution
- how to find the solution? search gap = gas - cost
- Merge cells as go thru the cycle: by iteration: update gap O(n) solution
- Idea: build from base case: if there are only two stations, then it is easy to find out the start point; so we can merge the stations consecutively in pairs: four situations : 
- (+ +); 
- (- -); 
- (+ -): 
- (- +); 
- only the `- +` case cannot be merged since any one travled to others except `- +` case can cointinue his travel (or stop) travle at least at the first station in the pair.

- A final solution can go through any parts of the cycle.  so try to merge such sections and finally we have



-imple
--------
- record merged gaps: pad with 0: means that station is trivial 
- O(n) time since linear search