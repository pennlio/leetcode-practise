class Solution:
    # @param gas, a list of integers
    # @param cost, a list of integers
    # @return an integer
    def canCompleteCircuit(self, gas, cost):
        if not gas or not cost:
            return -1
        if sum(gas) < sum(cost):
            return -1 # not enough fuel
        gap = []
        n = len(gas)

        for i in range(n):
            gap.append(gas[i] - cost[i])

        for i in range(n):
            if gap[i] < 0:
                continue
            else:
                leftover = 0
                prev = i
            for j in range(i+1, i+n+1):
                modj = j%n
                if leftover < 0:
                    break
                else:
                    leftover += gap[prev] + gap[modj]
                    prev = modj
                if modj == i: # finish one loop
                    return i
        return -1

'''TLE O(n^2)'''

if __name__ == '__main__':
    sl = Solution()
    gas = [2,3,1,9,2]
    cost = [2,4,2,8,2]
    print sl.canCompleteCircuit(gas,cost)