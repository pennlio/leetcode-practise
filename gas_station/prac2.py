class Solution:
    # @param gas, a list of integers
    # @param cost, a list of integers
    # @return an integer
    def canCompleteCircuit(self, gas, cost):
        if not gas or not cost:
            return -1
        if sum(gas) < sum(cost):
            return -1 # not enough fuel
        n = len(gas)
        if n == 1:
            return 0

        gap = []
        for i in range(n):
            gap.append(gas[i] - cost[i])

        i = 0
        count = n-1 #combine n-1 times 
        while i < n:
            if gap[i] == 0 :
                i += 1
                continue
            j = (i + 1)%n
            while j <= n-1:
                if gap[j] == 0:
                    j = (j+1)%n
                    continue  # if gap[j] is 0, skip
                if gap[i] < 0 and  gap[j] > 0: # this is a break point, cannot continue
                    break
                else: # merge
                    count -= 1
                    gap[i] = gap[j] + gap[i]
                    gap[j] = 0
                    if count == 0: # combine all
                        return i
            i += 1
        # return gap

if __name__ == '__main__':
    sl = Solution()
    gas = [5]
    cost = [4]
    gas = [2,6,1,9,2,5,2]
    cost = [8,1,3,3,9,1,1]
    gas = [1,2,3,3]
    cost = [2,1,5,1]

    # gas = [6,1,9,2,5,2]
    # cost = [1,3,3,9,1,8]
    print sl.canCompleteCircuit(gas,cost)
