Readme.md
===========
concept:
-------
- f(n): total styles of n without flat 
- g(n): total styles with n
- make recursive calls to calculate f(n) and g(n) !!! very complicated!


M2: concept:
------
- build parenthesis on the go
- realize: num of '(' must be >= num of ')'
- use buf to store up-to-date generated prthsis string
- finally return the fully generated [''] after n pairs are used out


imple:
----------
- return [''] or ''? when one string is fully built, return [''] in order to concatenate with returned strings from other branches