class Solution:
    # @param an integer
    # @return a list of string
    ''' double count some cases; and wrong result'''
    def generateParenthesis(self, n):
        if not n:
            return []
        else:
            return self.generateGn(n)


    def generateGn(self, n): # all parenthesis
        if n == 0:
            return []
        return ['()'*n] + self.generateFn(n)

    def generateFn(self, n): # non-flat parenthesis
        if n <= 1:
            return []
        elif n == 2:
            return ['(())']
        else:
            fn = []
            gn_1 = self.generateGn(n-1)
            fn_1 = self.generateFn(n-1)
            for i in range(len(gn_1)):
                fn.append('(' + gn_1[i] + ')')   # modify gn_1 result
            for i in range(len(fn_1)):
                fn.append('()' + fn_1[i])  
                fn.append(fn_1[i] + '()')
            return fn

if __name__ == '__main__':
    sl = Solution()
    n = 1
    print sl.generateParenthesis(n)