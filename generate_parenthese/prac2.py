class Solution:
    # @param an integer
    # @return a list of string
    def generateParenthesis(self, n):
        if n == 0:
            return []
        else: 
            buf = ''
            return self.buildParentheis(buf, 0,0,n)

    def buildParentheis(self, buf, lcnt, rcnt, n):
        if lcnt == rcnt == n:
            return [buf] # succesfully generate one case
        left, right = [], []
        if lcnt == rcnt:
            left = self.buildParentheis(buf+'(', lcnt+1, rcnt, n)
        elif lcnt == n:
            right = self.buildParentheis(buf+')', lcnt, rcnt+1, n)
        else:
            left = self.buildParentheis(buf+'(', lcnt+1, rcnt, n)
            right = self.buildParentheis(buf+')', lcnt, rcnt+1, n)
        return left + right

if __name__ == '__main__':
    sl = Solution()
    n = 5
    print sl.generateParenthesis(n)
    print len(sl.generateParenthesis(n))
