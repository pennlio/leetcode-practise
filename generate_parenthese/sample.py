class Solution:
    '''learn from others'''
    # @param an integer
    # @return a list of string
    def helper(self, current, opened, closed, n):
        if n == 0:
            return []
        if opened == closed == n:
            return [current]
        o, c = [],[]
        out = []
        if opened < n:
            o = self.helper(current + '(', opened + 1, closed, n)
        if opened > closed:
            c = self.helper(current + ')', opened, closed + 1, n)
        return o + c

    def generateParenthesis(self, n):
        return self.helper('', 0, 0, n)

if __name__ == '__main__':
    sl = Solution()
    n = 4
    print len(sl.generateParenthesis(n))