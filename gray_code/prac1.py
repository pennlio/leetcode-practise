class Solution:
    # @return a list of integers
    def grayCode(self, n):
        if n == 0:
            return [0]
        if n == 1:
            return [0,1]
        else:
            a = self.grayCode(n-1)
            b = [x + pow(2,n-1) for x in a]
            b.reverse()
            return self.grayCode(n-1) + b

if __name__ == '__main__':
    sl = Solution()
    n = 5
    print sl.grayCode(n)