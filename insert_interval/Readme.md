Readme.md
========
concept:
----------
- `two feet` solution: extend the start and end position of the newinterval if possible,  or just insert as new


imple:
--------
- sort intervals: 
    sorted(intervals, key = lambda Interval: Interval.start)
- output merged intervals: use index to skip; avoid stacks

improve:
-------
- debug for one pass impelmetation; compare just end points as sample soluton
