# Definition for an interval.
class Interval:
    def __init__(self, s=0, e=0):
        self.start = s
        self.end = e

class Solution:
    # @param intervals, a list of Intervals
    # @param newInterval, a Interval
    # @return a list of Interval
    def insert(self, intervals, newInterval):
        if not intervals:
            return [newInterval]
        if newInterval.end < intervals[0].start:
            intervals.insert(0,newInterval)
            return intervals
        elif newInterval.start > intervals[-1].end:
            intervals.append(newInterval)
            return intervals
        else: # need merge
            for i in range(len(intervals)):
                if intervals[i].start <= newInterval.start <= intervals[i].end:
                    newInterval.start = intervals[i].start
                    intervals.insert(i,newInterval)
                    break
                elif newInterval.start < intervals[i].start: # else output the first place to insert
                    intervals.insert(i,newInterval)
                    break
            
            for j in range(len(intervals)):
                if intervals[j].start <= newInterval.end <= intervals[j].end:
                    newInterval.end = intervals[j].end

            tail = 0
            for j in range(i, len(intervals)):
                if intervals[j].start > intervals[i].end:
                    tail = 1
                    break

            return intervals[:i+1] + intervals[j:] if tail else intervals[:i+1]




    def sortInterval(self, intervals):
        rank = sorted(intervals, key=lambda Interval: Interval.start)
        return rank


if __name__ == '__main__':
    sl = Solution()
    intervals = [Interval(1,5)]
    intervals = sl.sortInterval(intervals)
    newInterval = Interval(2,3)
    intervals = sl.insert(intervals, newInterval)
    for x in intervals:
        print x.start, x.end
    print "==="
    # for x in ansIntervals:
    #     print x.start, x.end

        