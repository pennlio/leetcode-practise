class ListNode:
    def __init__(self, value):
        self.val = value
        self.next = None
        # self.next =

class Solution:
# @param head, a ListNode
# @return a ListNode
    def insertionSortList(self, head):
        if head == None or head.next == None:
            return head
        pre_head  = ListNode(-1)
        pre_head.next = head  #use a pre_head to
        p = head.next
        q = head
        while p is not None:
            if p.val >= q.val:
                p=p.next
                q=q.next
            else:
                q.next = p.next  # q.next is next p
                r = pre_head
                while r.next is not None and r.next.val <= p.val:
                    r=r.next
                p.next = r.next
                r.next = p
                p = q.next
        return pre_head.next

    def buildList(self, value_array):
        if len(value_array) == 0:
            return None
        else:
            value = value_array
            head = ListNode(value[0])
            latest_node = head
            for i in range(1, len(value)):
                new_node = ListNode(value[i])
                latest_node.next = new_node
                latest_node = new_node
        return head

    def printList(self, head):
        p = head
        while p!=None:
            print p.val
            p = p.next

if __name__ == '__main__':
    sl = Solution()
    value = [2,7,3,1,9,4]
    head = sl.buildList(value)
    sl.printList(sl.insertionSortList(head))
