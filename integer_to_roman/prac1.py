class Solution:
    # @return a string
    def intToRoman(self, num):
        if num < 1 or num > 3999:
            return 'Error'
        romanDict = {} # init
        romanDict[1] = 'I'
        romanDict[5] = 'V'
        romanDict[10] = 'X'
        romanDict[50] = 'L'
        romanDict[100] = 'C'
        romanDict[500] = 'D'
        romanDict[1000] = 'M'
        mod = 10
        output = ''
        digit = 1
        while num > 0:
            rem = num % mod
            if 1 <= rem <= 3:
                result = rem*romanDict[digit]
            elif rem == 4 :
                result = romanDict[digit*5] + romanDict[digit]
            elif rem == 5:
                result = romanDict[digit*5]
            elif 6 <= rem <= 8:
                result = (rem-5)*romanDict[digit] + romanDict[digit*5]  # reverse way since finally will reverse again
            elif rem == 9:
                result = romanDict[digit*10] + romanDict[digit]
            else: # rem = 0
                result = ''
            output += result
            num //= mod
            digit *= 10
        return output[::-1]


if __name__ == '__main__':
    num = 399
    sl = Solution()
    print sl.intToRoman(num)