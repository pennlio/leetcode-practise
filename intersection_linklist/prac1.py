'''01/02/2015'''
'''cannot do it with reverse'''
# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    # @param two ListNodes
    # @return the intersected ListNode
    def getIntersectionNode(self, headA, headB):
        if not headA or not headB:
            return None
        p1 = headA
        p2 = headB
        p3 = reverseLinklist(p2)
        while p1!=None:
            p1 = p1.next
        p2 = reverseLinklist(p3) # reverse back p2
        if p1 == headB: #means p1 and p2 joins
            return 

        # if p1

    def reverseLinklist(self, headA):
        if not headA or headA.next == None:
            return headA
        p0 = headA
        p1 = headA.next
        p0.next = None  ## !!! headA.next = None !!!
        while p1.next != None:
            p2 = p1.next
            p1.next = p0
            p0 = p1
            p1 = p2
        p1.next = p0 # link the end of array
        return p1

    def printLinklist(self, headA):
        if not headA:
            return None
        p1 = headA
        while p1 != None:
            print p1.val
            p1 = p1.next

    def buildList(self, value_array):
        if len(value_array) == 0:
            return None
        else:
            value = value_array
            head = ListNode(value[0])
            latest_node = head
            for i in range(1, len(value)):
                new_node = ListNode(value[i])
                latest_node.next = new_node
                latest_node = new_node
        return head
if __name__ == '__main__':
    sl = Solution()
    headA = sl.buildList([1,2,3])
    headA = sl.reverseLinklist(headA)
    # sl.printLinklist(headB)
    sl.printLinklist(headA)




