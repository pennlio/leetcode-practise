# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution:
    # @param two ListNodes
    # @return the intersected ListNode
    def getIntersectionNode(self, headA, headB):
        if headA == None or headB == None:
            return None
        p1, p2 = headA, headB
        lena, lenb = 0, 0
        while p1 is not None:
            lena += 1
            p1 = p1.next

        while p2 is not None:
            lenb += 1
            p2 = p2.next
        # align lena and lenb

        p1, p2 = headA, headB
        if lena > lenb:
            for i in range(lena - lenb):
                p1 = p1.next
        elif lenb > lena:
            for i in range(lenb - lena):
                p2 = p2.next

        while p1!= p2: # finally have at least p1==p2==None
            p1 = p1.next
            p2 = p2.next
        return p1
