1st
========

concept:
--------
- use DP and recursion; iterate all possible jumping cases of the input


imple:
--------
- TLE
- O(n^2)


2dn
========

concept:
--------
- killer rule: only '0' element can stop jumper going forward
- scan and when met '0', look backwards to find out the farthest point jumper can reach and check if it passes the '0'.
- only focus on going forward (going to the farthest point as possbile)
