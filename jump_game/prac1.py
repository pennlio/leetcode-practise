class Solution:
    # @param A, a list of integers
    # @return a boolean
    '''too much recursion'''
    def __init__(self):
        self._hash = {}
    def canJump(self, A):
        if not A:
            return False
        return self.jump(A,0)

    def jump(self, A, i):
        if i >= len(A)-1:
            return True
        elif A[i] == 0:
            return False
        else: # jump
            if i not in self._hash:
                for x in range(A[i], 0, -1):
                    if self.jump(A, i+x): # recursively jump
                        self._hash[i] = True
                        return self._hash[i]
                self._hash[i] = False
            return self._hash[i]

if __name__ == '__main__':
    sl = Solution()
    A = [2,3,1,1,4]
    # A = [3,2,1,0,4]
    A = [3,3,3,3,3,3,3,3,3,0,3,3]
    print sl.canJump(A)
