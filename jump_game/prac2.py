class Solution:
    # @param A, a list of integers
    # @return a boolean
    def canJump(self, A): # O(n) solution
        if not A: return False
        stack = [-1] # init stack
        i = 0
        while i < len(A)-1:
            if A[i] == 0:
                maxJump = 0 # step-back to seek for max Jump
                for j in range(i-1, stack[-1],-1): # only backtrack to last 0
                    if A[j]+j > maxJump:
                        maxJump = A[j]+j
                        jumpPoint = j
                if maxJump > i: # can pass this zero !!!! WRONG ADD MAX REACHABLE I
                    stack.append(i)
                    i = maxJump # start from maxJump point
                    continue
                return False # o/w cannot pass this zero
            i += 1
        return True

if __name__ == '__main__':
    sl = Solution()
    A = [2,3,1,0,0,0]
    # A = [5,0,0,0]
    # A = [0,3,1]
    # A = [3,2,1,0,4]
    print sl.canJump(A)
        