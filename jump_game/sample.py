class Solution:
# @param A, a list of integers
# @return an integer
def trap(self, A):
    if len(A) < 3:
        return 0
    lo, hi = 0, len(A)
    ground, rain = 0, 0
    while lo < hi:
        if A[lo] <= ground:
            rain += (ground - A[lo])
            lo += 1
        elif A[hi-1] <= ground:
            rain += (ground - A[hi-1])
            hi -= 1
        else:
            ground = min(A[lo], A[hi-1])
    return rain