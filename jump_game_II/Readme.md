1st
=======

concept:
--------
- recursive DP

impel:
------
- too much recursive calls



2nd
======

concept:
------
- killer rule: jumper always finds the farthest jump available to save jumping times
- greedy search the reachable region of i' position, find the farthes reacheable pos. as next jump point

imple:
------
- O(n)