class Solution:
    # @param A, a list of integers
    # @return an integer
    def __init__(self):
        self._hash = {}
    def jump(self, A):
        '''dp method, too much recursion'''
        if not A: 
            return -1
        else:
            return self.jumpCount(A,0)
    def jumpCount(self, A, start):
        if start >= len(A)-1: # reached the end
            return 0
        if start not in self._hash:
            if A[start] == 0: 
                self._hash[start] = float('inf')
                return float('inf')
            minCnt = float('inf')
            for x in range(1, A[start]+1):
                thisCnt = self.jumpCount(A,start+x)
                if thisCnt == float('inf'): continue # invalid, skip
                minCnt = min(minCnt, thisCnt)
            self._hash[start] = minCnt + 1
        return self._hash[start] # 1 means current step

if __name__ == '__main__':
    sl = Solution()
    A = [2,3,1,1,4]
    A = [1,1,2,1,1]
    A = [3,3,0,0,0]
    A = [1]
    A = [6,2,6,1,7,9,3,5,3,7,2,8,9,4,7,7,2,2,8,4,6,6,1,3]
    print sl.jump(A)
