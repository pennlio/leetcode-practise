class Solution:
    # @param A, a list of integers
    # @return an integer
    def jump(self, A):
        '''greedy O(n)'''
        if not A: return 0
        cnt = i = 0
        while True:
            nextJumpPoint = i
            maxJump = 0
            if i >= len(A)-1 : return cnt
            if A[i] + i >= len(A) - 1: return cnt+1 # add 1 for final jump
            if A[i] == 0: return float('inf') # cannot reach
            for x in range(1,A[i]+1):
                if x+A[i+x] >  maxJump: # find out the maxJump as next jump
                    maxJump = x + A[i+x]
                    nextJumpPoint = x+i
            i = nextJumpPoint
            cnt += 1
        return cnt

if __name__ == '__main__':
    sl = Solution()
    A = [2,3,1,1,4]
    # A = [3,3,0,0]
    # A = [0]
    A = [6,2,6,1,7,9,3,5,3,7,2,8,9,4,7,7,2,2,8,4,6,6,1,3]
    print sl.jump(A)
