class Solution:
    def largestValue(self, num, k):
        if k <= 0: return []
        if len(num) <= k : return num
        import heapq
        heap = num[:2]
        heapq.heapify(heap)
        for x in num[2:]:
            if x > heap[0]:
                heapq.heappushpop(heap, x)
        return heap

if __name__ == '__main__':
    sl = Solution()
    k = 2
    # num = [-1,3,4,0,-2,5,9]
    num = [-1,0]
    print sl.largestValue(num,k)
