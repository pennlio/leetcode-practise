# given a array of float number, a target float t and an integer k;
# verify if there are k num in the array that combines to be at most t;
# programming pearls 2.8

import heapq
class Solution:  
    def kMinSum(self, num, target, k):
        if not num:
            return False
        heap = [] # init an empty heap
        for i in range(len(num)):
            if i <= k-1:
                heapq.heappush(heap,-num[i]) # build max-heap by store -num
            else:
                if -num[i] > heap[0]:
                    heapq.heappushpop(heap, -num[i])
        print heap
        return True if sum(heap) >= -target else False
        # heapq
def main():
    num = [1,7,5,16,18,10]
    k = 3
    target = 12
    sl = Solution()
    print sl.kMinSum(num,target,k)
if __name__ == '__main__':
    main()