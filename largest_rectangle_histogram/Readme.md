1st 
============
concept:
---------
- Recursion to shrink the size: too much recursion, use better way + DP

2nd
========== 
concept:
-------
D&C to dicuss different cases (inclusive)
- within [start, mid-1]
- within [mid+1, end]
- across mid 


imple:
-----------
- case 3: compare 3 cases each time: extend to left/right/both sides
- TLE, O(nlogn) with T(n) = 2T(n/2) + O(n)


3rd
===========
concept:
--------
- killer rule: the max area is among the maximal rects that can be formed by using each level in the array as the height


imple:
-------
- using stack to record the passed levels and matching the length of that level (height) 
- O(n) 
- sentinel [0]s: beginning 0 to avoid stack being empty; last 0 gurantee all levels larger than 1 is poped out
- any level is pushed in the stack when met; any level is poped out by any first lower level is met
- see sample code for example
