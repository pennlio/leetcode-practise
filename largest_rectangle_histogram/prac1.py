
class Solution:
    '''TLE for DP recursion'''
    # @param height, a list of integer
    # @return an integer
    def __init__(self):
        self._hash = {}
    def largestRectangleArea(self, height):
        if not height:
            return 0
        return self.calcMax(height, 0, len(height)-1)

    def calcMax(self, height, low, high):
        if low > high: return 0
        if low == high: return height[low]
        if (low, high) not in self._hash:
            # build up
            self._hash[(low,high)] = max(min(height[low:high+1])*(high-low+1), self.calcMax(height, low+1, high), self.calcMax(height, low, high-1))
        return self._hash[(low, high)]

        
if __name__ == '__main__':
    sl = Solution()
    height = [2,1,5,6,2,3]
    # height = [1,2,3]
    height = [1,1,1,1,1,1,2,2,2,2,2,1,1,1,1]
    height = [1,1,1,1,1,1]*100
    print sl.largestRectangleArea(height)