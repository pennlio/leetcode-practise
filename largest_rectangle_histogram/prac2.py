'''recursive D&C solution'''
class Solution:
    # @param height, a list of integer
    # @return an integer
    def largestRectangleArea(self, height):
        if not height:
            return 0
        return self.calcMax(height, 0 ,len(height)-1)

    def calcMax(self, height, start, end):
        if start > end:
            return 0
        elif start == end:
            return height[start]
        mid = (start + end)/2
        i = j = mid # two ptrs i and j traverse to start and end

        while True:
            cache = [-1]*3
            cur = self.calcArea(height,i,j)
            if i > start:            
                cache[0] = self.calcArea(height,i-1,j) # only left
            if j < end:
                cache[1] = self.calcArea(height,i,j+1) # only right
            if i > start and j < end:
                cache[2] = self.calcArea(height,i-1,j+1) # both
            max_cache = max(cache)
            if max_cache >= cur: # need move
                if max_cache == cache[0]:
                    i -= 1
                elif max_cache == cache[1]:
                    j += 1
                else:
                    i,j = i-1, j+1
            else:
                break # no move any more
        return max(cur, self.calcMax(height, start, mid-1), self.calcMax(height, mid+1, end))

    def calcArea(self, height, i, j): # calc area from i:j inclusive
        if i > j: return 0
        if i == j: return height[i]
        return min(height[i:j+1])*(j-i+1)

if __name__ == '__main__':
    sl = Solution()
    # height = [2,1,5,6,3]
    height = range(1,10000)
    # height = [1,1,1,1,1,1,1]
    # height = [1]*1/21
    # height = [1,2,3]
    print sl.largestRectangleArea(height)



