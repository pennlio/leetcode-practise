class Solution:
    # @param height, a list of integer
    # @return an integer
    def largestRectangleArea(self, height):
        # O(n) with sentinel elements
        if not height: return 0
        height = [0] + height + [0] # add sentinel
        stack = [0] # init stack
        maxArea = 0
        for i, x in enumerate(height):
            while x < height[stack[-1]]:
                curLevel = height[stack.pop()] # calculate current level
                maxArea = max(maxArea, curLevel*(i-stack[-1]-1)) #stack[-1] points to element smaller than curLevel
            stack.append(i)
        return maxArea




if __name__ == '__main__':
    sl = Solution()
    # height = [2,1,5,6,2,3]
    # height = [1,1,1,1,1]
    # height = [1]*20000
    print sl.largestRectangleArea(height)
        # if not stack:
        #     cur = stack.pop() # current value
        #     while len(stack)>0 or height[stack[-1]] >= cur:
        #             hidx = stack.pop()
        #     maxArea = max(maxArea, (i-hidx)*cur)
