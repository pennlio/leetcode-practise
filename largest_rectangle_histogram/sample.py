def solve(xs):
    xs = [0] + xs + [0] # first 0 to prevent stack from being empty set; last 0 to cal area of the true 'last' item, since 0 must pop any remaining element in the stack
    stack = [0] #stack stores the starting position of each value, so init it to be 0; also, since set xs[0] as a sentinel, so it guarantee stack cannot be empty no matter how small x it met (x is given at least >=0 )
    res = 0
    for i, x in enumerate(xs):  
        while x < xs[stack[-1]]: # an element can only be poped out by a lower element (that means it is the time to calculate the area with it as the base)
            y = xs[stack.pop()] 
            res = max(res, (i-1-stack[-1])*y) # how dose know 1. stack is not empty?  2. stack[-1] is the begining index
        stack.append(i)
    return res