1st
====
Concept:
-----
- `main2.py`: use `hash` table to record; and index to locate adj. elements; `delete` to ensure no double count

Corner case:
---------
- n = 1

Learn new
------
- `hash` map to reduce saving amount



2nd
=======
concept:
--------
- explore from the known samples, +1 and -1 two directions (that is why we make it O(n))
- use hashmap to store elements and provide O(1) access to element

imeple:
---------

- visted elements are labeled -1
- O(n)