LCS
class Solution:
	def longestConsecutive(self, num):
				# scan index_array
		n = len(num)
		if n == 0 : return None
		if n == 1: return 1
		else:
			index_array = self.markIndexArray(num)
			return self.scanIndexArray(index_array)

	def scanIndexArray(self, index_array):
		lcs_length = 0
		seq_count = 0
		in_seq = False # in seq or not
		for i in range(len(index_array)):
			if index_array[i] == False:
				if in_seq == False:
					continue
				else:
					if seq_count > lcs_length:
						lcs_length = seq_count
					seq_count = 0
			else:
				in_seq = True
				seq_count = seq_count + 1
				continue
		if in_seq == True:
			if seq_count > lcs_length:
				lcs_length = seq_count
				seq_count = 0
		return lcs_length



	def markIndexArray(self, num):
		maxi = max(num)  # linear time
		n = len(num)
		index_array = [False] * (maxi+1) # boolean array 
		for i in range(n):
			index_array[num[i]] = True
		return index_array


if __name__ == '__main__':
	sl = Solution()
	# num = [0,1]
	num = [2147483646,-2147483647,0,2,2147483644,-2147483645,2147483645]
	print sl.longestConsecutive(num)

