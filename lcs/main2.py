class Solution:
    def longestConsecutive(self, num):
        if len(num) == 0: return 0
        hasht = {} # hash
        for n in num:  # python code
            hasht[n] = True   # initialize hash table

        longest = 1  
        for n in num:
            current_length = 1
            prev = n-1
            while (prev in hasht):  # learn new phrase
                current_length += 1
                del hasht[prev]  # avoid double count
                prev -= 1

            next = n+1
            while  (next in hasht):
                current_length += 1
                del hasht[next]
                next += 1
            if current_length > longest:
                longest = current_length 
        return longest


if __name__ == '__main__':
    sl = Solution()
    # num = [0,1]
    num = [2147483643,-2147483647,0,2,2147483644,-2147483645,2147483645]
    print sl.longestConsecutive(num)

