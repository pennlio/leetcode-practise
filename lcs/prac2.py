class Solution:
    # @param num, a list of integer
    # @return an integer
    def __init__(self):
        self._hash = {}
    def longestConsecutive(self, num):
        if not num: return 0
        for i,x in enumerate(num):
            self._hash[x] = i  # build hash to check
        max_cnt = 1
        while num:
            x = y = num.pop()
            if self._hash[x] == -1: # visited
                continue
            self._hash[x] = -1 # label visited
            cnt = 1 # start from 1
            while x+1 in self._hash:
                if self._hash[x+1] >= 0: # upper half
                    self._hash[x+1] = -1
                    cnt+= 1
                    x += 1
            while y-1 in self._hash: # lower half
                if self._hash[y-1] >= 0:
                    self._hash[y-1] =-1
                    cnt += 1
                    y -= 1
            max_cnt = max(max_cnt, cnt)
        return max_cnt
            

        
        