README
========
use python string function split()

##Bugs:
- `split()` is a method, never omit `()`; this skip empty string
- more of `split(',')` (using ','), split('<>')... these do not skip empty string
- `x.join(words)`: use x to join separate items in `words`
- input is empty string`

##Corner case
--------
- input is empty or not empty but all spaces!!! -- split can handle them well!