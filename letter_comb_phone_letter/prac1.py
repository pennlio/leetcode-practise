class Solution:
    # @return a list of strings, [s1, s2]
    def letterCombinations(self, digits):
        if not digits:
            return ['']

        numDict = self.bulidDict()
        lastDigit = digits[-1]
        prevDigits = digits[:-1]
        preResult = self.letterCombinations(prevDigits)
        
        currentResult = []
        if lastDigit == '1':
            return preResult  # as if it does not exist
        else:
            for x in numDict[int(lastDigit)]: # find out correspondent letters
                if not preResult: # if prev result is empty set, directly add currentResult
                    currentResult.append(x)
                else:   # else append currentResult
                    for y in preResult:
                        currentResult.append(y+x)
        return currentResult
 
    def bulidDict(self):
        start = ord('a')
        dic = {} 
        # dic[1] = ''  # 1 means nothing, so skip
        dic[0] = ' '
        for i in range(2,10):
            if i == 7 or i == 9:
                gap = 4
            else:
                gap = 3
            dic[i] = [chr(x) for x in range(start,start+gap)]
            start += gap
        return dic

if __name__ == '__main__':
    sl = Solution()
    digits = '13'
    print sl.letterCombinations(digits)