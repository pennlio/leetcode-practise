#Linklist Cycle
=====

##concept: 
- two runner pointer, increasing until error
- when Attribute Error: list has end


##corner case:
1. `p2 = p1.next` does it exit or not?
2. `p2 = p2.next.next` exit or not?