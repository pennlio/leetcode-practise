class Solution:
    # @param head, a ListNode
    # @return a boolean
    def hasCycle(self, head):
    	if head == None or head.next == None:
    		return False
    	else:
	    	p1 = head
	    	p2 = p1.next  # two runners

	    	while p1 != p2:
	    		try:
	    		    p1 = p1.next
	    		    p2 = p2.next.next
	    		except AttirbuteError, e:
	    		    return False
	    		else:
	    			pass
	    	return True
