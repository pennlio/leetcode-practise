# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    # @param head, a ListNode
    # @return a list node
    def detectCycle(self, head):
        if not head:
            return None
        p1 = p2 = head

        while p2.next != None and p2.next.next != None:
            # until met
            p1 = p1.next
            p2 = p2.next.next
            if p2 == p1: #loop meeting node
                p2 = head # p2 go back to head
                while p2!=p1: # move x steps forward
                    p2 = p2.next
                    p1 = p1.next
                return p1 # must meet at node where cycle begins
        return None

def main():
    sl = Solution()
    head = ListNode(1)
    head.next = ListNode(2)
    head.next.next = ListNode(3)
    print sl.detectCycle(head)