class Solution:
    # @return a string
    def longestCommonPrefix(self, strs):
        if not strs:
            return ''
        commonStr = ''
        n = len(strs)
        minlen = float('inf')
        for i in range(n):
            minlen  = min(len(strs[i]), minlen)

        for j in range(minlen):
            for i in range(n-1):
                if strs[i][j] != strs[i+1][j]:
                    return commonStr
            commonStr += strs[i][j]
        return commonStr

if __name__ == '__main__':
    sl = Solution()
    strs = ['ad','add','']
    print sl.longestCommonPrefix(strs)