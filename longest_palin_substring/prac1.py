class Solution:
    # @return a string
    '''wrong output'''
    def longestPalindrome(self, s):
        if not s:
            return ''
        n = len(s)
        if n == 1:
            return s
        if n == 2:
            if s[0] == s[1]:
                return s
            else:
                return s[1]
        if n == 3:
            if s[0] == s[1] != s[2]:
                return s[:2]
        p2 = 2 # at least > 3
        maxlen = 0
        start = end = 0
        while p2 < n:
            if s[p2-1] == s[p2-2] == s[p2] : # three same
                p0 = p2 - 1
                while p0 >= p2-2:
                    (maxlen, start, end, p3) = self.findPalindrome(s,p0,p2,maxlen,start,end)
                    p0 -= 1 # try two symmetric axis
                p2 += 1
            elif s[p2-2] == s[p2]: # must in front of s[p2-1]
                p0 =p2-2
                (maxlen, start, end, p2) = self.findPalindrome(s,p0,p2,maxlen,start,end)
            elif s[p2-1] == s[p2]:
                p0 = p2-1
                (maxlen, start, end, p2) = self.findPalindrome(s,p0,p2,maxlen,start,end)
            else:
                p2 += 1
            # while p0 >= 0 and p2 <= n-1: # impossible to equal
            #     if s[p0] != s[p2]: 
            #         break
            #     p0 -= 1
            #     p2 += 1
            # if p2 - p0 - 1 > maxlen:
            #     start = p0 + 1
            #     end = p2 - 1
            #     maxlen = p2 - p0 - 1 # p2-p0+1-2 since p2, p0 move to unequal pos 
        return s[start:end+1]

    def findPalindrome(self, s, p0, p2, maxlen, start, end):
        n = len(s)
        while p0 >= 0 and p2 <= n-1: # impossible to equal
            if s[p0] != s[p2]: 
                break
            p0 -= 1
            p2 += 1
        if p2 - p0 - 1 > maxlen:
            start = p0 + 1
            end = p2 - 1
            maxlen = p2 - p0 - 1 # p2-p0+1-2 since p2, p0 move to unequal pos 
        return (maxlen, start, end, p2)


if __name__ == '__main__':
    sl = Solution()
    # s = 'dccccd'
    # s = 'ccdc'
    s = 'ababababababababababab'
    # s = 'sadfdccccd'
    # s = 'ccdddcc'
    print sl.longestPalindrome(s)

