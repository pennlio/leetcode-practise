class Solution:
    # @return a string
    def longestPalindrome(self, s):
        if not s: return ''
        # result = s[0]
        maxLength = 1
        start = 0
        for i in range(len(s)):
            if self.isPalin(s,i-maxLength,i): # i go backward not forward, in order not to miss preceding elements
                # result = s[i:i+len(result)+2]
                start = i-maxLength
                maxLength += 1
                # continue
            elif self.isPalin(s,i-maxLength-1,i):
                # result = s[i:i+len(result)+1]
                start = i-maxLength-1
                maxLength += 2
           
        return s[start:start+maxLength]
    
    def isPalin(self,s, i, j):
        if j > len(s)-1 or i < 0:
            return False
        while i<=j:
            if s[i] != s[j]:
                return False
            i+= 1
            j-= 1
        return True