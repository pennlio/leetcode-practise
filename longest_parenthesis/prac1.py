class Solution:
    # @param s, a string
    # @return an integer
    '''recursive: TLE exponetial time'''
    def longestValidParentheses(self, s):
        if not s:
            return 0
        n = len(s)
        if n == 1:
            return 0
        if self.isValid(s):
            return n
        else:
            return max(self.longestValidParentheses(s[1:]), self.longestValidParentheses(s[:-1]))

    def isValid(self, s):
        if not s:
            return True
        n = len(s)
        left = right = 0
        for x in s:
            if x == '(': 
                left += 1 
            else:
               right += 1
            if left < right:
                return False
        return True if left == right else False

if __name__ == '__main__':
    sl = Solution()
    s = ')(((((()())()())))()(()))('
    s=  ')((((()())()()))))()(()))('
    print sl.longestValidParentheses(s)