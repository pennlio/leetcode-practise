class Solution:
    # @param s, a string
    # @return an integer
    def longestValidParentheses(self, s):
        if not s:
            return 0
        n = len(s)
        buf = []
        count = [0]*len(s)
        curMax = 0
        for i in range(len(s)):
            if s[i] == '(':
                buf.append(i)
            else: # only adjust count when meet ')'
                if buf:
                    count[i] = 1
                    lastLefti = buf.pop()
                    count[lastLefti] = 1
                    curMax = max(curMax, self.countPar(count,i))
                # else:
                    # if recordi != lastLefti:  # lastLefti records the up-to-date i, if it does not change, means no '(' is added, go on
                         #calculate from last valid ')'
                        # count = 1
                        # recordi = lastLefti
                # not apppend in buf
        curMax = max(curMax, self.countPar(count,i)) #last item
        return curMax

    def countPar(self, count, thisi):
        p = thisi
        tCnt = 0
        while count[p] > 0 and p >= 0:
            tCnt = tCnt+count[p]
            p -= 1
        return tCnt


if __name__ == '__main__':
    sl = Solution()
    # s = ')(((((()())()())))()(()))('
    s=  ')((((()())()()))))()(()))('
    # s= '(()(())()'
    s = '(())())'
    s = '))))('
    s = '()'
    print sl.longestValidParentheses(s)