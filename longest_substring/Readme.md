Readme.md
concept:
--------
- two pointer check from the start
- once find dup char, chop off begin until to the first on dup char
- O(n)

imple:
------
- after iteration, compare the last string with maxlength
- not forget increment of while loop
- length = p1 - p2 not p1 - p2 +1