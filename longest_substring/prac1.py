class Solution:
    # @return an integer
    def lengthOfLongestSubstring(self, s):
        if not s:
            return 0
        n = len(s)
        if n == 1:
            return 1
        charHash = {}  # store met chrs

        maxlength = 0
        p1 = p2 = 0 # pointer
        while p1 < n:
            if s[p1] not in charHash:
                charHash[s[p1]] = 1
                p1 += 1
                continue
            else:
                length = p1 - p2
                maxlength = max(length, maxlength)  # update max length
                while s[p2] != s[p1]: # find first dup
                    del charHash[s[p2]]  # delete
                    p2 += 1
                p2 += 1  # move p2 to start of second substring
                p1 += 1
        length = p1 - p2
        maxlength = max(length, maxlength)  # comapre the last string
        return maxlength

if __name__ == '__main__':
    sl = Solution()
    s = 'sadadad'
    print sl.lengthOfLongestSubstring(s)

