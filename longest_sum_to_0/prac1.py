class Solution:
    def __init__(self):
        self._hash = {}
    def findSumtoZero(self, A):
        if not A: return (-1,-1)
        n = len(A)
        maxi = maxj = -1 # return -1 for not found
        for i in range(n):
            for j in range(i,n):
                if not self.calSum(A,i,j):
                    if j - i > maxj - maxi: # record max-length label
                        maxj = j
                        maxi = i
        return (maxi, maxj)

    def calSum(self, A, i, j): # DP
        if (i,j) not in self._hash:
            if i == j: 
                self._hash[(i,j)] = A[i]
            elif (i,j-1) in self._hash: # build up, so only two scenarios
                self._hash[(i,j)] = self._hash[(i,j-1)] + A[j] 
            elif (i-1,j) in self._hash:
                self._hash[(i,j)] = self._hash[(i-1,j)] - A[i-1]
        return self._hash[(i,j)]


if __name__ == '__main__':
    sl = Solution()
    # A = [-1,1,0,2,-1,0,-2,0,1]
    A = [0]
    print sl.findSumtoZero(A)