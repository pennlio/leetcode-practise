Majority elements
=====
concepts:
---------
- hash table
- O(n)


2nd
======
concept:
-------
- voting: iterate i from 0 to last, if x[i]==canditate, cnt++, else cnt --; when cnt==0, change candidate
- make use the fact that for major element, the averge cnt++ > 1/2 of total
- O(n)