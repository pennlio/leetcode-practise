'''12/24/2014'''
#!\bin\python
class Solution:
    # @param num, a list of integers
    # @return an integer
    def majorityElement(self, num):
        l = len(num)
        if l == 0: 
            return None
        elif l == 1: 
            return num[0]
        hl = l/2
        if l%2!= 0:
            hl += 1
        # print hl
        # build hash
        hashtable = {}
        for n in num:
            if n not in hashtable:
                hashtable[n] = 1
            else:
                hashtable[n] += 1
        for n in num:
            if hashtable[n] >= hl: # >= accroding to the test cases
                return n

if __name__ == '__main__':
    # num = [1,1,2,4,4,4,1,4]
    num = [3,3,4]
    sl = Solution()
    print sl.majorityElement(num)

