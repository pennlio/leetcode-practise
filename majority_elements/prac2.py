class Solution:
    '''learn from others, moore voting algo'''
    # @param num, a list of integers
    # @return an integer
    def majorityElement(self, num):
        counter = 0
        candidate = num[0]
        for i in xrange(len(num)):
            if num[i] == candidate:
                counter += 1
            else:
                counter -= 1
                if counter == 0: 
                    candidate = num[i] # filtered any that is not maj ele
                    counter = 1
        return candidate

if __name__ == '__main__':
    sl = Solution()
    num = [4,5,4]
    print sl.majorityElement(num)