# max depth BT
# Definition for a  binary tree node
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    # def __init__ (self):
        # self._maxdepth = 0
    # @param root, a tree node
    # @return an integer
    def maxDepth(self, root):
        if root == None:
            return 0
        else:
            if root.left != None:
                mxdl = self.maxDepth(root.left)
            else:
                mxdl = 0 # recursion finaly point
            if root.right != None:
                mxdr = self.maxDepth(root.right)
            else:
                mxdr = 0
        return max(mxdr,mxdl)+1


if __name__ == '__main__':
    sl = Solution()
    rootnode = TreeNode(3)
    rootnode.left = TreeNode(9)
    rootnode.right = TreeNode(20)
    rc = rootnode.right
    rc.left = TreeNode(15)
    rc.right = TreeNode(7)
    rc.right.right = TreeNode(29)
    rootnode.left.left = TreeNode(0)
    print sl.maxDepth(rootnode)