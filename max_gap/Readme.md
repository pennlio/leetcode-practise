concept:
----------
- try to use recursion as `Largest-Subarray-Sum`, which is T(n)=T(n-2)+O(n), making O(n^2); so no recursion
- try step descend from largest elem; not O(n), since the search space is much larger than sample size

- Radix sort (with linear time) and then iterate

impel:
---------
- radix sort: counting sort by each digit. 
- counting sort:

def countingSort(self, strNum, digit):
    buckets = [ [] for y in range(10)] # constr. buckets.
    for x in strNum:
        if len(x) <= digit:
            buckets[0].append(x)
        else:
            buckets[int(x[digit])].append(x)
    strNum = []
    for y in range(10):
        strNum.extend(buckets[y])
    return strNum
- take care the index in counting sort, do not mistake strNum for num;




- this does not change x:
for x in array:
    x = f(x) 



