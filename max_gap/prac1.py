class Solution:
    # @param num, a list of integer
    # @return an integer
    def maximumGap(self, num):
        if len(num)<2:
            return 0
        heighttbl = {}  # height table
        maxnum = -1
        maxgap = 0
        for x in num:
            if x > maxnum:
                maxnum = x
            heighttbl[x] = True
        currentStand = lastStand = maxnum
        while currentStand>0:
            if currentStand in heighttbl:
                if lastStand - currentStand > maxgap:
                    maxgap = lastStand - currentStand
                lastStand = currentStand
            currentStand -= 1
        return maxgap

'''not O(n), search space is much larger than sample size'''

if __name__ == '__main__':
    sl = Solution()
    num = [2,3,1,5,9]
    print sl.maximumGap(num)


        