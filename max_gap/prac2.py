class Solution:
    # @param num, a list of integer
    # @return an integer
    def maximumGap(self, num):
        n = len(num)
        if n < 2: return 0
        strNum = []
        # convert to string and reverse
        maxdigit = 1 # find the longest num
        for x in num:
            rstr = str(x)[::-1]
            strNum.append(rstr)
            maxdigit = max(maxdigit, len(rstr))
        for i in range(maxdigit):
            strNum = self.countingSort(strNum, i)
        # convert strNum back to num
        num = [int(x[::-1]) for x in strNum]
        maxgap = 0
        for u in range(len(num)-1):
            maxgap = max(maxgap, num[u+1]-num[u])
        return maxgap

    def countingSort(self, strNum, digit): # strNum is reversed
        # construct buckets
        buckets = [[] for y in range(10)]
        # check each strNum from lowest to highest digits
        for x in strNum:
            if len(x) <= digit:
                buckets[0].append(x)
            else:
                buckets[int(x[digit])].append(x)  
        # put each in app. bucket
        strNum = []
        #  sort:
        for y in range(10):
            strNum.extend(buckets[y])  # merge buckets
        return strNum

if __name__ == '__main__':
    sl = Solution()
    # num = [1,81,99]
    num = [1,10000000]
    print sl.maximumGap(num)

