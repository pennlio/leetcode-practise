class Solution:
# @param num, a list of integer
# @return an integer
    def maximumGap(self, num):
        if len(num) < 2:
            return 0
        strNum = []
        maxSize = 1
        # convert num to string list, work out max size of num
        for e in num:
            eStr = str(e)[::-1] # reversed string
            strNum.append(eStr)
            maxSize = max(maxSize, len(eStr))
        # radix sort
        for x in range(maxSize):
            buckets = [[] for y in range(10)]
            for e in strNum:
                if len(e) <= x:
                    buckets[0].append(e) # put num shorter than x digits in lowest bucket
                else:
                    buckets[int(e[x])].append(e) # sort according to xth digit
            strNum = []
            for y in range(10):
                strNum.extend(buckets[y])  # connect bucket to sort again
        num = [int(x[::-1]) for x in strNum]
        maxGap = 0
        for x in range(len(num) - 1):
            maxGap = max(maxGap, num[x + 1] - num[x])
        return maxGap

if __name__ == '__main__':
    sl = Solution()
    num = [14,51,22,69,86]
    num = [1,10000000]
    print sl.maximumGap(num)