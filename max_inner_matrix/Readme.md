concept:
--------
- Brute force with DP; a two dimension DP


impel:
--------
- the 2D DP can be better calculated with reducing cols and then summing across rows
- if overbound, return 0; o/w cannot add with int