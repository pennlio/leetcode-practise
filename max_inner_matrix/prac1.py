class Solution:
    # @ para: list of lists
    # @ return: integer
    def __init__(self):
        self._hash={}
    def maxMatrixSum(self, matrix):
        if not matrix: return None
        nRow = len(matrix)
        nCol = len(matrix[0])
        maxSum = -float('inf') # min-max
        for i in range(nRow):
            for j in range(i, nRow):
                for m in range(nCol):
                    for n in range(m, nCol):
                        maxSum = max(maxSum, self.calSum(matrix, i,j,m,n))
        return maxSum

    def calSum(self, matrix, i, j, m, n): # max matrix sum row i:j and column m:n    
        nRow = len(matrix)
        nCol = len(matrix[0])
        if i > j or m > n:
            return 0
        if i < 0 or j > nRow-1 or m < 0 or n >nCol-1: 
            return 0 # over bound
        if (i,j,m,n) not in self._hash:
            if i == j and m == n :
                self._hash[(i,j,m,n)] = matrix[i][m]
            elif i == j:
                self._hash[(i,j,m,n)] = self.calSum(matrix, i, j, m, n-1) + matrix[i][n]
            else:
                self._hash[(i,j,m,n)] = self.calSum(matrix, i, j-1, m, n) + sum(matrix[j][m:n+1])
        return self._hash[(i,j,m,n)]

if __name__ == '__main__':
    sl = Solution()
    # matrix = [[1,3,4],[-3,1,2],[1,-9,3]]
    matrix = [[0,0,1],[-1,0,1]]
    print sl.maxMatrixSum(matrix)


