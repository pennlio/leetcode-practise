# Definition for a point
import itertools
import math
class Point:
    def __init__(self, a=0, b=0):
        self.x = a
        self.y = b

class Solution:
    def __init__(self):
       pass
    # @param points, a list of Points
    # @return an integer
    # def maxPoints1(self, points):
    #     number_of_points = len(points)
    #     # print number_of_points
    #     if number_of_points == 0:
    #         return 0
    #     elif number_of_points ==1:
    #         return 1
    #     else:
    #         line_count_dictionary = {}
    #         for i in range(len(points)):
    #             points[i] = self.ConverttoFloat(points[i])

    #         points_combination_list = list(itertools.combinations(points, 2)); # output a combination list composed paired points, !!! take O(n^2) complexity

    #         for i in range(len(points_combination_list)): # len(combination list) = n^2
    #             point_pair = points_combination_list[i];
    #             (k,intercept) = self.CalculateLineFunction(point_pair[0], point_pair[1])  #calculate line k and intercept
    #             key_count = line_count_dictionary.get((k,intercept)) # save in dict taking O(n^2)
    #             if key_count==None:
    #                 line_count_dictionary[(k,intercept)] = 1 #creat a new one
    #                 # print key_count
    #             else:
    #                 line_count_dictionary[(k,intercept)] = key_count + 1
    #         # print line_count_dictionary.items()
    #         max_number_point_combinations = max(line_count_dictionary.values()) #return the max
    #         # print max_number_point_combinations, O(n^2)
    #         return int((1+ math.sqrt( 1 + 8*max_number_point_combinations))/2)  #calculate C(n,2) = max_number_point_combinations
    #         # print points_combination_list
    def maxPoints(self, points):
        number_of_points = len(points)
        if number_of_points == 0 or number_of_points == 1 or number_of_points == 2:
            return number_of_points
        else:
            overall_max_points_on_line = 0
            for i in range(len(points)):
                points[i] = self.ConverttoFloat(points[i])
                slope_set = [] # save slopes
                number_of_replicate_points = 0  #
                for j in range(len(points)):
                    points[j] = self.ConverttoFloat(points[j])
                    (k, intercept) = self.CalculateLineFunction(points[i], points[j])
                    if intercept == None: #same point
                        number_of_replicate_points += 1
                        continue
                    else:
                        slope_set.append(k)
                slope_set.sort()  # sort slope_set
                max_points_on_line = self.FindMaxPointsOnLine(slope_set) + number_of_replicate_points
                if max_points_on_line > overall_max_points_on_line:
                    overall_max_points_on_line = max_points_on_line

        return overall_max_points_on_line


    def FindMaxPointsOnLine(self, slope_set):
        if len(slope_set) == 0:
            return  0
        counter = 1
        max_count = 0
        for i in range(1,len(slope_set)):
            if slope_set[i] == slope_set[i-1]:
                counter += 1
            else:
                if counter > max_count:
                    max_count = counter
                counter = 1   # reset counter
        if counter > max_count:
            max_count = counter
        return max_count


    def ConverttoFloat(self, point): #convert the x and y of a point to float
        point.x = float(point.x)
        point.y = float(point.y)
        return point

    def CalculateLineFunction(self, point1, point2):
        # line function : y = k*x + interceptt
        try:
            k = (point1.y - point2.y)/(point1.x - point2.x);
        except ZeroDivisionError, e:
            if point1.y == point2.y:   #if there are same points
                k = None
                intercept = None
            else:
                k = float("inf")   # k is inf
                intercept = point1.x;  # store the x=point1.x
        else:
            intercept = point1.y - k * point1.x
        finally:
            return (k,intercept)

from random import randrange

if __name__ == '__main__':
    points = [];
    # for i in range (4):
    #     point = Point(randrange(20), randrange(20)); # randomly generates a point on the plane
    #     print point.x , point.y
    #     points.append(point);
    # print points
    p1 = Point(0,0)
    points.append(p1)
    p2 = Point(0,0)
    points.append(p2)

    
    p3 = Point(0,0)
    # p4 = Point(2,6)
    # p5 = Point(1,1)
    # p6 = Point(1,9)

   
    
    points.append(p3)
    # points.append(p4)
    # points.append(p5)
    # points.append(p6) 
    # points = []

    solution = Solution()
    print solution.maxPoints(points)

