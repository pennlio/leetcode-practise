class Solution:
    # @param points, a list of Points
    # @return an integer
    def maxPoints(self, points):
        number_of_points = len(points)
        if number_of_points <= 2:
            return number_of_points
        else:
            overall_max_points_on_line = 0
            for i in range(len(points)):
                points[i] = self.ConverttoFloat(points[i])
                slope_set = [] # save slopes
                number_of_replicate_points = 0  #
                for j in range(len(points)):
                    points[j] = self.ConverttoFloat(points[j])
                    (k, intercept) = self.CalculateLineFunction(points[i], points[j])
                    if intercept is None: #!!! cannot replace by not intercept, since it may == 0
                        number_of_replicate_points += 1
                        continue
                    else:
                        slope_set.append(k)
                slope_set.sort()  # sort slope_set
                max_points_on_line = self.FindMaxPointsOnLine(slope_set) + number_of_replicate_points
                overall_max_points_on_line = max(max_points_on_line,overall_max_points_on_line)
        return overall_max_points_on_line


    def FindMaxPointsOnLine(self, slope_set):
        if not len(slope_set): return  0
        counter = 1
        max_count = 0
        slope_set += ['s'] # add sentinel
        for i in range(1,len(slope_set)):
            if slope_set[i] == slope_set[i-1]:
                counter += 1
            else:
                max_count = max(counter, max_count)
                counter = 1   # reset counter
        return max_count

    def ConverttoFloat(self, point): #convert the x and y of a point to float
        point.x = float(point.x)
        point.y = float(point.y)
        return point

    def CalculateLineFunction(self, point1, point2):
        # line function : y = k*x + interceptt
        if point1.x == point2.x and point1.y == point2.y: 
            k = intercept = None
        elif point1.x == point2.x:
            k = float("inf")   # k is inf
            intercept = point1.x;  # store the x=point1.x
        else:
            k = (point1.y - point2.y)/(point1.x - point2.x);
            intercept = point1.y - k * point1.x
        return (k,intercept)