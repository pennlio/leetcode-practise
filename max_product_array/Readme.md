1st DP
======

concept:
--------
- define K(i,j): the current product from i to j
- define P(i,j): largest product within i to j
- recursive function: K(i,j) = K(i, j-1) * A[j]; P(i,j) = P(i,j-1) if ... or K(i,j) else
- do it from bottom up

imple:
------
- TLE, since O(n^2)



2st Killer rule
=========

concept:
--------
- for each K(i,j), P(i,j) is from K(i,j) getting rid of at most one neg product
- basically: P(i,j) = max( K(head(i,j)), K(tail(i,j))), which can be obtained from  CurPro for i in range(len(A))
- 0 as sepeartors 

imple:
---------
- O(n)
- add sentinels [0] for final 0

corner case:
--------
- !!! when neither head(i,j) nor tail(i,j) exits, above algorithm always returns P(i,j) = 1; so need deal with the corner case; but only if one of head or tail exists, its value should at least be larger than 1. So that case does not matter
