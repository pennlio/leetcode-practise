class Solution:
    # @param A, a list of integers
    # @return an integer
    ''' use DP but TLE'''
    def __init__(self):
        self._phash = {} # store max_product
        self._khash = {} # store up product from i to j
        self.maxP = -float('inf')
    def maxProduct(self, A):
        if not A: return 0
        n = len(A)
        for i in range(n):
            for j in range(i,n):
                self.calPHash(A,i,j)
        return self.maxP
        
    def calPHash(self, A, i, j):
        if i < 0 or j > len(A)-1: return
        if (i,j) not in self._phash:
            if i==j:
                self._phash[(i,j)] = A[i]
            else:
                if self.calKHash(A,i,j-1)*A[j]<0:
                    self._phash[(i,j)] = self.calPHash(A,i,j-1)
                elif A[j] == 0 and self.calPHash(A,i,j-1) > 0: # now stop extending
                    self._phash[(i,j)] = self.calPHash(A,i,j-1)
                else:
                    self._phash[(i,j)] = self.calKHash(A,i,j)
            self.maxP = max(self.maxP, self._phash[(i,j)])
        return self._phash[(i,j)]
            
    def calKHash(self, A, i, j):
        if i < 0 or j > len(A)-1: return
        if (i,j) not in self._khash:
            if i==j:
                self._khash[(i,j)] = A[i]
            else:
                self._khash[(i,j)] = self.calKHash(A,i,j-1)*A[j]
        return self._khash[(i,j)]

if __name__ == '__main__':
    sl = Solution()
    A = [-5,2,4,1,-2,2,-6,3,-1,-1,-1,-2,-3,5,1,-3,-4,2,-4,6,-1,5,-6,1,-1,-1,1,1,-1,1,1,-1,-1,1,-1,-1,1,1,-1,1,1,1,-1,-1,-1,-1,1,-1,1,-1,1,1,-1,-1,-1,-1,1,-1,-1,1,-1,-1,1,1,-1,-1,1,1,-1,1,-1,-1,1,-1,-1,-1,-1,1,1,1,1,1,1,-1,1,-1,1,-1,-1,1,-1,-1,1,-1,1,1,-1,1,-1,-1,1,-1,-1,-1,1,1,-1,1,1,-1,-1,1,-1,1,-1,1,-1,-1,-1,-1,1,1,1,1,1,1,-1,1,1,-1,-1,1,1,1,-1,1,-1,-1,-1,-1,-1,1,1,1,1,-1,-1,1,-1,-1,1,1,-1,-1,1,1,-1,1,1,1,1,-1,1,-1,-1,1,1,-1,-1,-1,1,1,-1,-1,1,1,-1,-1,1,-1,1,-1,1,-1,1,-1,-1,1,-1,-1,-1,-1,1,1,1,1,-1,1,1,1,-1,1,-1,-1,-1,-1,-1,-1,1,-1,-1,1,1,-1,-1,-1,1,-1,-1,-1,1,1,-1,1,-1,1,-1,1,1,-1,1,-1,1,1,-1,1,1,-1,1,1,-1,1,-1,-1,-1,1,1,1,1,-1,-1,1,-1,-1,1,1,-1,1,1,-1,-1,1,1,-1,-1,1,-1,-1,1,-1,1,1,1,1,1,-1,1,1,1,1,-1,1,1,-1,-1,1,-1,1,-1,-1,1,1,-1,-1,-1,1,1,-1,-1,1,-1,-1,-1,1,1,-1,1,1,1,1,-1,1,-1,1,1,-1,-1,-1,-1,-1,1,1,1,-1,-1,-1,-1,1,-1,-1,-1]
    # A = [-2,3,4,-1,0,5,1]
    # A = [2,3,-2,4,-1,0,99]
    print sl.maxProduct(A)