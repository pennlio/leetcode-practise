class Solution:
    # @param A, a list of integers
    # @return an integer
    def maxProduct(self, A):
        if not A: return None
        if len(A) == 1:
            return A[0]
        A = [0] + A + [0] # add sentinel
        CurPro = len(A)*[1]
        negNum = []
        maxPro = -float('inf')
        for i in range(1,len(A)):
            if A[i] != 0 : 
                if A[i] < 0:
                    negNum.append(i)
                CurPro[i] = A[i]*CurPro[i-1]
            else: # 0 as separator, skip update CurPro[i] for next start
                if A[i-1] == 0: 
                    maxPro = max(maxPro, 0)
                    continue # zeros just skip
                if CurPro[i-1] > 0:
                    maxPro = max(CurPro[i-1], maxPro)
                else: # if max is neg; finally return 0
                    if A[i-2]==0: # a neg num is crowded by two 0s: 0 -1 0
                        maxPro = max(maxPro, 0)
                    else:       
                        tail = CurPro[i-1]/CurPro[negNum[0]]
                        head = CurPro[negNum[-1]-1]
                        maxPro = max(maxPro, 0, head, tail)
                negNum = [] 
        return maxPro
if __name__ == '__main__':
    sl = Solution()
    A = [0,0,-1,1,-1,0,0]
    A = [-1]
    A = [-3,0,1,-2]
    # A = [-2,0,-1]
    print sl.maxProduct(A)
            



