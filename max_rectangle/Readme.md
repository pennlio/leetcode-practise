1st
===========
concept:
---------
- build up from base case:
- iteratively calculate maxArea of matrix until each row
- use buf to label the consecutive 1s at each row;  
- scan buf and use region index to store and calculate maxArea up to such row (use region as a stack)

impel:
------
- test None case of matrix: 
    if not len(matrix) then return 0
    if not len(maxtrix[0]) then return 0
    cannot write them up together

- stack to calculate longest consecutive 1s (region)

improve
---------
- WRONG RESULT


2nd
==========
concept:
----------
- solve using max rectangular in historgram

