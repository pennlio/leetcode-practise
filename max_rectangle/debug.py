def calcMax(buf, currentMax):
    region = []
    print buf
    for x in buf:
        if x != 0:
            region.append(x)
        else:
            region, currentMax = calRegionMax(region, currentMax)
    # do not forget the last row case
    region, currentMax = calRegionMax(region,currentMax)
    return currentMax

def calRegionMax(region, currentMax):
    while region:
        currentMax = max(len(region)*min(region), currentMax)
        if region[0] <= region[-1]:
            region = region[1:]
        else:
            region.pop()
    return region, currentMax
if __name__ == '__main__':
    buf = [3, 3, 8, 4, 1, 10, 6, 2, 4, 6, 37, 30, 3, 4, 7, 11, 0, 1, 4, 10, 2, 7, 25, 13, 3, 3, 4, 15, 7, 39, 9, 17, 27, 23, 25, 13, 22, 14, 3, 20, 1, 1, 1, 8, 8, 2, 33, 3, 2, 84, 10, 19, 9, 23, 22, 5, 20, 9, 11, 3, 3, 1, 24, 19, 3, 3, 35, 10, 13, 27, 0, 38, 3, 9, 11, 25, 1, 11, 4, 9, 0, 6, 5, 2, 0, 16, 1, 79, 1, 6, 0, 2, 28, 23, 46, 23, 22, 8, 21, 27]
    currentMax = 0
    print calcMax(buf,currentMax)