class Solution():
    def __init__(self):
        self._hash = {}
    def findMax(self, image):
        if not image:
            return 0
        nRow = len(image)
        nCol = len(image[0])
        maxsize = 0
        for i in range(nRow):
            for j in range(nCol):
                maxsize = max(maxsize, self.DPfindmax(image,i,j))
        return maxsize

    def DPfindmax(self, image, i, j):
        if (i,j) not in self._hash:
            nRow = len(image)
            nCol = len(image[0])        
            if i > nRow - 1 or i <0 or j > nCol - 1 or j <0 or image[i][j] == 0:
                self._hash[(i,j)] = 0                
            else:
                self._hash[(i,j)] = 1 + max(self.DPfindmax(image, i+1, j), self.DPfindmax(image, i, j+1), self.DPfindmax(image, i-1, j), self.DPfindmax(image, i, j-1))
        return self._hash[(i,j)]

if __name__ == '__main__':
    sl = Solution()
    image = [[0, 1, 1, 1, 1],
             [0, 1, 1, 0, 1],
             [0, 1, 0, 0, 1]]
    print sl.findMax(image)

