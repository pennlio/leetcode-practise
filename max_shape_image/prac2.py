class Solution:
    def __init__(self):
        self._visited = []
    def findMax(self, image):
        if not image:
            return 0
        m = len(image)
        n = len(image[0])
        self._visited = [[0]*n for x in range(n)]
        maxArea = 0

        for i in range(m):
            for j in range(n):
                if image[i][j] == 1 and self._visited[i][j] == 0:
                    maxArea = max(self.exploreArea(image, i,j), maxArea)
        return maxArea
    
    def exploreArea(self, image, i, j): # calculated unexplored area origninating from (i,j)
        m = len(image)
        n = len(image[0])
        if i > m-1 or i < 0 or j > n-1 or j< 0:
            return 0
        if self._visited[i][j] == 1 or image[i][j] == 0:
            self._visited[i][j] = 1
            return 0
        else:
            self._visited[i][j] = 1
            return self.exploreArea(image, i-1,j) + self.exploreArea(image, i,j-1) + self.exploreArea(image,i,j+1) + self.exploreArea(image, i+1,j) + 1 #not max but sum

if __name__ == '__main__':
    sl = Solution()
    image = [[1, 0, 1, 0, 0],
             [1, 1, 1, 1, 1],
             [0, 1, 0, 0, 1],
             [0, 1, 0, 0, 0]]
    print sl.findMax(image)
