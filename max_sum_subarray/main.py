class Solution():
	def maxSubArray(self, A):
		if len(A) == 0:
			return # when len(A) == 0, just return nothing, not zero!!!
		elif len(A) == 1:
			return A[0]
		else:
			n = len(A)
			if n%2 == 0:
				mid = n/2
				cmax = self.findmaxsum(A[mid-1::-1]) + self.findmaxsum(A[mid:]) # centercase is further divided the sum of two subarrays
				maxsum = max(self.maxSubArray(A[:mid]),  cmax, self.maxSubArray(A[mid:]))
			else:
				mid = int(n/2)
				cmax = self.findmaxsum(A[mid::-1]) + self.findmaxsum(A[mid:]) - A[mid]
				maxsum = max(self.maxSubArray(A[:mid]),  cmax, self.maxSubArray(A[mid+1:]))
			return maxsum

	def findmaxsum(self,A):  #find max sum for A starting at A[0]
		n = len(A)
		if n == 0:
			return 0
		elif n == 1:
			return A[0]
		else:
			max_sum = A[0]
			sum = A[0]
			max_index = 0
			j = 0
			for i in range(1,n):
				sum = sum + A[i]
				if sum > max_sum:
					max_index = j
					max_sum = sum
			return max_sum

if __name__ == "__main__":
	sl = Solution()
	# a = [-2,3,4,1,-2,-3,9]
	# a = [-2,-1]
	a= [-1,-2]
	print sl.maxSubArray(a)