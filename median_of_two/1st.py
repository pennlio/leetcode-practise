#! \bin\python


class Solution:
    # @return a float
    def findMedianSortedArrays(self, A, B):
        na = len(A)
        nb = len(B)

        if na == nb == 0:
            return False
        elif na == 0:
            return self.findMedian(B)[0]
        elif nb == 0:
            return self.findMedian(A)[0]
        elif na == nb == 1:
            return (float(A[0]) + float(B[0])) / 2
            # if A[0] >= B[0]:
            # return B[0]
            # else:
            #     return A[0]
        else:
            (ma, mia, mta) = self.findMedian(A)
            (mb, mib, mtb) = self.findMedian(B)
            if ma == mb:
                return ma
            elif ma > mb:
                A = A[0:mia + 1]
                if mtb == 0:  # mb is the lower median, not include mb
                    B = B[mib + 1:]
                else:
                    B = B[mib:]
            else:
                B = B[0:mib + 1]
                if mta == 0:
                    A = A[mia + 1:]
                else:
                    A = A[mia:]
            return self.findMedianSortedArrays(A, B)

    def findMedian(self, A):  # median 
        n = len(A)
        if n % 2 == 0:
            return (float((A[n / 2 - 1]) + float(A[n / 2])) / 2, n / 2 - 1, 0)  # 0 means lower median position
        else:
            return (A[n / 2], n / 2, 1)  # 1 means median position


if __name__ == '__main__':
    # A = [2,3,3,4]
    A = [2, 3 ,6]
    # A = [7, 9, 10, 11]
    B = [2, 3,4,5]
    # A = [1,2,3,4,5]
    # B = [3,5,7,9]
    # B = [2,3]
    sl = Solution()
    print sl.findMedianSortedArrays(A, B)
# print A[sl.findMedian(A)[0]]
