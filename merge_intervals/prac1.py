# Definition for an interval.
class Interval:
    def __init__(self, s=0, e=0):
        self.start = s
        self.end = e

class Solution:
    # @param intervals, a list of Interval
    # @return a list of Interval
    def getkey(self, item):
        return item.start
    def merge(self, intervals):
        if not intervals:
            return intervals
        intervals = sorted(intervals, key = self.getkey)
        lastInterval = intervals[0]
        output = []
        for i in range(1,len(intervals)):
            if intervals[i].start > lastInterval.end: # disjoint
                output.append(lastInterval)
                lastInterval = intervals[i]
                continue
            elif intervals[i].end <= lastInterval.end: # included
                continue
            else: # intersected
                newInterval = Interval(lastInterval.start, intervals[i].end)
                lastInterval = newInterval
        output.append(lastInterval)  # remember the last one
        return output

if __name__ == '__main__':
    sl = Solution()
    i1 = Interval(1,3)
    i2 = Interval(3,4)
    i3 = Interval(2,4)
    i5 = Interval(6,8)
    i4 = Interval(9,11)
    intervals = [i4,i2,i1,i3,i5]
    intervals = sl.merge(intervals)
    for x in intervals:
        print x.start, x.end
        