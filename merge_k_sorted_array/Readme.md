Readme.md

concept:
--------
- k-ways merge: use heaps


imple:
-------
- each time add (list[i][0], i) into the heap
- once an element from ith list is extracted from the heap, pop its current head as replacement into the heap, and new pointer point to new-head of that list
- O(nlogk)