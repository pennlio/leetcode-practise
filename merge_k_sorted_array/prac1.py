import heapq
class Solution:
    # @ list of lists
    def merge(self, list):
        if not list: return []
        heap = [(x[0],i) for i, x in enumerate(list) if x] # add non [] elements
        heapq.heapify(heap)
        result = []
        while heap: # heap is not empty
            i = heapq.heappop(heap)[1]
            result.append(list[i].pop(0)) # pop current ele to result set
            if list[i]:
                heapq.heappush(heap, (list[i][0],i)) # add head ele of this array to heap
        return result

if __name__ == '__main__':
    sl = Solution()
    list = [[1,5,8],[-3],[],[-1,0]]
    list = [[],[-1]]
    print sl.merge(list)


