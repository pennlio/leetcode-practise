concept:
----------
- k min-heap
- extract and form linked list


implementation:
------------
- `heapq` module 
- `heapify` a list with tuples: maintain the (val, node) pairs
- sentinel node
- O(n)


