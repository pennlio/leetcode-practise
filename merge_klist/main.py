# Definition for singly-linked list.
import heapq as heapq
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    # @param a list of ListNode
    # @return a ListNode
    def mergeKLists(self, lists):
        k = len(lists)
        # if k == 0 :
        if not lists:
            return None
        L = []
        sentinel = ListNode(0) # sentinel
        p1 = sentinel
        for node in lists:
            if node:
                L.append((node.val, node))  # store tuple as list element

        heapq.heapify(L)
        while L:
            heapmin, minnode = heapq.heappop(L)  # extract current min-val and min-node
            if minnode.next != None:
                heapq.heappush(L, (minnode.next.val, minnode.next))
            p1.next = minnode
            p1 = p1.next 
        return sentinel.next


        # buff = list(L)  # store orignal order
        # while len(L) > 0:
        # L = [x.val]
        # heapq.heapify(L)  # heapify L
        # while len(L) > 0:
        #     heapmin = heapq.heappop(L) # extract min
        #     for i in range(k):
        #         if heapmin == buff[i]:
        #             # xnext = x.next
        #             xnode = lists[i]
        #             if xnode.next != None:
        #                 heapq.heappush(L, xnode.next.val)
        #                 buff[i] = xnode.next.val
        #             else:
        #                 buff[i] = None
        #             # xnext = x.next
        #             lists[i] = lists[i].next
        #             break

        #     # if xnext != None:
        #         # heapq.heappush(L, xnext.val) # add next value to heap
        #     p1.next = ListNode(heapmin)
        #     p1 = p1.next

        # return sentinel.next

def buildList(value_array):
    if len(value_array) == 0:
        return None
    else:
        value = value_array
        head = ListNode(value[0])
        latest_node = head
        for i in range(1, len(value)):
            new_node = ListNode(value[i])
            latest_node.next = new_node
            latest_node = new_node
    return head


if __name__ == '__main__':
    sl = Solution()
    h1 = buildList([2,4,10])
    h2 = buildList([1])
    h3 = buildList([3,6,9])
    lists = [h1,h2,h3]
    # lists = [h1,h2]
    head = sl.mergeKLists(lists)
    p1 = head
    while p1!=None:
        print p1.val
        p1 = p1.next


        # head = ListNode
    # def buildMinPriorityQueue(self, lists):


    # def heapify(self, i):




