concept:
-------
- iterate from back of A and B, saving moving distances

corner case:
---------
- when A is finished before B, then copy rest of B to A