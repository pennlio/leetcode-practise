class Solution:
    # @param A  a list of integers
    # @param m  an integer, length of A
    # @param B  a list of integers
    # @param n  an integer, length of B
    # @return nothing
    def merge(self, A, m, B, n):
        if n == 0:
            return
        pb = n-1 
        pa = m-1
        p = m+n-1 
        # fill A form back
        while pb > -1:
            if pa == -1:  #corner case A is all ranked. copy rest B to A
                A[pb] = B[pb]
                pb -=1
                continue
            if B[pb] >= A[pa]: # put B at last
                A[p] = B[pb]
                pb -= 1
            else:
                A[p] = A[pa]
                pa -= 1
            p -= 1
        # return A
        return 

if __name__ == '__main__':
    sl = Solution()
    # A = [1,3,5,7,10,12,0,0,0,0]
    # A = [9,10,11,0,0,0]
    A = [2,0]
    # B = [2,4,8,9]
    B =[1]
    m = 1
    n = 1
    print sl.merge(A,m,B,n)
