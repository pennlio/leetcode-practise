# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    # @param two ListNodes
    # @return a ListNode
    def mergeTwoLists(self, l1, l2):
        if not l1:
            return l2
        elif not l2:
            return l1

        sentinel = ListNode(-111)
        p0 = sentinel

        p1 = l1
        p2 = l2

        while p1 != None and p2 != None:
            if p1.val <= p2.val:
                p0.next = p1
                p1 = p1.next
            else:
                p0.next = p2
                p2 = p2.next
            p0 = p0.next
        if not p1:
            p0.next = p2
        else:
            p0.next = p1
        return sentinel.next

def buildList(value_array):
    if len(value_array) == 0:
        return None
    else:
        value = value_array
        head = ListNode(value[0])
        latest_node = head
        for i in range(1, len(value)):
            new_node = ListNode(value[i])
            latest_node.next = new_node
            latest_node = new_node
    return head


if __name__ == '__main__':
    sl = Solution()
    h1 = buildList([2])
    # h2 = buildList([1])
    h3 = buildList([3,6,9])
    # lists = [h1,h2,h3]
    # lists = [h1,h2]
    head = sl.mergeTwoLists(h1,h3)
    p1 = head
    while p1!=None:
        print p1.val
        p1 = p1.next

