# Definition for a  binary tree node
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    # @param root, a tree node
    # @return an integer
    def minDepth(self, root):
        if not root:
            return 0
        p = root
        depth = 1
        stack1 = []
        stack2 = []
        stack1.append(p)
        while len(stack1) > 0:
            p = stack1.pop()
            if p.left == None and p.right == None:
                return depth
            if p.left:
                stack2.append(p.left)
            if p.right:
                stack2.append(p.right)
            if len(stack1) == 0: # if last layer is all checked
                stack1 = stack2  # proceed to next layer
                stack2 = []
                depth += 1

if __name__ == '__main__':
    sl = Solution()
    root = None
    root = TreeNode(5)
    root.right = TreeNode(6)
    root.right.left = TreeNode(9)
    root.right.right = TreeNode(8)

    root.left = TreeNode(3)
    root.left.left = TreeNode(37)
    root.left.right = TreeNode(4)
    print sl.minDepth(root)






