class Solution:
    # @param grid, a list of lists of integers
    # @return an integer
    def __init__(self):
        self._minhash={}  # record
    def minPathSum(self, grid):
        if not grid:
            return
        m = len(grid)   # numR
        n = len(grid[0]) #numC
        return self.minSum(grid,0,0) + grid[0][0] # add first grid finally 

    def minSum(self, grid, i, j):
        m = len(grid)   # numR
        n = len(grid[0]) #numC
        if (i,j) in self._minhash:
            return self._minhash[(i,j)]
        else:
            if i == m-1:
                self._minhash[(i,j)] = sum(grid[i][j+1:]) # not include current cell
            elif j==n-1:
                colsum = 0
                while i+1 <= m-1:
                    colsum += grid[i+1][j] # not include current cell either
                    i += 1
                self._minhash[(i,j)] = colsum
            else: # in the middle
                a = grid[i+1][j] + self.minSum(grid,i+1,j)
                b = grid[i][j+1] + self.minSum(grid,i,j+1)
                self._minhash[(i,j)] = min(a,b)
            return self._minhash[(i,j)]

if __name__ == '__main__':
    sl = Solution()
    # grid = [[1,2,3],[4,5,6],[7,8,9]]
    # grid = [[1,2,1],[4,5,6],[1,1,9]]
    grid= [[1],[2],[1]]
    print sl.minPathSum(grid)