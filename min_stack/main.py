class MinStack:
    # @param x, an integer
    # @return an integer
	def __init__(self):
		self.stack = []
		self.min_stack = []
	def push(self, x):
		self.stack.append(x)
		if self.getMin() == None or self.getMin() >= x:
			self.min_stack.append(x)
		else:
			self.min_stack.append(self.getMin()) 
	def pop(self):
		try: 
			output = self.stack.pop()
		except IndexError, e:
			print "empty stack"
			return None
		else:
			self.min_stack.pop()
		return output 

	def top(self):
		if len(self.stack) == 0:
			return None
		else:
			return self.stack[len(self.stack)-1]
        	
	def getMin(self):
		if len(self.min_stack) == 0:
			return None
		else:
			return self.min_stack[len(self.min_stack)-1]

if __name__ == "__main__":
	stack = MinStack()
	print stack.getMin()
	print stack.top()

	stack.push(2);
	stack.push(3);
	stack.push(-1);
	stack.push(3);
	print stack.top()
	print stack.getMin()

	stack.pop()
	stack.pop()
	print stack.getMin()
	stack.pop()
	print stack.getMin()
