class MinStack:
    # @param x, an integer
    # @return an integer
    def __init__(self):
        self._stack = []
        self._min_stack = [] # use min_stack
        # self._min = float('inf') 
    def push(self, x):
        self._stack.append(x)
        if x <= self.getMin(0):  # not checkonly
            # self._min = x
            self._min_stack.append(x)
        return
    # @return nothing
    def pop(self):
        if len(self._stack) == 0:
            return
        else:
            x = self._stack.pop()
            if x == self.getMin(1):
                self._min_stack.pop()
            # if len(self._stack) == 0:
                # self._min = float('inf') # reset min
        return
    # @return an integer
    def top(self):
        if len(self._stack) == 0:
            return None
        else:
            return self._stack[-1]
        
    # @return an integer
    def getMin(self, checkonly = 1):
        if len(self._min_stack) == 0:
            if checkonly:
                return None
            else:
                return float('inf')
        else:
            return self._min_stack[-1]

if __name__ == "__main__":
    stack = MinStack()
    # print stack.getMin()
    # print stack.top()
    # push(395),stack.getMin,stack.top,stack.getMin,stack.push(276),push(29),getMin,push(-482),getMin,pop
    stack.push(395)
    print stack.getMin()
    print stack.top()
    print stack.getMin()
    stack.push(276)
    stack.push(29)
    print stack.getMin()
    stack.push(-482)
    print stack.getMin()
    stack.pop()
    stack.push(-108)
    stack.push(-251)
    # print stack
    print stack.getMin()



    # stack.push(2147483646)
    # # stack.push(2147483647)
    # print stack.top()
    # print stack.pop()
    # print stack.getMin()
    # print stack.pop()
    # print stack.getMin()
    # print stack.pop()
    # print stack.push(2147483647)

        