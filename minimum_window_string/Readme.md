1st:
======
concept:
------
- implement rolling window problem: where duplicates are not allowed to emerge in the window



2nd:
=======

concept:
-------
- try to work it out by mainting a subarray only by elements from T; but failed



3rd: (almost right)
====== 

concept: 
--------
- good to see the problem from index perspective, and build index array for each T
- good to use stack to indicate current state of the window; calculate windowsize when all is collected
- design a linear algo but fail to output rightly; 


4th:
=======

concept:
-----
- killer rule: only exact the number of x's indcies need to be stored under hash[x]; o/w, just update the sequence; since IF x is not the window start, then it does not matter; IF it is the window start, it shrinks the window size by moving index forward.
- so the window size is min and max of all `valid` indices

- like 3rd; but only more complicated
- when compare

impel:
------
- O(mn) where m is the size of T; not O(n)