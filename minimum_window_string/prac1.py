class Solution:
    # @return a string
    ''' this does not allow extra char from T in window; different quetion'''
    def __init__(self):
        self._hash = {} # hash to store available spaces for x
    def minWindow(self, S, T):
        if not T or not S:
            return ''
        n = len(T)
        stack = [] # to put chars
        for x in T:
            if x not in self._hash:
                self._hash[x] = 1
            else:
                self._hash[x] += 1 # build index
        S = S + '-' # add 0 as sentinel
        mW = (-1,float('inf'))
        for i, y in  enumerate (S):
            if y in self._hash : # have already met one
                if self._hash[y] > 0:
                    self._hash[y] -= 1 # not overload
                else:
                    while stack[0][0]!= y:
                        self._hash[stack[0][0]] += 1 # remove from front
                        stack.pop(0)
                    stack.pop(0)
                stack.append((y,i))
            if len(stack) == n: # check if stack is full
                if stack[-1][1] - stack[0][1] < mW[1]-mW[0]: #update minWindow
                    mW = (stack[0][1], stack[-1][1])
        return S[mW[0]:mW[1]+1] if mW[0] > -1 else ''

if __name__ == '__main__':
    sl = Solution()
    S = 'CAODEBANC'
    # S = 'ADOBECODEBANC'
    # T = 'ABC'
    T = 's'
    # S =" acbdbaab"
    # T = "aabd"
    print sl.minWindow(S,T)





