class Solution:
    # @return a string
    def __init__(self):
        ''' change way of working'''
        self._hash = {}
    def minWindow(self, S, T):
        if not S or not T:
            return ''
        S = S + '0' # add sentinel element
        stack = [] # save the status
        n = len(T)
        for x in T:
            self._hash[x] = [] #init hash
        window = (-1, float('inf'))
        for i, y in enumerate(S):
            if y in self._hash:
                self._hash[y].append(i) # add them to any way
                # if self._hash[y] == 0: #already in # also need save duplicates by hash table
                if stack and stack[0][0] == y:
                    stack.pop(0) # get rid of head
                    stack.append((y,i)) # renew
                else:
                    self._hash[y] = 0
                    stack.append((y,i))
                # check header to decide remove or not
                if len(stack) == n: # record the window
                    if stack[-1][1] - stack[0][1] < window[1]-window[0]:
                        window = (stack[0][1], stack[-1][1]) 
        return S[window[0]:window[1]+1] if window[0] > -1 else ''

if __name__ == '__main__':
    sl = Solution()
    S = 'CACCBAEB'
    # S = 'ADOBECODEBANC'
    T = 'ABC'
    # T = 's'
    # S =" acbdbaab"
    # T = "aabd"
    print sl.minWindow(S,T)



