class Solution:
    # @return a string
    def minWindow(self, S, T):
        '''build index: wrong answer: not consider T has dups; search direction also has flaws'''
        if not S or not T: return ''
        self._hash = {}
        for x in T:
            self._hash[x] = [] # init
        VT = []
        for i in range(len(S)):
            if S[i] in self._hash:
                if self._hash[S[i]] == []:
                    VT.append(S[i])
                self._hash[S[i]].append(i) 
        
        if len(VT) < len(T): return '' # not enough
        low = 0
        high = len(S)-1
        lf = hf = 0
        while low <= high:
            if S[low] in self._hash:
                if len(self._hash[S[low]]) == 1:
                    lf = 1
                else:
                    self._hash[S[low]].pop(0)

            if S[high] in self._hash:
                if len(self._hash[S[high]]) == 1:
                    hf = 1
                else:
                    self._hash[S[high]].pop() # remove last one

            if hf and lf: break
            if not lf: low += 1 
            if not hf: high -= 1
        return S[low:high+1]

if __name__ == '__main__':
    sl = Solution()
    # S = 'CACCBAEB'
    S = 'ADOBECODEBANC'
    T = 'ABC'
    # T = 's'
    # S =" acbdbaab"
    # T = "aabd"
    print sl.minWindow(S,T)




