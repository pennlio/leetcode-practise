class Solution:
    def minWindow(self, S, T): 
    # learn part from sample code at https://oj.leetcode.com/discuss/20275/accepted-python-solution-using-hashtable
        if not S or not T:
            return ''
        self._hash = {}
        for x in T:
            self._hash[x] = []
        miss = list(T) # miss

        low = 0
        high = len(S)-1
        for i, y in enumerate(S):
            if S[i] in self._hash:
                if y not in miss and self._hash[y] != []: # not in miss: do not need y now
                    self._hash[y].pop(0) # remove the first element
                elif y in miss:
                    miss.remove(y) # miss git rid of y
                self._hash[y].append(i)  # anyway update this one

            if miss == []: # if no one missed 
                start = min([x[0] for x in self._hash.values()]) # here is O(m) opers
                end = max([x[-1] for x in self._hash.values()])
                if end - start < high - low:
                    high = end
                    low = start
        if miss:
            return '' # do not include all
        else:
            return S[low:high+1]


if __name__ == '__main__':
    sl = Solution()
    # S = 'CACCBAEB'
    S = 'ADOBECODEBANC'
    T = 'ABC'
    # T = 's'
    # S =" acbdbaab"
    # T = "aabd"
    print sl.minWindow(S,T)


