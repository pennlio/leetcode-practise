class Solution:
    # @param num, a list of integer
    # @return a list of integer
    def nextPermutation(self, num):
    ''' wrong solution'''
        # two pointers iterate from back
        if not num:
            return []
        n = len(num)
        p1 = n-1
        buf = []
        while p1 >= 0:
            p2 = p1 - 1
            buf = num[p1+1:] #store digits after p1
            while p2 >= 0:
                buf.append(num[p2])
                if num[p2] < num[p1]:
                    num[p2] = num[p1]
                    buf.sort()
                    return num[:p2+1] + buf
                else:
                    p2 -= 1
            p1 -= 1
        # last case
        num.sort()
        return num

if __name__ == '__main__':
    sl = Solution()
    # num = [1,2,1]
    num = [1,2,3,4]
    num = [4,5,8,7]
    num = [3,4,2,1]
    num = [4,2,0,2,3,2,0]
    print sl.nextPermutation(num)

