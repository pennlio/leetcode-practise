class Solution:
    # @param num, a list of integer
    # @return a list of integer
    def nextPermutation(self, num):
        # check the largest value behind
        if not num:
            return []
        n = len(num)
        p1 = n-2
        while p1 >= 0:
            if num[p1] < num[p1+1]:
                return self.outPermutation(num,p1)
            else:
                p1 -= 1
        num.sort()
        return num

    def outPermutation(self, num, p1):
        p2 = len(num)-1
        while p2 > p1:
            if num[p2] > num[p1]:
                temp = num[p2]
                num[p2] = num[p1]
                num[p1] = temp
                a = num[p1+1:]
                a.sort()
                return num[:p1+1] + a
            else:
                p2 -= 1


if __name__ == '__main__':
    sl = Solution()
    # num = [1,2,1]
    num = [3,2,1]
    # num = [1,2,3,4]
    # num = [4,5,8,7]
    # num = [3,4,2,1]
    # num = [4,2,0,2,3,2,0]
    print sl.nextPermutation(num)