import random
class Solution:
    def findPoint(self, locations, grid_dim):
        ''' brute force method'''
        if min(grid_dim) <= 0:
            return [None, None]
        minDistance = float('inf')
        (a,b) = (-1,-1)
        for x in range(grid_dim[0]):
            for y in range(grid_dim[1]):
                distance = self.calcSumDistance(x,y,locations)
                if distance < minDistance:
                    minDistance = distance
                    (a,b) = (x,y)
        return (a,b)

    def calcSumDistance(self,x,y,locations):
        distance = [abs(x-z[0]) + abs(y-z[1]) for z in locations]
        return sum(distance)

if __name__ == '__main__':
    sl = Solution()
    grid_dim = (5,100)
    randnum = range(grid_dim[0]*grid_dim[1])
    random.shuffle(randnum)
    locations = [(x//grid_dim[1], x%grid_dim[1]) for x in randnum[:20]] # generate non-overlap pos.
    print sl.findPoint(locations, grid_dim)
