import random
class Solution:
    def findMin(self, locs, grid):
        if not grid: return (-1,-1)
        # by math equation : meeting point is average of x and y
        n = len(locs)
        sumX = sumY = 0
        for x in locs:
            sumX += x[0]
            sumY += x[1]
        minX = avgX = sumX/n
        minY = avgY = sumY/n
        minDist = self.calSumDistance(minX, minY, locs)
        for x in [avgX, avgX+1]:
            for y in [avgY, avgY+1]:
                dist = self.calSumDistance(x,y,locs)
                if dist < minDist:
                    minDist = dist
                    minX, minY = x,y
        return (minX, minY)

    '''brute force'''
    # def findPoint(self, locs, grid_dim): 
    #     ''' brute force method'''
    #     if min(grid_dim) <= 0:
    #         return [None, None]
    #     minDistance = float('inf')
    #     (a,b) = (-1,-1)
    #     for x in range(grid_dim[0]):
    #         for y in range(grid_dim[1]):
    #             distance = self.calSumDistance(x,y,locs)
    #             if distance < minDistance:
    #                 minDistance = distance
    #                 (a,b) = (x,y)
    #     return (a,b)


    def calSumDistance(self, x, y, locs):
        dist = [abs(x-z[0])+abs(y-z[1]) for z in locs]
        return sum(dist)

if __name__ == '__main__':
    sl = Solution()
    grid_dim = (4,3)
    randnum = range(grid_dim[0]*grid_dim[1])
    random.shuffle(randnum)
    locs = [(x//grid_dim[1], x%grid_dim[1]) for x in randnum[:5]] # generate non-overlap pos.
    # print sl.findPoint(locs, grid_dim)
    print sl.findMin(locs, grid_dim)