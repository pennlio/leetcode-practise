// #include <stdlib>
#include <iostream>


using namespace std;

class Node {
public:
	Node * p1;
	Node * p2;
};

Node * CopyNode (Node * src){
	if (src == NULL) 
		return NULL;
	
	Node * dst = new Node();
	*(dst->p1) = * (src->p1);
	*(dst->p2) = * (src->p2);
	return dst;
}


int main(){
	Node * source = new Node();
	Node * s1 = new Node();
	Node * s2 = new Node();
	Node * dest = new Node();

	source->p1 = s1;
	source->p2 = s2;

	dest = CopyNode(source);
}