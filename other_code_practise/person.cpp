#include <iostream>
using namespace std;

class Person
{
public:
	virtual ~Person(){
		cout<<"delete a person\n";
	}
	void virtual aboutme(){
		cout << "i am a Person\n!";
	}
};

// Person::Person(int input_id, int input_salary){
// 	int id = input_id;
// 	int salary = input_salary;

// };

class Employee: public Person{

public:
	~Employee (){
		cout<<"delete an Employee\n";
	}
	void aboutme(){
		// cout << "i am a Employee with id "<<id<<" and salary "<<salary<<endl;
		cout<<"i am an Employee\n";
	}
};



int main(){

	Person * p = new Employee(); //automatically add constructor to Employee()
	p->aboutme();
	delete p;

	Employee *q = new Employee(); 
	q->aboutme();
	delete q;

 	Employee a = Employee(); //when quit, automatically call ~Employee()
 	a.aboutme(); 
 	// a.~Employee();
	return 0;

}