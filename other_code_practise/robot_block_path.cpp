#include<iostream>

using namespace std;

class ListNode{
public:
	int x;
	int y;
	ListNode* next = NULL;
	ListNode* prev = NULL;
};

class PathFinder{

public:
	int x_dim;
	int y_dim;
	int **block_cell;
	static ListNode * head;
	static ListNode * last;

	PathFinder(int x, int y, int **array){
		x_dim = x;
		y_dim = y;
		block_cell = array;
		head = new ListNode();
		last = head; // point to empty list
	}

	void PrintLinkList(ListNode* head){
		ListNode *p = head;
		while(p!=NULL){
			cout<<"("<<p->x<<","<<p->y<<")"<<endl;
		}
	}

	int SearchLinkList(ListNode* head, int x_start, int y_start){
		ListNode *p = head;
		while(p->x == x_start && p->y==y_start){
			return 1;
		}
		return 0;
	}

	void RecursiveFindPath(int x_start, int y_start){
		if (x_start<0 || y_start<0){
			cout<<"start error\n";
			return;
		}
		else if( x_start == x_dim && y_start == y_dim){
			ListNode *p = new ListNode();
			p->x = x_start;
			p->y = y_start;
			last->next = p;
			p->next = NULL;
			p->prev = last;
			last = p;
			cout<<"complete!\n";
			PrintLinkList(head);
			exit;
		}
		else if (SearchLinkList(head, x_start,y_start)){
				cout<<"meet block cells\n";
				return;}
		else if (x_start > x_dim || y_start>y_dim){
				cout<<"cross border\n";
				return;}
		else{
			// append current point
			ListNode *p = new ListNode();
			p->x = x_start;
			p->y = y_start;
			last->next = p;
			p->next = NULL;
			p->prev = last;
			last = p;
			RecursiveFindPath(x_start+1, y_start);
			RecursiveFindPath(x_start,y_start+1);
			// remove current point
			last = last->prev;
			last->next = NULL;
			return;
		}
	}
};

int main(){
	ListNode *p = new ListNode();
	int block_cell[4][4] = {{3,0},{1,1},{2,1},{2,2}};
	int x_dim = 3;
	int y_dim = 3;
	PathFinder *q = new PathFinder(x_dim,y_dim,block_cell);
	q->RecursiveFindPath(0,0);
}