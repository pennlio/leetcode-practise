#!bin/python
''' look at the matrix as a BT, and use DFS to find out the way'''
import sys
class PathFinder:
	path_history = []; #static var; stack
	def __init__(self, x_dim, y_dim, block_cells):
		self.x_dim = x_dim
		self.y_dim = y_dim
		self.block_cells = block_cells
		pass
	
	def RecurisveFindPath(self, x_start, y_start):
		if (x_start<0 or y_start<0):
			print "start point error"
			return
		elif (x_start == x_dim and y_start == y_dim):
			self.__class__.path_history.append((x_start,y_start))
			print "completed!"
			print self.__class__.path_history
			sys.exit(0)
		elif (x_start,y_start) in self.block_cells:
			print "meet block region"
			return
		elif (x_start>x_dim or y_start>y_dim):
			print "cross border"
			return
		else:
			self.__class__.path_history.append((x_start,y_start)) # add into path history
			self.RecurisveFindPath(x_start+1,y_start)
			self.RecurisveFindPath(x_start, y_start+1)
			self.__class__.path_history.pop()  # remove the nearest point
			return

if __name__ == '__main__':
	x_dim = 3; # 0 to 3
	y_dim = 3;
	block_cells = [(3,0),(1,1),(2,1),(2,2)]
	# block_cells = [(2,0),(1,1)]


	path_finder = PathFinder(x_dim,y_dim,block_cells)
	path_finder.RecurisveFindPath(0,0)



