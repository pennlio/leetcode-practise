#!\bin\python

class PathCounter:
	path_count_dict = {}
	def __init__(self):
		pass
	def RecursiveCountPath(self,(x,y)): #count path number to (x,y)
			if (x<0 or y<0):
				return 0
			elif (x==0 and y==0):
				return 1   # count to origin
			else:
				count = self.__class__.path_count_dict.get((x,y))
				if str(count) == "None": # return None should be in str type
					count = self.RecursiveCountPath((x-1,y)) + self.RecursiveCountPath((x,y-1))
					self.__class__.path_count_dict[(x,y)] = count # add to dict

				return count

if __name__ == "__main__":
	x = 2;
	y = 2;
	path_counter = PathCounter();

	count = path_counter.RecursiveCountPath((x,y));
	print count