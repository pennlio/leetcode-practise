class MyQueue:

	def __init__(self):
		self.main_stack = []
		self.b_stack = []

	def enQueue(self, a):
		if a == None:
			print "input error"
		else:
			self.main_stack.append(a)
		return

	def deQueue(self):
		while 1:
			try:
				tmp = self.main_stack.pop()
			except IndexError, e:
				break # main_stack is empty
			else:
				self.b_stack.append(tmp)
				continue
		out = self.b_stack.pop()
		while 1:
			try:
				tmp = self.b_stack.pop()
			except IndexError, e:
				break # buffer is empty
			else:
				self.main_stack.append(tmp)
				continue
		return out
	def display(self):
		print self.main_stack

if __name__ == "__main__":
	myq = MyQueue()
	myq.enQueue(1)
	myq.enQueue(1)
	myq.enQueue(2)
	myq.enQueue(5)
	myq.enQueue(1).
	myq.deQueue()
	myq.deQueue()
	myq.display()