#!bin/python
class Counter:
	diction = {}; # static dictionary

	def RecursiveCountNum(self, n):
		if n<0:
			# print "Error"
			return 0;
		# elif (n==1 or n==2 or n==0):
		elif n==0:
			return 1 # record the latest step to n==0, so ==1
		else:
			count = self.__class__.diction.get(n)
			if count == None: # when a dic not intitialized
				n1 = self.RecursiveCountNum(n-1) 
				n2 = self.RecursiveCountNum(n-2) 
				n3 = self.RecursiveCountNum(n-3)
				count = n1 + n2 + n3;
				self.__class__.diction[n] = count # static var
				return count
			else:
				return self.__class__.diction[n] 
			


if __name__ == '__main__':
	
	n = 3;
	counter = Counter()
	count = counter.RecursiveCountNum(n)
	print count