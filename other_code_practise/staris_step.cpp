#include <iostream>

using namespace std;

int RecursiveCountNum(int n){
	if(n<0){
		cout<< "Error\n";
		return 0;
	}
	else if(n==0){
		return 1;
	}
	else{
		int n1=0, n2=0, n3=0;
		int count;
		n1 = RecursiveCountNum(n-1);
		n2 = RecursiveCountNum(n-2);
		n3 = RecursiveCountNum(n-3);

		count = n1 + n2 + n3;
		return count;

	}

}

int main(){

	int n = 10;
	int count;
	count = RecursiveCountNum(n);
	cout<<count;

}