#include <iostream>
#include <fstream>
using namespace std;
class Foo
{
        public:
        Foo() {};
        void speak() { cout << "Foo's constructor" << endl; }
        virtual void hello() { cout << "my name is Foo\n";}
};
class Bar : public Foo
{
        public:
        Bar() {} 
        void speak() { cout << "Bar's constructor" << endl; }
        virtual void hello() { cout << "my name is bar\n";}
};

int main()
{
        // a lovely elephant ;)
        // Bar bar;
		char str[10];
        Bar *p = new Bar();
        p->speak();
        p->hello();
        ofstream afile ("test.txt"); //output to afile
        afile << "hello\n";
        afile.close();

        ifstream bfile ("test.txt");  //input from bfile
        bfile >> str;
        cout << str;

}