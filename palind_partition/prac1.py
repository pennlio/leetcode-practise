class Solution:
    # @param s, a string
    # @return a list of lists of string
    def partition(self, s):
        n = len(s)
        if n == 1:
            return [s]
        result = []
        if self.isPalind(s):
            return self.genPalindromeSet(s)
        for i in range(len(s)):
            if self.isPalind(s[:i]) and self.isPalind(s[i:]):
                a = self.genPalindromeSet(s[:i])
                b = self.genPalindromeSet(s[i:])
                for x in a:  '''cannot append appropriate strings'''
                    for y in b:
                        x.append(y)
                return a
                # print b
        # if self.isPalind(s):
        #     return self.genPalindromeSet(s)
        # else:
        #     print [s[0], self.partition(s[1:])]
        #     print [self.partition(s[:-1]), s[-1]]
        

    # def generateSubPalind(self, s): # generate sub-palindrome for s
    #     if not s:
    #         return     # should be recursive
    #     n = len(s)
    #     if n == 1:
    #         return s
    #     result = []
    #     if self.isPalind(s):
    #         # result = [s[0], self.generateSubPalind(s[1:n-1]), s[n-1]]
    #         return s
    #     else:
    #         result = [s[0], self.generateSubPalind(s[1:])]
    #     if n > 2:
    #         result += [self.generateSubPalind(s[:-1]), s[-1]]

    #     # # i = 
    #     # # j = n
    #     # # while i <=j:
    #     # result.append([s[:start], s[start:end],s[end:]])
    #     # while start < end:
    #     #     self.generateSubPalind()
    #     #     result.append(s)
    #     #     # i += 1
    #     #     # j -= 1

    #     return result
    # def recursiveGenSubPalin(self,s,start,end):
    def genPalindromeSet(self, s):
        n = len(s)
        if n <= 1:
            return [s]
        i = 0
        j = n - 1
        result = [[s]]
        while i < j:
            subresult = []
            '''key part'''
            subresult += s[0:i+1]
            if s[i+1:j] != '':
                subresult.append(s[i+1:j])
            subresult += s[i::-1]  # a += b, put b into chars if b is a string
            '''key part'''
            result.append(subresult)
            i +=1
            j -=1
        return result


    def isPalind(self,s):
        if s == '' :
            return True
        n = len(s)
        if n == 1:
            return True
        else:
            i = 0
            j = n-1-i
            while i < n//2:
                if s[i] != s[j]:
                    return False
                i += 1
                j -= 1
        return True

if __name__ == '__main__':
    sl = Solution()
    # s = 'sadfsa'
    # s = 'asddsa'
    s = 'aabcb'
    # s = 'aab'
    print sl.partition(s)


    # s = 'ewtwe'
    # print sl.isPalind(s)
    # print sl.generateSubPalind(s)
    # s = 'aa'
    # print sl.genPalindromeSet(s)





