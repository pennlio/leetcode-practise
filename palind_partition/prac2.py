
class Solution:
    # @param s, a string
    # @return a list of lists of string
    def partition(self, s):
        if not s: return [[]]
        self.cache = {}
        return self.part(s, 0, len(s)-1)


    def part(self, s, low, high):
        # return set of available sets
        if low > high: return [[]] # need one element to add s[start:part+1]
        if low == high: return [[s[low:high+1]]] 
        if (low, high) in self.cache:
            return self.cache[(low,high)]
        part = start = low
        result = []
        while part <= high:
            if self.isPalin(s, start, part):
                subResult = self.part(s, part+1, high)
                for sbr in subResult:
                    result.append([s[start:part+1]]+sbr)
            part += 1
        self.cache[(low,high)] = result
        return result

    def isPalin(self, s, low, high):
        if low > high: return False
        if low == high: return True
        while low < high:
            if s[low] != s[high]:
                return False
            low += 1
            high -= 1
        return True

def main():
    sl = Solution()
    s = 'a'
    print sl.partition(s)


if __name__ == '__main__':
    main()