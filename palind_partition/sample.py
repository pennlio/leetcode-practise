class Solution:
    # @param s, a string
    # @return a list of lists of string
    def partition(self, s):
        if len(s) == 0: return []
        length = len(s)
        s = s[::-1]
        dptable = []
        # DP computing for substring [0, i]
        for i in xrange(length):
            i_lst = []
            if s[0:i+1] == s[i::-1]: i_lst.append([s[0:i+1]])
            for j in xrange(1, i+1):
                if s[j:i+1] == s[i:j-1:-1]:
                    for l in dptable[j-1]:
                        t = l[:] # intermediate var, not change l
                        t.append(s[j:i+1])
                        i_lst.append(t)
            dptable.append(i_lst)
        ans = dptable[-1]
        ans = [lst[::-1] for lst in ans]
        return ans

if __name__ == '__main__':
    sl = Solution()
    # s = 'sadfsa'
    # s = 'asddsa'
    s = 'aab'
    # s = 'ewtwe'
    # print sl.isPalind(s)
    print sl.partition(s)