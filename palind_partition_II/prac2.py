'''learn from sample'''
class Solution():
    def minCut(self, s):
        if not s: return 0
        n = len(s)
        cut = [i-1 for i in range(n+1)] # always record the number of cuts for the first k chars
        for i in range(n):
            j = 0
            while i-j>=0 and i+j<n and s[i-j] == s[i+j]:
                cut[i+j+1] = min(cut[i+j+1], 1+cut[i-j]) # cut[i+j+1] is at most cut[i-j]+1
                j+= 1
            j = 1
            while i-j+1 >=0 and i+j<n and s[i-j+1] == s[i+j]:
                cut[i+j+1] = min(cut[i+j+1], 1+cut[i-j+1])
                j+= 1

        return cut[n]

def main():
    sl = Solution()
    s = 'cbbbcc'
    print sl.minCut(s)

if __name__ == '__main__':
    main()

