class Solution:
    # @return a boolean
    def isPalindrome(self, x):
        if x < 0:
            return False
        if x == 0:
            return True
        return True if self.reverse(x) == x else False
    def reverse(self, x):
        # use stack to reverse
        digistack = []
        while x!=0:
            digit = x%10
            digistack.append(digit)
            x //= 10
            # mod *= 10
        y = 0
        n = len(digistack)
        mod = 1
        while n > 0:
            n -= 1
            y = digistack.pop()*mod + y
            mod *= 10
        return y

if __name__ == '__main__':
    sl = Solution()
    x = 0
    print sl.isPalindrome(x)
