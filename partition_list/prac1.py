# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    # @param head, a ListNode
    # @param x, an integer
    # @return a ListNode
    '''move smaller to front, failed'''
    def partition(self, head, x):
        if not head:
            return head
        prev = low = sentinel = ListNode(-1)
        low.next = head        
        p = head
        while p!=None:
            if p.val < x:
                tmp = low.next
                low.next = p
                prev.next = p.next
                p.next = tmp
                low = p
                p = prev.next
            else:
                p = p.next
                prev = prev.next
        return sentinel.next

    def buildList(self, value_array):
        if len(value_array) == 0:
            return None
        else:
            value = value_array
            head = ListNode(value[0])
            latest_node = head
            for i in range(1, len(value)):
                new_node = ListNode(value[i])
                latest_node.next = new_node
                latest_node = new_node
        return head

    def printList(self, head):
        p = head
        if not p:
            return
        while p != None:
            print p.val
            p = p.next
if __name__ == '__main__':
    sl = Solution()
    head = sl.buildList([1,4,3,2,5,2])
    # head = sl.buildList([5,5,3,2])
    x = 3
    h2 = sl.partition(head,x)
    sl.printList(h2)