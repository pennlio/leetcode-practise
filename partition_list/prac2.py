# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    # @param head, a ListNode
    # @param x, an integer
    # @return a ListNode
    '''move larger to end'''
    def partition(self, head, x):
        if not head:
            return head
        prev = sentinel = ListNode(-1)
        prev.next = last_node = head
        while last_node.next!=None: # move to last node!!!
            last_node = last_node.next
        tail = last_node
        p = head
        while p!=last_node:
            if p.val >= x:
                tail.next = p
                tail = tail.next # move to tail
                p = p.next
                tail.next = None
                prev.next = p
            else:
                p = p.next
                prev = prev.next
        # modify last node
        if p.val >= x:
            tail.next = p
            tail = tail.next # move to tail
            p = p.next
            tail.next = None
            prev.next = p
        return sentinel.next

    def buildList(self, value_array):
        if len(value_array) == 0:
            return None
        else:
            value = value_array
            head = ListNode(value[0])
            latest_node = head
            for i in range(1, len(value)):
                new_node = ListNode(value[i])
                latest_node.next = new_node
                latest_node = new_node
        return head

    def printList(self, head):
        p = head
        if not p:
            return
        while p != None:
            print p.val
            p = p.next
if __name__ == '__main__':
    sl = Solution()
    head = sl.buildList([1,4,3,2,5,2])
    head = sl.buildList([5,5,3,2,9])
    x = 2
    h2 = sl.partition(head,x)
    sl.printList(h2)