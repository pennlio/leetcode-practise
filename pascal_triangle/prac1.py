class Solution:
    # @return a list of lists of integers
    def generate(self, numRows):
        result= []
        if numRows == 0:
            return result
        elif numRows == 1:
            result.append([1])
            return result
        else:
            newRow = [1]
            result = curentTriangle = self.generate(numRows-1)
            lastRow = curentTriangle[-1] # get last row
            for i in range(len(lastRow)-1):
                newRow.append(lastRow[i]+lastRow[i+1]) # generate new row
            newRow.append(1) # add last 1
            result.append(newRow)
            return result

if __name__ == '__main__':
    sl = Solution()
    numRows = 9
    print sl.generate(numRows)
