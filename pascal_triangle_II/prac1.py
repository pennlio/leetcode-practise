class Solution:
    # @return a list of integers
    def getRow(self, rowIndex):
        # same ways as Pascal I
        if rowIndex < 0 :
            return []
        if rowIndex == 0:
            return [1]
        currentRow = [1]
        lastRowIndex = 0
        while lastRowIndex < rowIndex:
            lastRow = currentRow
            currentRow = [1]
            for i in range(len(lastRow)-1):
                currentRow.append(lastRow[i]+lastRow[i+1])
            currentRow.append(1)
            lastRowIndex += 1
        return currentRow

if __name__ == '__main__':
    sl = Solution()
    rowIndex = 0
    print sl.getRow(rowIndex)