# Definition for a  binary tree node
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    # @param root, a tree node
    # @param sum, an integer
    # @return a boolean
    def hasPathSum(self, root, sum):
        if not root:
            return False
        if sum - root.val == 0:
            if not root.left and not root.right: # root is a leave
                return True
        if root.left:
            if self.hasPathSum(root.left, sum-root.val):
                return True
        if root.right:
            if self.hasPathSum(root.right, sum-root.val):
                return True
        return False

if __name__ == '__main__':
    sl = Solution()
    root = TreeNode(1)
    root.left = TreeNode(-2)
    root.right = TreeNode(-3)
    root.left.left = TreeNode(1)
    root.left.right = TreeNode(3)
    root.right.left = TreeNode(-2)
    # root.right.right = TreeNode(4)
    root.left.left.left = TreeNode(-1)
    # root.left.left.right = TreeNode(2)
    # root.right.right.right = TreeNode(1)
    sum = -1
    print sl.hasPathSum(root,sum)