# Definition for a  binary tree node
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    # @param root, a tree node
    # @param sum, an integer
    # @return a list of lists of integers
    def pathSum(self, root, sum):
        # DFS
        if not root: return []
        if sum == root.val and root.left == None and root.right == None:
            return [[root.val]]
        leftSet = self.pathSum(root.left, sum-root.val)
        rightSet = self.pathSum(root.right, sum-root.val)
        allSet = leftSet + rightSet
        for i,x in enumerate(allSet):
            if x != []:
                allSet[i].insert(0, root.val)  # this step can be improved ;
        return allSet

if __name__ == '__main__':
    sl = Solution()
    root = TreeNode(1)
    root.left = TreeNode(2)
    root.right = TreeNode(3)
    root.left.left = TreeNode(-1)
    root.right.left = TreeNode(4)
    root.right.right = TreeNode(5)
    print sl.pathSum(root, 2)