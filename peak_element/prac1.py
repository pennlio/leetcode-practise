class Solution:
    # @param num, a list of integer
    # @return an integer
    def findPeakElement(self, num):
        if not num:
            return 
        n = len(num)
        if n == 1 or num[0] > num[1] :
            return 0
        elif num[n-1] > num[n-2]: # test end case
            return n-1
        '''use binary search to test'''
        return self.binarySearch(num,0,n-1)


    def binarySearch(self, num, i, j): # both i and j are included
        if j - i <= 1:
            return -1
        else:
            mid = (i+j)//2
            '''test center num'''
            if self.isPeak(num, mid):
                return mid
            elif self.isPeak(num, mid+1):
                return mid+1

            if (i+j+1) % 2 == 1: # odd case
                '''test center left num'''
                if self.isPeak(num, mid-1):
                    return mid-1
                else:
                    return max(self.binarySearch(num,i,mid-1), self.binarySearch(num, mid+1,j))
            else: # even case
                return max(self.binarySearch(num,i,mid), self.binarySearch(num, mid+1,j))

    def isPeak(self, num, i):
        n = len(num)
        if i==0 or i == n-1:
            return False  # if true, then return at beginning
        else:
            if num[i-1] < num[i] > num[i+1]:
                return True
            else:
                return False

if __name__ == '__main__':
    sl = Solution()
    num = [9,8,7]
    print sl.findPeakElement(num)
