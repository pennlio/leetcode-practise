Readme.md
1st
=======
concept:
--------
- pop correspondent elements in numSet to form the string

imple:
--------
- (n,k) --> (n,k-1), since we append the numSet[a] by
indexing a = k//fact, where fact is the factorial, so the 
largest num should be k-1; on the other hand, we guarantee that 
when k==1, that is k-1 =0, we directly append numSet[0] to result.


corner case:
-------
- when n = 1

improve:
-------
- can further shrink the code and simplfiy the idea by investigating a = k%fact

2nd
=====
concept:
------
- same

imple:
------
- shorter code with improvement mentioned in one
