# import math
class Solution:
    # @return a string
    def getPermutation(self, n, k):
        if n < 1 or n > 9:
            return ''
        fact = self.calFactorial(n)
        if k == 0 or k > fact:
            return ''
        k -= 1 # 
        b = n
        fact = fact/b
        numSet = range(1,n+1)
        result = ''
        a = k // fact
        while a == 0:
            b -= 1
            if b == 0 :  # here only one element left, since fact already reduce to 0!
                return result + str(numSet.pop(a))
            result += str(numSet.pop(a))
            fact = fact/b
            a = k // fact
        while a >= 0 :  # start from rest, even if a == 0; that means it  need to add the residual
            result += str(numSet.pop(a))
            residual = k % fact
            b -= 1
            if b == 1: 
                result += str(numSet.pop(residual)) # add the residual
                break
            fact = fact/b
            a = residual // fact
        for x in numSet:
            result += str(x)
        return result
        
    def calFactorial(self,n):
        if n == 0:
            return 1
        else:
            return n*self.calFactorial(n-1)

if __name__ == '__main__':
    sl = Solution()
    n = 4
    k = 24
    print sl.getPermutation(n,k)
