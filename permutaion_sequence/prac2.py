class Solution:
    # @return a string
    def getPermutation(self, n, k):
        if n <= 0 or k <= 0 : return ''
        fact = 1
        num = []
        for x in range(1,n+1): # calc factorial
            num.append(str(x)) # num is a char set
            fact *= x
        result = []
        k -= 1 # k start from k-1
        while k > 0 and n > 0: # select from num; if n <= 0, also return
            fact /= n # fact = (n-1)!
            result.append(num.pop(k//fact)) 
            k %= fact
            n -= 1
        result.extend(num)
        return ''.join(result)
            
        