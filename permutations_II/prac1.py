class Solution:
    # @param num, a list of integer
    # @return a list of lists of integers
    def permuteUnique(self, num):
        if not num:
            return [[]]
        n = len(num)
        if n == 1:
            return [num]
        num.sort()
        result = []
        for i in range(n):
            if i > 0 and num[i-1] == num[i]:
                continue
            for x in self.permuteUnique(num[:i]+num[i+1:]):
                x.append(num[i])
                result.append(x)
        return result

if __name__ == '__main__':
    num = [1,1,1,2,3]
    num = [1,1,1]
    sl = Solution()
    print sl.permuteUnique(num)

