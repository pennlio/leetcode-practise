class Solution:
    # @param num, a list of integer
    # @return a list of lists of integers
    def permuteUnique(self, num):
        if not num: 
            return [[]]
        elif len(num) == 1:
            return [num]
        num.sort()
        result = []
        for i,x in enumerate(num):
            if i == 0 or x != num[i-1]: #skip same elements
                buf = list(num)
                buf.pop(i) # must copy num, since num is a common pointer 
                subsets = self.permuteUnique(buf) # subsets
                for y in subsets:
                    # y.append(x)
                    y.extend([x])
                    result.append(y)
        return result


if __name__ == '__main__':
    num = [1,1,1,2,3]
    # num = [1,1,1]
    sl = Solution()
    print sl.permuteUnique(num)