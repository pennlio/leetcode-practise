# ex:
# RO 1287 ['rlo', "rolling", "real", "WhaT", "rOad"] => "rOad"
class Solution():
    def findPlate(self, plate, dict):
        # do counting sort for plate
        cnt = [0] * 26 # chars
        for i in plate:
            id1 = ord(i) - ord('a')
            id2 = ord(i) - ord('A')
            if 0 <= id1 <= 26:
                cnt[id1] += 1
            elif 0 <= id2 <= 26:
                cnt[id2] += 1
            else:
                break
            continue
        # check strings:
        minL = float('inf')
        minChar = None
        for x in dict:
            if self.checkChar(x,cnt):
                if minL > len(x):
                    minChar = x
                    minL = len(minChar)
        return minChar

    def checkChar(self, string, cnt):
        ccnt = list(cnt)  # must copy a new one here, since function interface pass pointers 
        for i in string:
            id1 = ord(i) - ord('a')
            id2 = ord(i) - ord('A')
            if 0 <= id1 <= 26:
                if ccnt[id1] > 0:
                    ccnt[id1] -= 1
            elif 0 <= id2 <= 26:
                if ccnt[id2] > 0:
                    ccnt[id2] -= 1
            if max(ccnt)==0:
                return True
        return False

if __name__ == '__main__':
    sl = Solution()
    plate = 'ROl 1287'
    dict = ['rlo', "rolling", "real", "WhaT", "rOad","rod", 'dd'] 
    print sl.findPlate(plate, dict)
