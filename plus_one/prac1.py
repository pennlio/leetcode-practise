class Solution:
    # @param digits, a list of integer digits
    # @return a list of integer digits
    def plusOne(self, digits):
        if not digits:
            return []
        # digits = list(digits)
        digits.reverse()
        carrier = 1
        for i in range(len(digits)):
            x = digits[i] + carrier # store x in case modify carrier and digits[i] later
            if x >= 10:
                carrier = 1
            else:
                carrier = 0
            digits[i] = x %10
        if carrier == 1:
            digits.append(1)
        digits.reverse()
        return digits

if __name__ == '__main__':
    sl = Solution()
    digits = [1,9,9,9]
    digits = [0]
    print sl.plusOne(digits)
