# Definition for a  binary tree node
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None
        self.next = None

class Solution:
    # @param root, a tree node
    # @return nothing
    def connect(self, root):
        if not root:
            return root
        stack1 = []
        stack2 = []
        p = root
        stack1.append(p)
        # while len(stack1)>0:
        lpcondition = True
        while lpcondition:
            for i in range(len(stack1)):
                if stack1[i].left != None and stack1[i].right != None:
                    stack2.append(stack1[i].left)
                    stack2.append(stack1[i].right)
                if i == len(stack1)-1:  # last element
                    stack1[i].next == None
                else:
                    stack1[i].next = stack1[i+1]
            if not stack2:
                lpcondition = False  # do while
            else:
                stack1 = stack2
                stack2 = []  # reset stack2
        return

if __name__ == '__main__':
    sl = Solution()
    root = TreeNode(4)
    root.left = TreeNode(3)
    root.right = TreeNode(5)
    sl.connect(root)


