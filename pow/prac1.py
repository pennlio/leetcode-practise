class Solution:
    # @param x, a float
    # @param n, a integer
    # @return a float
    def pow(self, x, n):
        if x == 0 or x ==1 or n==1:
            return x
        if n == 0:
            return 1
        if n < 0:
            x = 1/x
            n = -n
        if n%2 == 1:
            m = (n-1)/2
            half = self.pow(x,m)
            return half*half*x
        else:
            half = self.pow(x,n/2)
            return half*half

if __name__ == '__main__':
    x = 34.00515
    n = -3
    sl = Solution()
    print sl.pow(x,n)