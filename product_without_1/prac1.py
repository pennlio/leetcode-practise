class Solution:
    def product(self, num):
        if not num: return 0
        n = len(num)
        front = [1]*n
        back = list(front) # back and front are two lists
        for i in range(1,n): # first one = 1
            front[i] =  num[i-1]*front[i-1]
        for i in range(n-2,-1,-1): # last one = 1
            back[i] = back[i+1]*num[i+1]
        # print front
        # print back
        return [front[i]*back[i] for i in range(n)]
if __name__ == '__main__':
    sl = Solution()
    num = [2,3,5,2,1]
    print sl.product(num)
