class Solution():
	def removePunct(self, a):
		n = len(a)
		if n==0 or n==1:
			return a
		else:
			state = 0 
			# 0: not met any; 
			# 1: so far all !; 
			# 2: so far all ?;
			# 3: mix ? and !
			i = 0
			while True:
				if a[i] == "!":
					if state == 0:
						state = 1
						start_pos = i
					elif state == 1:
						state = 1
					elif state == 2:
						state = 3
					elif state == 3:
						pass
				elif a[i] == "?":
					if state == 0:
						state = 2
						start_pos = i
					elif state == 1:
						state = 3
					elif state == 2:
						state = 2
					elif state == 3:
						pass
				else:
					if state != 0:
						end_pos = i-1
						a = self.modifyString(a, start_pos, end_pos,state)
						i = start_pos
						state = 0 # reset to labels 
						start_pos = None 
						end_pos = None
				i += 1
				try:
					a[i]
				except IndexError, e:
					break  # reach end of string
				else:
					continue
			end_pos = len(a)
			if state!=0:
				a = self.modifyString(a, start_pos, end_pos,state)
			return a
	def modifyString(self, a, start_pos, end_pos, state):
		if state == 1:
			return a[:start_pos] + "!" + a[end_pos+1:]
		else:
			return a[:start_pos] + "?" + a[end_pos+1:]


if __name__ == "__main__":
	sl = Solution()
	a = ""
	a = "!!!"
	a = "????! hello?? world !!??"
	a = "how are you?!! ??? !!!"
	a = "do you like this??!! yes, of course !!!"
	b = sl.removePunct(a)
	print b
