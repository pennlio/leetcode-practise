#!bin\python

# quick sort partiotion a array into 3 parts

def qs_partition(A, low, high):
    if low == high: return A
    pivot = A[high]
    low_index = low
    high_index = high - 1
    i = low
    # print pivot
    while (i < high_index+1):  # here i==high_index  
        if A[i] < pivot:
            tmp = A[i]
            A[i] = A[low_index]
            A[low_index] = tmp
            low_index += 1
        elif A[i] > pivot:
            tmp = A[i]
            A[i] = A[high_index]
            A[high_index] = pivot  # move pivot from back from step 1
            A[high_index+1] = tmp
            high_index -= 1    # put one
            continue  #no add i
        i += 1
    print low_index - 1   # return high/low_order index 
    print high_index + 1
    return A   # no need to exchange pivot since it's been moved front


if __name__ == '__main__':
    a = [4, 2, 4, 6, 3, 2, 3, 5, 4, 7, 3, 3, 8, 4]
    high = len(a) - 1
    a = qs_partition(a, 0, high)
    print a