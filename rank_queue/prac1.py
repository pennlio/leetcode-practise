# from google intern interview
from operator import itemgetter
class Solution:
    def rank(self, hr):
        if not hr:
            return []
        # sort the tuple by height
        hr = sorted(hr, key = itemgetter(0))
        newQueue = []
        while hr:
            for x in hr:
                if x[1] == 0:
                    newQueue.append(x[0])
                    hr.remove(x)
                    break
                else:
                    x[1] -= 1 # tuple does not support value assignment
        return newQueue

if __name__ == '__main__':
    sl = Solution()
    hr = [[165,1],[175,1],[190,0],[180,0],[170,3]]
    print sl.rank(hr)
