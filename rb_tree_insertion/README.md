RB-tree insertion
============
@pennlio Sept. 18, 2014

steps:
--------
- construct a BT with recursion
- fix-up color and structre from bottom of tree iteratively (3 cases)

corner case:
-----
left, right-rotaion when 
- node's parent/grantparent does not exist
- node's child is None
- root pointer changes during rotation


fix-up color:
- when node or node's parent is root node
- root pointer changes during fix-up (due to rotation)

why not recursion?
- when recursive function involves another recursive function within, be careful, e.g., fix-up and rotation
- fix-up fixes whole tree not subtree


Cheers @-@