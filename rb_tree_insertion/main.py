#!\bin\python

class RBTreeNode():
    """docstring for RBTreeNode"""

    def __init__(self, value='None', left='None', right='None', parent='None', color='red'):
        # super(RBTreeNode, self).__init__()
        self.value = value
        self.left = left
        self.right = right
        self.p = parent
        self.color = color


def FixupColor(root_node, z_node):
    while z_node.p.color == "red":
        if z_node.p == "None":
            z_node.color = "black" # z is root_node
            return z_node
        if z_node.p.p == "None":
            root_node.color = "black"
            return root_node
        if z_node.p == z_node.p.p.left:  # p is the left child of pp : leftcase
            if z_node.p.p.right != "None":
                y_node = z_node.p.p.right # save uncle as y
                if y_node.color == "red":  #case 1
                    z_node.p.color = "black"
                    y_node.color = "black"
                    z_node.p.p.color = "red"
                    z_node = z_node.p.p
                    if z_node.p == "None":  # judge if z_node is root
                        z_node.color = "black"
                        return  z_node
            else:
                if z_node == z_node.p.right: #black uncle
                    z_node = z_node.p
                    root_node = LeftRotate(root_node, z_node)
                # case 3
                z_node.p.color = "black"
                z_node.p.p.color = "red"
                root_node = RightRotate(root_node,z_node.p.p)
        else:
            if z_node.p.p.left != "None": 
                y_node = z_node.p.p.left # save uncle as y
                if y_node.color == "red":  #case 1
                    z_node.p.color = "black"
                    y_node.color = "black"
                    z_node.p.p.color = "red"
                    z_node = z_node.p.p
                    if z_node.p == "None":  # judge if z_node is root
                        z_node.color = "black"
                        return  z_node
            else:
                if z_node == z_node.p.left: #black uncle
                    z_node = z_node.p
                    root_node = RightRotate(root_node, z_node)
                # case 3
                z_node.p.color = "black"
                z_node.p.p.color = "red"
                root_node = LeftRotate(root_node,z_node.p.p)
    root_node.color = "black"
    return root_node



# def FixupColor(root_node, x_node):
#     if x_node == 'None' or x_node.color == "black":
#         return  # empty case
#     elif x_node.p == 'None':  # x_node is the root node
#         x_node.color = 'black'
#         return
#     elif x_node.p.color == "black" or x_node.p.p == "None":  # x_node follows a black node or a root node
#         return  # no problem
#     else:
#         '''  the case about uncle '''
#         if x_node.p == x_node.p.p.left: # the left case
#             if x_node.p.p.right == "None":
#                 if x_node == x_node.p.right:  # if x_node is the right child of parent
#                     LeftRotate(root_node,x_node.p)  #do rotate to parent to left
#                 # same as case 3
#                 RightRotate(root_node, x_node.p.p)
#                 x_node.color = 'black'  # now x_node is the 'root'

#                 # x_node.right.color = 'red'
#                 return
#             if x_node.p.p.right.color == "red":  # uncle's is red
#                 x_node.p.p.color = 'red'  # change grandparent's color to red
#                 x_node.p.p.right.color = 'black'  # change parent and uncle's color to black
#                 x_node.p.color = 'black'
#                 FixupColor(x_node.p.p)  # move up 'red' node
#                 x_node = x_node.p.p  # move up 
#                 return
#             else:  # when uncle is black
#                 if x_node == x_node.p.right:  # if x_node is the right child of parent
#                     LeftRotate(root_node,x_node.p)  
#                 # case 3
#                 RightRotate(root_node,x_node.p.p)
#                 x_node.color = 'black'  # now x_node is the 'root'
#                 x_node.right.color = 'red'
#                 return
#         else:
#             pass  # the right case


def RBTInsertNode(root_node, value):
    if root_node == "None":
        root_node = RBTreeNode(value)
        return root_node
    
    x_node = root_node
    y_node = "None"
    z_node = RBTreeNode(value)
    while (x_node!="None"):
        y_node = x_node
        if value < x_node.value:
            x_node = x_node.left
        else:
            x_node = x_node.right
    z_node.p = y_node  # put z as child of y_node
    if y_node == "None":
        root_node = z_node  # if z_node is the first node
    elif z_node.value < y_node.value:
        y_node.left = z_node
    else:
        y_node.right = z_node
    root_node = FixupColor(root_node, z_node)
    return root_node
    # z_node.left = "None"
    # z_node.right = "None"
    # z_node.color = 'red'

    


# def RBTInsertNode(root_node, value):
#     """
#     :itype : RBTreeNode root_node, value
#     :rtype : RBTreeNode root_node
#     """
#     new_node = RBTreeNode(value)
#     if root_node.value == 'None':   
#         new_node.color = 'black'
#         return new_node  # new_node now is the root
#     else:
#         if new_node.value < root_node.value:
#             # new_node.color = 'red'
#             if root_node.left == 'None':  # nil of head's left
#                 root_node.left = new_node
#                 new_node.p = root_node
#                 # FixupColor(root_node, new_node)   # fix-up color
#                 # return root_node
#             else:
#                 RBTInsertNode(root_node.left, value)
#                 # return root_node  # remember to return
#         else:
#             if root_node.right == 'None':
#                 root_node.right = new_node
#                 new_node.p = root_node
#                 # FixupColor(root_node, new_node)   # fix-up color
#                 # return root_node
#             else:
#                 RBTInsertNode(root_node.right, value)
#                 # return root_node
#     # print root_node.value
#     FixupColor(root_node, new_node)
#     return root_node


def LeftRotate(root_node, x_node):
    # do left rotate to x_node
    try:
        y_node = x_node.right 
    except AttributeError, e:
        return  # if x_node is None, then return
    else:
        x_node.right = y_node.left
        if y_node.left != 'None':     # otherwise y_node.left has no 'p' attribute
            y_node.left.p = x_node

        y_node.left = x_node
        y_node.p = x_node.p
        if x_node.p == 'None':
            root_node = y_node
        elif x_node == x_node.p.left:
            x_node.p.left = y_node
        else:
            x_node.p.right = y_node
        x_node.p = y_node
    return root_node


def RightRotate(root_node, x_node):
    # do right rotate to x_node
    try:
        y_node = x_node.left
    except AttributeError, e:
        return
    else:
        x_node.left = y_node.right
        if y_node.right != "None":
            y_node.right.p = x_node
        y_node.right = x_node
        y_node.p = x_node.p
        if x_node.p == "None":
            root_node = y_node #root
        elif x_node == x_node.p.left:
            x_node.p.left = y_node
        else:
            x_node.p.right = y_node
        x_node.p = y_node

    return root_node



def PreTraverseRBTree(x_node):
    # recursion traverse
    if x_node == 'None':
        return
    else:
        print x_node.value, x_node.color
        PreTraverseRBTree(x_node.left)
        PreTraverseRBTree(x_node.right)
        return


if __name__ == '__main__':
    '''construct a RB tree'''
    root_node = "None"
    values = [7, 3, 5, 6, 8, 10]
    # print range(len(values))
    for i in range(len(values)):
        root_node = RBTInsertNode(root_node, values[i]) # in-place )
        # root_node_copy = root_node
    PreTraverseRBTree(root_node)


# to be finished about changing colors'''