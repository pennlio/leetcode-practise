#!bin/python

def divide(number):
	number = number/2
	if number < 100:
		return number
	else: 
		# divide(number)  # this will not return the final result
		number = divide(number) #this returns the final result
        print number
		return number

if __name__ == '__main__':

	n = 1000
	p = divide(n)
	print p