# Definition for a  binary tree node
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    # @param root, a tree node
    # @return a tree node
    def __init__(self):
        self._nodeStack = []
        self._status = 0 # 0 for before the first node; 1 for first node found; 2 for go over the first decreasing point
    def recoverTree(self, root):
        if not root:
            return root
        lastNode = self.inOrderTraverse(root, None)
        # print len(self._nodeStack)
        if len(self._nodeStack) == 1: # two node case
            self._nodeStack.append(lastNode)
        self._nodeStack[0].val, self._nodeStack[1].val = self._nodeStack[1].val, self._nodeStack[0].val
        return root

    def inOrderTraverse(self, root, lastNode):
        if not root:
            return None
        if root.left:
            lastNode = self.inOrderTraverse(root.left, lastNode)
        if lastNode: # start looking for swapped position
            if root.val < lastNode.val:  # compare root
                if not self._status:
                    self._nodeStack.append(lastNode)
                    self._status += 1 # first node found
                else:
                    if self._status == 2: # if first node next is attached
                        self._nodeStack.pop() # pop the last one and save new one
                    self._nodeStack.append(root) #current node
                # print self._nodeStack[-1].val
            elif root.val > lastNode.val: # and then save first node's next node
                if self._status == 1: # if first is found, record its next node in case there is only one decreasing point
                    self._nodeStack.append(lastNode)
                    self._status += 1 # already attached first node's next
        lastNode = root # root
        print root.val
        if root.right:
            lastNode = self.inOrderTraverse(root.right, lastNode)
        return lastNode

if __name__ == '__main__':
    # import Tree from Tree
    sl = Solution()
    root = TreeNode(1)
    root.left = TreeNode(2)
    root.right = TreeNode(3)
    # root.right = TreeNode(1)
    root = sl.recoverTree(root)
    sl.inOrderTraverse(root,None)




