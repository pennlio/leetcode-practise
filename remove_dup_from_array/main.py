#!bin\python

class Solution:
    # @param a list of integers
    # @return an integer
    def removeDuplicates(self, A):
    	n = len(A)
    	if n==1 or n==0 : return n
    	storage_idx = 1;   #assuming first two already in
    	itor = storage_idx # check from the second one 
    	while (itor < n):
    		if A[itor] != A[storage_idx-1]: # compare if there are more than 2
    			A[storage_idx] = A[itor]  # put in ouput
    			storage_idx+=1
    		itor+=1
    	return storage_idx   # storage index always points end of the output array


if __name__ == "__main__":
	# A = [1,2,3,3,4,5,6,6]
	A = []
	sl = Solution()
	h = sl.removeDuplicates(A)
	print h