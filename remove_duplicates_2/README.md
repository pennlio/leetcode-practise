Remove Dup. from Sorted Array II
============
pennlio@Oct. 1, 2014

method:
-----
- in-place scan and squezze array 

comment:
---
- similar to Quicksort
- can be extended to 'k' case

corner case:
---
- storage_index starts at 2
- compare A[itor] and A[storage_index-2] with elem in storage
- return A[0:storage_index], since storage_index points to last elem

complexity: 
------
- O(n)