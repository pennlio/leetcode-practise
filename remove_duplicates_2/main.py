#!bin/python
# use storage_index to implement in-place operation;
# commpare with in-store A, A[storage_index-k];
# like the QuickSort
# can be extended to eliminating k elems
# only return to storage_index length
class Solution:
    # @param A a list of integers
    # @return an integer
    def removeDuplicates(self, A):
    	n = len(A)
    	if n <= 2: return n 
    	storage_index = 2  # store the lowest available output
    	itor = 2
    	while(itor < n):
    		if A[itor]!=A[storage_index-2]:  # compare with storage index
    			A[storage_index] = A[itor] 
    			storage_index+=1
    		itor+=1

    	# print A[:storage_index]		# return only to storage_index
    	return storage_index



if __name__ == "__main__":

	A = [1,1,1,2,2,2,3,3,3,3,4,5]
	sl = Solution()
	length = sl.removeDuplicates(A);
	print length
	print A
