class Solution:
    # @param    A       a list of integers
    # @param    elem    an integer, value need to be removed
    # @return an integer
    def removeElement(self, A, elem):
        if not A:
            return 0
        n = len(A)
        i = 0
        low_index = 0 # label the place of non-target elems
        high_index = n-1 # label the place of target elems
        high_index = self.alignHighindex(A, elem, high_index)
        if high_index != -1:
            while i <= high_index:
                if A[i] == elem:
                    self.swap(A,i,high_index)
                    high_index -= 1
                    high_index = self.alignHighindex(A, elem, high_index)
                low_index += 1
                i += 1
        return len(A[0:low_index])

    def alignHighindex(self, A, elem, high_index):
        while A[high_index] == elem:
            if high_index == -1: # bound -1
                break
            high_index -= 1
        return high_index

    def swap(self, A, i, j):
        temp = A[i]
        A[i] = A[j]
        A[j] = temp
        return
if __name__ == '__main__':
    sl = Solution()
    # A = [2,3,4,5,2,3,3,4,3]
    # A = [12,4,5,2]
    A = [3,3,3]
    # A = [2]
    # A = []
    print sl.removeElement(A,3)
