# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    # @return a ListNode
    def removeNthFromEnd(self, head, n):
        start = ListNode(0)
        start.next = head # start elment

        # if head.next == None and n == 1:
        #     return None
        # else:
        h1 = h2 = start
        while n > 0:
            h2 = h2.next
            n -= 1
        while h2.next != None:
            h1 = h1.next
            h2 = h2.next
        temp = h1.next.next
        h1.next = temp
        return start.next

    def printLinklist(self, head):
        h = head
        while h != None:
            print h.val
            h = h.next


if __name__ == '__main__':
    sl = Solution()
    head = ListNode(1)
    # head.next = ListNode(2)
    # head.next.next = ListNode(3)
    # head.next.next.next = ListNode(4)
    # sl.printLinklist(head)
    head = sl.removeNthFromEnd(head,1)
    sl.printLinklist(head)
    
