# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    # @param head, a ListNode
    # @return nothing
    def reorderList(self, head):
        if not head or head.next == None:
            return
        p1 = p2 = head
        while p2.next != None and p2.next.next != None:
            p2 = p2.next.next # jump two steps
            p1 = p1.next
        h2 = p1.next
        p1.next = None # cut into two lists
        h2 = self.reverseList(h2)
        p1 = head
        p2 = h2
        while p1 != None and p2 != None:
            p1n = p1.next # store p1
            p2n = p2.next
            p1.next = p2
            p2.next = p1n
            p1 = p1n
            p2 = p2n
        return

    def reverseList(self, head):
        if not head or not head.next:
            return head
        p0 = head
        p1 = p0.next
        p2 = p1.next
        head.next = None
        while p2 != None:
            p1.next = p0
            p0 = p1
            p1 = p2
            p2 = p2.next
        p1.next = p0
        return p1  # 3 pointers imple


    def buildList(self, value_array):
        if len(value_array) == 0:
            return None
        else:
            value = value_array
            head = ListNode(value[0])
            latest_node = head
            for i in range(1, len(value)):
                new_node = ListNode(value[i])
                latest_node.next = new_node
                latest_node = new_node
        return head
    
    def printList(self, head):
        p = head
        if not p:
            return
        while p != None:
            print p.val
            p = p.next

if __name__ == '__main__':
    sl = Solution()
    # a = [1,2,3,4,5]
    a = [1,2]
    # a = [1,2,3]
    head = sl.buildList(a)
    head = sl.reorderList(head)
    # hh = sl.reverseList(head)
    sl.printList(head)
    # print "h"
    # sl.printList(hh)