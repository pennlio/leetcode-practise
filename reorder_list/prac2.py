# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    # @param head, a ListNode
    # @return nothing
    def reorderList(self, head):
        if not head: return None
        if not head.next: return head # only one element
        h1,h2 = self.cutHalf(head)
        h2 = self.reverse(h2)
        return self.merge(h1,h2)
        
    def cutHalf(self, head): # return h1 and h2
        p1 = head
        p2 = head
        while p2.next!= None and p2.next.next != None:
            p2 = p2.next.next
            p1 = p1.next
        p2 = p1.next
        p1.next = None
        return head, p2
    
    def reverse(self, head): # return ~h2
        if not head: return None
        sentinel = ListNode(-1) # sentinel
        sentinel.next = head
        p0,p1,p2 = sentinel, head, head.next
        while p2:
            p1.next = p0
            p0 = p1
            p1 = p2
            p2 = p2.next
        p1.next = p0 # lastone
        head.next = None # 
        return p1
    
    def merge(self, h1, h2): # return h
        p1,p2 = h1,h2
        while p1 and p2:
            p3,p4 = p1.next, p2.next
            p1.next = p2
            p2.next = p3
            p1 = p3
            p2 = p4
        return h1

    def buildList(self, value_array):
        if len(value_array) == 0:
            return None
        else:
            value = value_array
            head = ListNode(value[0])
            latest_node = head
            for i in range(1, len(value)):
                new_node = ListNode(value[i])
                latest_node.next = new_node
                latest_node = new_node
        return head
    
    def printList(self, head):
        p = head
        if not p:
            return
        while p != None:
            print p.val
            p = p.next

if __name__ == '__main__':
    sl = Solution()
    # a = []
    # a = [1,2]
    a = [1,2,3]
    head = sl.buildList(a)
    # sl.printList(head)
    head = sl.reorderList(head)
    # hh = sl.reverseList(head)
    sl.printList(head)
        