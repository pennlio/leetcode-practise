#!\bin\env\python 

class Solution:
    ''' @return an integer'''
    def __init__ (self):
        self._digit_stack = [];
        self._mod = 10;
    
    def reverse(self,x):
        if x<0:
            negative_flag = 1;
            x = -x;         # turn into positive number
        else:
            negative_flag = 0;
        while (x!=0):
            x_residual = x % self._mod; #obtain the residual 
            self._digit_stack.append(x_residual/((self._mod)/10)); #push current digit into stack
            x = x - x_residual;
            # print self._digit_stack;
            self._mod = self._mod * 10;
            
        '''output number reversely'''
        x_output = 0;
        self._mod = 1;
        while(1):
            try:
                x_digit = self._digit_stack.pop();  #get the digits reversely
            except IndexError, e:
                # Index Error mean "stack empty";
                break;
            else:
                x_output = self._mod * x_digit + x_output;
                self._mod = self._mod * 10 ;

        return -x_output if negative_flag else x_output  # sign correction

if __name__ == '__main__':
    solution = Solution();
    x = 0;
    x_output = solution.reverse(x);
    print x_output;



    		

    	
