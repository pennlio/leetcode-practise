# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    # @param head, a ListNode
    # @param k, an integer
    # @return a ListNode
    def reverseKGroup(self, head, k):
        if not head or k == 0:
            return head # empty list
        if k == 1: # original list
            return head
        count = k
        p0 = sentinel = ListNode(-1)  # sentinel node
        sentinel.next = head

        pk = head 
        while pk != None:
            pk = pk.next
            count -= 1
            if not count:
                p0 = self.reverseListPart(p0.next,pk,p0)
                count = k # restart conunting
        return sentinel.next

    def reverseListPart(self, start, end, prev):
        if start.next == end: # length one
            return end
        else:
            p1 = start
            p2 = p1.next
            p3 = p2.next
        while p3 != end:
            p2.next = p1
            p1 = p2
            p2 = p3
            p3 = p2.next
        p2.next = p1
        start.next = end
        prev.next = p2
        return start  # only return tailnode of reversed list


    def buildList(self, value_array):
        if len(value_array) == 0:
            return None
        else:
            value = value_array
            head = ListNode(value[0])
            latest_node = head
            for i in range(1, len(value)):
                new_node = ListNode(value[i])
                latest_node.next = new_node
                latest_node = new_node
        return head
    
    def printList(self, head):
        p = head
        if not p:
            return
        while p != None:
            print p.val
            p = p.next
        
if __name__ == '__main__':
    sl = Solution()
    head = sl.buildList([1,2,3])
    k = 2
    h = sl.reverseKGroup(head,k)
    sl.printList(h)
        # start = ListNode(-1)
        # p = start
        # p.next = head
        # while p != None:
        #     p = p.next
        #     count -= 1
        #     if count == 0:


