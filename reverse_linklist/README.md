#Reverse Linklist II (derivde from I)
======

##concepts:
1. make use of `reverse linklist I`
2. each iteration reverse only one pointer


##Runtime: 
- one pass
- O(n) in time
- in-place

##Corner case:
1. when reverse to the end: `p.next.next` may dose not exit
2. whe reverse from the head: `p_before_start` may be `None`
3. when `cnter` reaches `n`, need to reveser this node too, but after that`p_cur` is at the n+1 node

##Improvment:
1. datastructure: no since its in-place
2. algorithm:  use `sentinel` to signify the end 