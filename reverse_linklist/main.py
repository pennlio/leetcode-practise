# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    # @param head, a ListNode
    # @param m, an integer
    # @param n, an integer
    # @return a ListNode
    def reverseBetween(self, head, m, n):
        if m == n:
            return head
        else:
            start_node = head
            cnter = 1
            if m == 1:
                before_s_node = None
                start_node = head
            else:
                while cnter < m:
                    before_s_node = start_node  # keep one before start_node
                    start_node = start_node.next  #find start_node
                    cnter += 1

            if start_node.next == None: # no need to reverse last node
                return head
            else:
                p_pre = before_s_node;
                p_cur = start_node
                p_next = start_node.next
            while cnter <= n:
                p_cur.next = p_pre
                p_pre = p_cur
                p_cur = p_next
                try:
                    p_next = p_next.next  #!!! if p_next is None it is the end; save it to None anyway
                except AttributeError, e:
                    p_next = None
                finally:
                    cnter += 1
            if before_s_node == None:  #!!!check if the before_start_node is None
                head = p_pre
            else:
                before_s_node.next = p_pre   #!!! here is p_pre not p_cur
            start_node.next = p_cur
            return head

    def reverse(self, head):
        if head == None or head.next == None:
            return head
        else:
            p_pre = None
            p_cur = head
            p_next = head.next
            
            while p_next!=None:
                p_cur.next = p_pre # each step reverse only one
                p_pre = p_cur
                p_cur = p_next
                p_next = p_cur.next
            
            head = p_cur;
            p_cur.next = p_pre # carefull!!
        return head

    def buildList(self, value_array):
        if len(value_array) == 0:
            return None
        else:
            value = value_array
            head = ListNode(value[0])
            latest_node = head
            for i in range(1, len(value)):
                new_node = ListNode(value[i])
                latest_node.next = new_node
                latest_node = new_node
        return head

    def printList(self, head):
        p = head
        while p!=None:
            print p.val
            p = p.next

if __name__ == '__main__':
    value = [1,2,3,4]
    # value = [1,2]
    sol = Solution()
    head = sol.buildList(value)
    # head.val = value[0]
    next_node = head
    # while next_node!=None:
    #   print next_node.val
    #   next_node = next_node.next

    # head = sol.reverse(head)
    head = sol.reverseBetween(head,1,3)

    sol.printList(head)