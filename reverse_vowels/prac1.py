class Solution:
    def reverseVowels(self, string):
        if not string: return ''
        vowel = ['a','e','i','o','u']
        string = list(string)
        i, j = 0, len(string)-1
        while i<j:
            if string[i] not in vowel:
                i += 1
            elif string[j] not in vowel:
                j -= 1
            else:
                string[i], string[j] = string[j], string[i]
                j -= 1
                i += 1
        return ''.join(string)

 
if __name__ == '__main__':
    sl = Solution()
    # string = 'barcelona'
    # string = 'christina'
    # string = 'read madrid'
    # string = 'hello kitty'
    # string = 'goldman saches'
    # string = 'microsoft'
    # string = 'xichang'
    # string = 'natural language processing'
    # string = 'iniesta'
    # string = 'portfolio'
    # string = 'waterloo'
    # string = 'adelaide'
    print sl.reverseVowels(string)
