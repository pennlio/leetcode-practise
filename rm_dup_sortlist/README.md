#Remove duplicates in sort list
------
## I

###concept:
- scan and skip duplicates

###running eff: 
- time: O(n) 
- space: in place
- One pass


## II

###concept:
- scan and compare `first_met` node's value with its next neighbour's value
- in place with low index 

###conrner case:
- when the whole l.l is the same value : error capture
- when the last node has a unique value : error capture


###running eff:
- time: O(n)
- space: in place
- one pass

###improve:
- running time: 