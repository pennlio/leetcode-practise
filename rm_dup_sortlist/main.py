# !\bin\python
# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    # @param head, a ListNode
    # @return a ListNode
    def deleteDuplicates(self, head):
    	if head == None or head.next == None:
    		return head
    	else:
    		latest_node = head
    		current_node = head.next
    		while current_node!=None:
    			if (current_node.val == latest_node.val):
    				current_node = current_node.next
    			else:
    				latest_node.next = current_node
    				latest_node = current_node  #update latest_node
    		latest_node.next = None
    		return head

if __name__ == "__main__":
	value = [1,1,2,2,2,3,4,5,5,7,8,8,8,10]
	# value = [1,2]

	head = ListNode(value[0])
	# head.val = value[0]
	latest_node = head
	for i in range(1, len(value)):
		new_node = ListNode(value[i])
		# new_node.val = value[i]
		latest_node.next = new_node
		latest_node = new_node

	next_node = head

	sol = Solution()
	head = sol.deleteDuplicates(head)
	while next_node!=None:
		print next_node.val
		next_node = next_node.next


