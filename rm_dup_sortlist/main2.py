# !\bin\python
# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    # @param head, a ListNode
    # @return a ListNode
    def deleteDuplicates(self, head):
    	if head == None or head.next == None:
    		return head
    	else:
            latest_node = None # to save the in list elemt
            current_node = head
            first_met = None # start
            while current_node!=None:
                if current_node.val == first_met:
                    current_node = current_node.next  # meet twice, skip this node
                else:  # meet a new value
                    first_met = current_node.val # update first_met
                    if current_node.next == None: # this is the last node
                        try:
                            latest_node.next = current_node
                        except AttributeError,e:
                            head = ListNode(current_node.val)
                            latest_node = head
                        else:
                            latest_node = current_node # end
                        finally:
                            break
                    if current_node.next.val == first_met: # has dup
                        current_node = current_node.next
                    else: # only value
                        try:
                            latest_node.next = current_node
                        except AttributeError, e:
                            head = ListNode(current_node.val)  #create head node
                            latest_node = head
                        else:
                            latest_node = current_node
                        finally:
                            current_node = current_node.next
            if latest_node == None: # if it is a empty list
                return None
            else:
                latest_node.next = None
                return head
    		


if __name__ == "__main__":
    # value = [1,1,2,2,2, 2,2,3,3,4,4,5,5,7,7,8,8,8,10,10]
    value = [1,1,2]
    # value = [1,1]

    head = ListNode(value[0])
    # head.val = value[0]
    latest_node = head
    for i in range(1, len(value)):
    	new_node = ListNode(value[i])
    	# new_node.val = value[i]
    	latest_node.next = new_node
    	latest_node = new_node

    sol = Solution()
    newhead = sol.deleteDuplicates(head)
    next_node = newhead
    while next_node!=None:
    	print next_node.val
    	next_node = next_node.next
