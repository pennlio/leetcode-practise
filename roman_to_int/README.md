#Roman to Integer
------

##concept 
- check input one by one
- check if `X1` > `X2`, add X1 to sum; o/w substract X1 from sum;

##runing time: O(n)

##another way: 
- to form two sets: `plus_set` and `minus_set`;
- `final` = `plus_set` - `minus_set`