class Solution:
    # @return an integer
    def romanToInt(self, s):
    	if s==None:
    		return 0
    	else:
    		roman_dict = {'I':1,'X':10,'V':5,'L':50, 'C':100, 'M':1000, 'D':500}
    		letters = list(s)
    		length = len(letters)
    		total_sum = 0
    		current_value = 0;
    		for i in range(length):
    			last_value = current_value
    			current_value = roman_dict[letters[i]]
    			# print current_value
    			if last_value >= current_value:
    				total_sum += last_value
    				continue
    			else :
    				total_sum -= last_value
    		total_sum += current_value  #last one must be positive
    	return total_sum



if __name__ == "__main__":
	s = 'MMDCCCXC'
	sl = Solution()
	total_sum = sl.romanToInt(s)
	print total_sum
	# print letters
        