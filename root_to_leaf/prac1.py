# Definition for a  binary tree node
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    # @param root, a tree node
    # @return an integer
    def __init__(self):
        self._numStack = []
        self._results = []
    def sumNumbers(self, root):
        # use DFS
        if not root:
            return 0
        self.DFStraverse(root)
        print self._numStack
        return sum(self._results)

    def DFStraverse(self, root):
        self._numStack.append(root.val)
        if not root.left and not root.right:
            self._results.append(self.convert(self._numStack)) # leaf node
        if root.left:
            self.DFStraverse(root.left)
            self._numStack.pop()
        if root.right:
            self.DFStraverse(root.right)
            self._numStack.pop()
        return

    def convert(self, num): # convert to int
        # string =[str(x) for 
        n = len(num)
        ans = 0
        for i in range(n-1,-1,-1): # range(1,2,1)
            ans += num[i]*10**(n-1-i)
        return ans

if __name__ == '__main__':
    sl = Solution()
    root = TreeNode(3)
    root.left = TreeNode(1)
    root.right = TreeNode(2)
    root.left.right = TreeNode(2)
    # num = [0,0,2,3,4,0,9]
    # print sl.convert(num)
    print sl.sumNumbers(root)