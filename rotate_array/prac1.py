class Solution:
    def rotateArray(self, A, i):
        if not A or i <= 0 :return A
        i = i % len(A)
        return self.rotate(A,i,0,len(A)-1)

    def rotate(self, A, i, low, high):
        if low >= high or i == 0: return A
        if i <= (high-low+1)/2:  # ab1b2
            A = self.swap(A, i, low, high)  # swap a,b2
            A = self.rotate(A, i, low, high-i)
        else: #a1a2b
            A = self.swap(A, high-low+1-i, low, high)
            A = self.rotate(A, 2*i-high+low-1, high-i+1, high)
        return A

    def swap(self, A, l, start, end):
        if l <= 0: return A
        for i in range(l):
            A[start+i], A[end+i-l+1] = A[end-l+i+1], A[start+i]
        return A

if __name__ == '__main__':
    sl = Solution()
    # A = [1,2,3,4,5]
    A = [1,2,3]
    i =4
    print sl.rotateArray(A,i)