class Solution:
    def __repr___(self):
        # print "aa"
        return 'aa'
    def rotateArray(self, A, i):
        if not A or i <= 0 :return A
        i = i % len(A)
        a = A[:i]
        b = A[i:]
        a.reverse()
        b.reverse()
        c = (a+b)
        c.reverse()
        return c
        # return reversed(reversed(A[:i]) + reversed(A[i:]))

if __name__ == '__main__':
    sl = Solution()
    A = [1,2,3,4,5]
    i = 2
    print sl.rotateArray(A,i)
    # print sl
