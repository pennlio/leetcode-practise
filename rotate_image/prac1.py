class Solution:
    # @param matrix, a list of lists of integers
    # @return a list of lists of integers
    def rotate(self, mx):
        n = len(mx)
        if n <= 1 :
            return mx
        # buf = [-1] * len(mx) # init buf
        buf = []
        '''cannot buffer an entire row for one time; much easier to buffer only one element using for loop'''
        # start from outer layer
        for i in range(n//2): # only rotate half
            for j in range(i, n-i):  # build buf
                buf.append(mx[j][n-i-1])
            # print buf
            # corner value
            # mx[j][n-i-1] = mx[i][j]  # !!!!! replaced corner
            # head to buf
            # for 
            # for j in range(i,n-i):

                # print mx[i][j]
        # print buf
        # print mx


if __name__ == '__main__':
    sl = Solution()
    mx = [[1,2],[3,4]]
    sl.rotate(mx)
