class Solution:
    # @param matrix, a list of lists of integers
    # @return a list of lists of integers
    def rotate(self, matrix):
        if not matrix:
            return matrix
        n = len(matrix)
        if n == 1:
            return matrix
        # x.reverse() for x in matrix:
        matrix.reverse()
        for i in range(n):
            for j in range(i, n):
                self.swap(matrix, i,j)
        return matrix

    def swap(self, matrix, i, j):
        if i == j:
            return
        else:
            temp = matrix[i][j]
            matrix [i][j] = matrix[j][i]
            matrix[j][i] = temp
            return

if __name__ == '__main__':
    sl = Solution()
    mx = [[1,2],[3,4]]
    mx = [[1,2,3],[4,5,6],[7,8,9]]
    print sl.rotate(mx)


