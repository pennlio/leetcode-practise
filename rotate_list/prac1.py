# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    # @param head, a ListNode
    # @param k, an integer
    # @return a ListNode
    def rotateRight(self, head, k):
        if not head or head.next == None:
            return head
        length = 0
        p = head
        while p!=None:
            length += 1
            p = p.next

        move = k % length
        if not move:
            return head
        p2 = head

        while move > 0:
            p2 = p2.next # label finaly pos
            move -= 1
        p1 = head
        while p2.next != None:
            p2 = p2.next
            p1 = p1.next
        p2.next = head
        p3 = p1.next
        p1.next = None
        return p3

    def buildList(self, value_array):
        if len(value_array) == 0:
            return None
        else:
            value = value_array
            head = ListNode(value[0])
            latest_node = head
            for i in range(1, len(value)):
                new_node = ListNode(value[i])
                latest_node.next = new_node
                latest_node = new_node
        return head

    def printList(self, head):
        p = head
        if not p:
            return
        while p != None:
            print p.val
            p = p.next

if __name__ == '__main__':
    sl = Solution()
    head = sl.buildList([1,2,3,4,5])
    k = 5
    h2 = sl.rotateRight(head,k)
    sl.printList(h2)



