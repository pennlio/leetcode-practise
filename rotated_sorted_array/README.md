Rotate Sorted Array
===========
- log(n), binary search

Corner Case
-----
- when x has only two ordered inputs, shall output x[0]

BUGs
----------
- words = x.split ---- only split when there is space, or comma....
- x = [n/2];  --- x is a list not a number!!!
- x = math.ceil(n/2)  --- x is a float number
- x[p1,p2]  --- from x[p1] to x[p2-1] , no p2 !!!
- return x  --- return a list not  a  number




