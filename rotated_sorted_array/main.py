# import math
class Solution:
    # @param num, a list of integer
    # @return an integer
    def findMin(self, num):
    	n =len(num)
    	if n == 0:
    		print "wrong input"
    		return
    	elif n == 1:
    		return num[0]
    	mid = n/2
    	# print mid
    	if num[0]<=num[mid] and num[mid]<=num[n-1]:
    		return num[0]
    	elif num[0]<num[mid] and num[mid]>num[n-1]:
    		# print num[mid+1:n]
    		return self.findMin(num[mid+1:n]) #remove mid
    	else:
    		# print num[1:mid+1]
    		return self.findMin(num[1:mid+1]) #include mid


if __name__ == '__main__':
	sl = Solution()
	# a = [1,2,3,4,5]
	a = [2,1]
	# a = [2,3,4,5,1]
	# a = [3,4,5,1,2]
	print sl.findMin(a)