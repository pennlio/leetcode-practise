'''12/26/2014'''
# Definition for a  binary tree node
# use recursion
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    # @param p, a tree node
    # @param q, a tree node
    # @return a boolean
    def isSameTree(self, p, q):
        if p == None and q == None:
            return True
        elif (p == None and q!= None) or (p != None and q == None):
            return False
        elif p.val != q.val:
            return False
        else:
            ls = self.isSameTree(p.left, q.left)
            rs = self.isSameTree(p.right, q.right)
        if ls == rs == True:
            return True
        else:
            return False

if __name__ == '__main__':
    sl = Solution()
    p = TreeNode(1)
    q = TreeNode(1)
    s = TreeNode(2)
    q.left = s
    t = TreeNode(3)
    p.right = t
    print sl.isSameTree(p,q)
