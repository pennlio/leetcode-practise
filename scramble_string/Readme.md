concept:
-------
- DP: recursively verify sequences
- self.isScramble(s1[:i],s2[-i:]) and self.isScramble(s1[i:],s2[:-i]):

impel:
--------
- set is built by : set([a,b]) --> set = {[a,b]}
- but set is not hashable

corner case:
----------
- when two children have different length, verify 