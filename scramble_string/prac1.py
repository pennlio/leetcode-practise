class Solution:
    # @return a boolean
    def __init__(self):
        self._hash = {}
    def isScramble(self, s1, s2):
        '''two strings can cut at any position'''
        if s1 == '' and s2 == '':
            return True
        if s1 == '' or s2 == '':
            return False
        if (s1,s2) not in self._hash:
            if s1 == s2: 
                self._hash[(s1,s2)] = True
            elif sorted(s1) != sorted(s2):
                self._hash[(s1,s2)] = False
            else:
                n = len(s1)
                for i in range(1,n):
                    if self.isScramble(s1[:i],s2[:i]) and self.isScramble(s1[i:],s2[i:]):
                        self._hash[(s1,s2)] = True
                        break
                    elif self.isScramble(s1[:i],s2[-i:]) and self.isScramble(s1[i:],s2[:-i]):
                        self._hash[(s1,s2)] = True
                        break
                if (s1,s2) not in self._hash:
                    self._hash[(s1,s2)] = False 
        return self._hash[(s1,s2)]

if __name__ == '__main__':
    sl = Solution()
    # s1 = 'great'
    # s1 = 'barcelona'
    # s2 = 'rgtae'
    # s2 = 'rcbaelona'
    s1 = 'abb'
    s2 = 'bba'

    print sl.isScramble(s1,s2)
        