class Solution:
    # @return a boolean
    def isScramble(self, s1, s2):
        if len(s1) != len(s2):
            return False
        if s1 == s2:
            return True
        s1Sorted = ''.join(sorted(s1))
        s2Sorted = ''.join(sorted(s2))
        if s1Sorted != s2Sorted:
            return False
        l = len(s1)
        for i in range(1, l):
            s11 = s1[:i]
            s12 = s1[i:]
            s21 = s2[:i]
            s22 = s2[i:]
            if self.isScramble(s11, s21) and self.isScramble(s12, s22):
                return True
            s21 = s2[-i:]
            s22 = s2[:-i]
            if self.isScramble(s11, s21) and self.isScramble(s12, s22):
                return True
        return False

if __name__ == '__main__':
    sl = Solution()
    # s1 = 'great'
    # s1 = 'barcelona'
    # s2 = 'rgtae'
    # s2 = 'rcbaelona'
    s1 = 'abb'
    s2 = 'bba'

    print sl.isScramble(s1,s2)