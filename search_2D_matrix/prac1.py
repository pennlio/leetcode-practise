class Solution:
    # @param matrix, a list of lists of integers
    # @param target, an integer
    # @return a boolean
    def searchMatrix(self, matrix, target):
        if not matrix:
            return False
        nRow = len(matrix)
        nCol = len(matrix[0])

        buf = [matrix[i][0] for i in range(0,nRow)]
        i = self.BinarySearch(buf,target,0,nRow-1)
        if i < 0: return False
        if matrix[i][0] == target:
            return True
        else:
            j = self.BinarySearch(matrix[i],target,0,nCol-1)
            if matrix[i][j] == target:
                return True
            else:
                return False


    def BinarySearch(self, array, target, low, high):
        if low > high:
            return high  # since to find the closest lower index of target, so return high (here it is mid-1 when tgt > array[mid] in last recursion)
        if low == high:
            if array[low] <= target:
                return low # since array[low+1] must >= target 
            else:
                return low-1 # since array[low-1] must < target 
        else:
            mid = (high+low)/2
            print array[mid]
            if target == array[mid]:
                return mid
            elif target > array[mid]:
                return self.BinarySearch(array, target, mid+1, high)
            else:
                return self.BinarySearch(array, target, low, mid-1)

if __name__ == '__main__':
    sl = Solution()
    matrix = [  [1,   3,  5,  7],[10, 11, 16, 20],[23, 30, 34, 50]]
    # matrix = [[-10,-8],[-6,-5],[-2,-2],[-1,0],[3,4],[7,7],[8,9],[10,10],[11,11],[12,14],[15,16],[17,19],[20,21],[22,22],[25,27],[28,30],[32,32],[35,36]]
    target = 10
    print sl.searchMatrix(matrix, target)


