class Solution:
    # @param A, a list of integers
    # @param target, an integer to be searched
    # @return a list of length 2, [index1, index2]
    def searchRange(self, A, target):
        if not A:
            return [-1,-1]
        n = len(A)
        if n == 1:
            if A[0] == target:
                return [0,0]
            else:
                return [-1,-1]
        target_pos = self.BinarySearch(A, 0, n-1, target)
        if target_pos == -1:
            return [-1,-1]
        left = right = target_pos
        while left >=0 and A[left] == target:
            left -= 1
        while right <= len(A)-1 and A[right] == target:
            right += 1
        return [left+1, right-1]

    def BinarySearch(self, A, low, high, target):  # include high
        if low > high: # 
            return -1
        if low == high:
            if A[low] == target:
                return low
            else:
                return -1
        mid = (low+high)/2
        if A[mid] == target: 
            return mid
        elif A[mid] < target:
            return self.BinarySearch(A, mid+1, high, target)
        else:
            return self.BinarySearch(A, low, mid-1, target)

    # def LeftedgeBinarySearch(self, A, low, high, target):
    #     if low > high:
    #         return -1
    #     if low == high:
    #         # if A[low] < target and A[low+1] == target:
    #             # return low+1
    #         if A[low] == target:
    #             if low == 0:
    #                 return 0
    #             elif A[low-1] != target:
    #                 return low
    #             else:
    #                 return 
    #         # A[low] can never be larger than target
    #     mid = (low+high)/2
    #     if A[mid] == target: # search left
    #         pos = self.LeftedgeBinarySearch(A,low, mid-1,target)
    #     else: #A[mid]<target:
    #         if A[mid+1] == target:
    #             return mid+1
    #         else:
    #             pos = self.LeftedgeBinarySearch(A, mid+1,high,target)
    #     return pos

    # def RightedgeBinarySearch(self, A, low, high, target):
    #     if low > high:
    #         return -1
    #     if low == high:
    #         if A[high] == target:
    #             if high == len(A)-1:
    #                 return len(A)-1
    #             elif A[]
    #                 return high


        # midLow =(low+target_pos)/2
        # if A[midLow]
        # midHigh =(high+target_pos)/2

if __name__ == '__main__':
    sl = Solution()
    A = [1,2,3,8,8,8,8,9,10]
    # A = [8,8]
    A = []
    target = 8
    print sl.searchRange(A,target)