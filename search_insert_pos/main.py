class Solution:
    # @param A, a list of integers
    # @param target, an integer to be inserted
    # @return integer
    def searchInsert(self, A, target):
        if not A:
            return 0
        pos = 0
        for x in A:
            if target <= x:
                return pos
            else:
                pos += 1
        return pos

if __name__ == '__main__':
    sl = Solution()
    A = [2,3,5,6]
    # A=[]
    target = 0
    print sl.searchInsert(A,target)