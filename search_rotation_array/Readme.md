Search in rotated array
======
main2.py

Concepts:
- by any means, A must be concatenated with 2 monotone arrays; 
- at least [A[low, A[mid]] or [A[mid], A[high]] is sorted 
- find the structure of array first: A[low] > < A[high]? which part 
- dicuss from the sorted part

Corner case:
----
1. n=2 case; how to deal with it more efficiently
2. n=1 case

Improvement
----
- how to run more efficiently?


Search in dup rotated array
======
main_dup.py

Concepts:
- almost same as I
- handle A[low] = A[high]: recursion (convert it to known cases)
- when rotated: pay attention to boundary `=` conditions