#!bin\pytohn
# recursion: must use array aboslote indices, o/w index error
# corner case: when not rotated; 
# fix: add both boundary condition for case 1 and 2
class Solution:
    # @param A, a list of integers
    # @param target, an integer to be searched
    # @return an integer
    def search(self, A, target):
    	n = len(A)
    	if n==0: return -1
    	if n==1:
    		if (target == A[0]):
    			return 0
    		else:
    			return -1
    	high = n-1
    	low = 0
        idx = self.doSearch(A, high, low, target)
    	# mid = n/2  #find mid
        return idx

    def doSearch(self, A, high, low, target):
        if low == high:
            if target == A[low]:
                return low
            else:
                return -1
        elif low + 1 == high: # deal with n=2
            if A[low] == target: 
                return low
            elif A[high] == target: 
                return high
            else:
                return -1
        mid = (high+low)/2
    	if A[mid] == target:
    		return mid
    	elif A[mid] >= high:  # case 1
    		if (target>A[mid]) or (target <= high and target < low):
                low = mid
                index = self.doSearch(A, high, low, target)
    		else:
                high = mid				
    			index = self.doSearch(A, high, low, target)
    	else:				# case 2
    		if (target<A[mid]) or (target > high and target >= low):
                high = mid
    			index = self.doSearch(A, high, low, target)
    		else:
                low = mid
    			index = self.doSearch(A, high, low, target)
    	return index


if __name__ == '__main__':
    # A = [4,1,2,3]
    # A = [8,9,1,2,5,6]
    A = [4,5,6,7,8,1,2,3]
    sl = Solution()
    idx = sl.search(A,1)
    print idx
