# !\bin\python

class Solution:
	# @param A, a list of integers
    # @param target, an integer to be searched
    # @return an integer
    def search(self, A, target):
    	n = len(A)
    	if n == 0: return -1
    	high = n - 1
    	low = 0
    	idx = self.doSearch(A, high, low, target)
    	return idx

    def doSearch(self, A, high, low, target): # add high/low as abs index
    	if low == high: # A has only one ele
    		if target == A[low]:
    			return low
    		else:
    			return -1
    	elif low + 1 == high:  #deal with n=2
    		if A[low] == target: 
    			return low
    		elif A[high] == target: 
    			return high
    		else:
    			return -1
    	mid = (high+low)/2

    	if A[mid] == target:
    		return mid
    	elif A[low] < A[high]: # not rotated; use binary search
    		if A[mid] >= target:
    			high = mid
    			idx = self.doSearch(A, high, low, target)
    		else:
    			low = mid
    			idx = self.doSearch(A, high, low, target)
    	elif A[low] > A[high]: # rotated, pay attention to structure
    		if A[high] < target < A[low]:
    			return -1
    		if A[low] < A[mid]: # left part is sorted
    			if A[mid] > target >= A[low]:
    				high = mid
    			else:
    				low = mid
    		elif A[mid] < A[high]: # right part is sorted
    			if A[mid] < target <= A[high]:
    				low = mid
    			else:
    				high = mid
    		idx = self.doSearch(A, high, low, target)
    	return idx


if __name__ == '__main__':
	A = [4,5,6,7,8,1,2,3]
	# A = [1]
	# A = [2,3,1]
	# A = [3,1]
	# A = [1,2,3]
	target = 3
	sl = Solution()
	idx = sl.search(A,target)
	print idx
        