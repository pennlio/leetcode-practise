# !\bin\python

class Solution:
	# @param A, a list of integers
    # @param target, an integer to be searched
    # @return an integer
    def search(self, A, target):
    	n = len(A)
    	if n == 0: return False
    	high = n - 1
    	low = 0
    	label = self.doSearch(A, high, low, target)
    	return label

    def doSearch(self, A, high, low, target):
    	if low == high: # A has only one ele
    		if target == A[low]:
    			return True
    		else:
    			return False
    	elif low + 1 == high:  #deal with n=2
    		if A[low] == target: 
    			return True
    		elif A[high] == target: 
    			return True
    		else:
    			return False
    	mid = (high+low)/2

    	if A[mid] == target:
    		return True
    	elif A[low] < A[high]: # not rotated; use binary search
    		if A[mid] >= target:
    			high = mid
    			idx = self.doSearch(A, high, low, target)
    		else:
    			low = mid
    			idx = self.doSearch(A, high, low, target)
    	elif A[low] > A[high]: # rotated, pay attention to structure
    		if target < A[low] and target > A[high]:
    			return False
    		if A[low] <= A[mid]: # left part is sorted
    			if target < A[mid] and target >= A[low]:
    				high = mid
    			else:
    				low = mid
    		elif A[mid] <= A[high]: # right part is sorted
    			if target <= A[high] and target > A[mid]:
    				low = mid
    			else:
    				high = mid
    		idx = self.doSearch(A, high, low, target)
        else: # A[low] = A[high] use recursion
            idx = max(self.doSearch(A,high, mid, target), self.doSearch(A, mid, low, target))
        return idx

if __name__ == '__main__':
    # A = [3,3,3,3,5,1,2,3]
    # A = [1]
    # A = [2,3,1]
    A = [3,1,1,1,1]
    # A = [0,0,1,1,2,0]
    # A = [0,0,1]
    # A = [1,1,2,0]
    # A = [1,2,3]
    target = 1
    sl = Solution()
    idx = sl.search(A,target)
    print idx
        