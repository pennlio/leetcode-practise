class Solution:
    # @param matrix, a list of lists of integers
    # RETURN NOTHING, MODIFY matrix IN PLACE.
    def setZeroes(self, matrix):
        # constant extra space: record status within matrix
        if not matrix:
            return matrix
        nRow = len(matrix) 
        nCol = len(matrix[0])
        row0 = 1
        col0 = 1
        for i in range(nRow):
            for j in range(nCol):
                if matrix[i][j] == 0:
                    if i == 0:
                        row0 = 0
                    else:
                        matrix[i][0] = 0 #label row i status with matrix[i][0]
                    if j == 0:
                        col0 = 0
                    else:
                        matrix[0][j] = 0
                    # break             # cannot break, since there my be other j s.t max[i][j] == 0

        for i in range(1, nRow):
            if matrix[i][0] == 0:
                self.setRowZero(matrix,i)    
        for j in range(1, nCol):
            if matrix[0][j] == 0:
                self.setColZero(matrix,j)
        # finally overite 0 row and 0 col (the index row)
        if row0 == 0:
            self.setRowZero(matrix, 0)
        if col0 == 0:
            self.setColZero(matrix,0)
        return matrix
    
    def setRowZero(self, matrix, rowNum):
        nCol = len(matrix[0])
        for i in range(nCol):
            matrix[rowNum][i] = 0
        return

    def setColZero(self, matrix, colNum):
        nRow = len(matrix) 
        for j in range(nRow):
            matrix[j][colNum] = 0
        return

if __name__ == '__main__':
    sl = Solution()
    matrix = [[1,2,3],[0,0,6],[7,0,9]]
    matrix = [[3,5,5,6,9,1,4,5,0,5],[2,7,9,5,9,5,4,9,6,8],[6,0,7,8,1,0,1,6,8,1],[7,2,6,5,8,5,6,5,0,6],[2,3,3,1,0,4,6,5,3,5],[5,9,7,3,8,8,5,1,4,3],[2,4,7,9,9,8,4,7,3,7],[3,5,2,8,8,2,2,4,9,8]]
    # matrix = [[0,0,0,5],[4,3,1,4],[0,1,1,4],[1,2,1,3],[0,0,1,1]]
    print sl.setZeroes(matrix)

        