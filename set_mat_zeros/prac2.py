class Solution:
# @param matrix, a list of lists of integers
# RETURN NOTHING, MODIFY matrix IN PLACE.
    def setZeroes(self, matrix):
        rows=set();
        columns=set(); # use set here avoid dup
        for i in range(len(matrix)):
            for j in range(len(matrix[0])):
                if(matrix[i][j]==0):
                   rows.add(i);  # avoid dup
                   columns.add(j);
    
        for row in rows:
            for i in range(len(matrix[0])):
                matrix[row][i]=0;
        for col in columns:
            for i in range(len(matrix)):
                matrix[i][col]=0;