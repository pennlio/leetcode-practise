class Solution:
    # @param path, a string
    # @return a string
    def simplifyPath(self, path):
        #'''using stack'''
        if not path:
            return paht
        fStack = []
        curFolder = '' # string
        path += '/' # add as sentinel 
        for x in path:
            if x == '/':
                if curFolder!= '': # have some thing
                    if curFolder == '..':
                        if fStack != []:
                            fStack.pop()
                    elif curFolder != '.':
                        fStack.append(curFolder)
                    curFolder = ''
            else: # do not save "/"
                curFolder += x
        
        outF = ''
        if not fStack:
            outF = '/'
        else:
            for x in fStack:
                outF += ("/" + x)
        return outF

if __name__ == '__main__':
    sl = Solution()
    path = "/a/./b/../..//c/"
    # path = "/../a/bsd/"
    # path = "/home/"
    print sl.simplifyPath(path)


