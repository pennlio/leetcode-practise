class Solution:
    def simplifyPath(self, path):
        if not path:
            return path
        fdStack = []
        path = path+'/' #sentinel
        curFd = ''
        for x in path:
            if x == '/':
                if curFd != '': # have smthing
                    if curFd == '..':
                        if fdStack != []:
                            fdStack.pop()
                    elif curFd != '.':
                        fdStack.append(curFd)
                    curFd = ''
            else:
                curFd += x
        if fdStack == []:
            out = '/'
        else:
            out = ''
            for y in fdStack:
                out += ('/'+ y )
        return out 

if __name__ == '__main__':
    sl = Solution()
    path = "/a/./b/../..//c/"
    # path = "/../a/bsd/"
    # path = "/home/"
    print sl.simplifyPath(path)