class Solution:
    # @param path, a string
    # @return a string
    def simplifyPath(self, path):
        if not path: return path
        buf = [] # save word met before /
        path = path + '/' # sentinel
        result = []
        for i, x in enumerate(path):
            if x == '/':
                if buf: # not empty
                    if buf == ['.','.']: 
                        if result:
                            result.pop() # last layer # if /.. what happens?
                    elif buf != ['.']:
                        result.append(''.join(buf))
                buf = []
            else: # other chars
                buf.append(x)
        return '/'+ '/'.join(result) # finally need append '/' at front