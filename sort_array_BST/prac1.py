# Definition for a  binary tree node
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    # @param num, a list of integers
    # @return a tree node
    def sortedArrayToBST(self, num):
        if not num:
            return None
        n = len(num)
        return self.buildTree(num,0,n)

    def buildTree(self, num, low, high): # high point is not included
        if low >= high:
            return None
        elif low == high - 1:
            return TreeNode(num[low])
        else:
            mid = (low + high)//2
            subRoot = TreeNode(num[mid])
            subRoot.left = self.buildTree(num,low,mid)
            subRoot.right = self.buildTree(num,mid+1,high) # exclude root
            return subRoot

    def inorderTraverse(self, root):
        if not root:
            return
        else:
            self.inorderTraverse(root.left)
            print root.val
            self.inorderTraverse(root.right)
        return
if __name__ == '__main__':
    sl = Solution()
    num = []
    num.sort()
    # print num
    root = sl.sortedArrayToBST(num)
    sl.inorderTraverse(root)