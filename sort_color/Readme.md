concept:
-------
- M1: count sort
- M2: in-place modify: two pointer p0 and p2 label the edge of 0s and 2s; 


imple:
--------
- while loop: self counter ++
- p0 starting from 0, so when the first 0 is swapped from end, it need skip a swap (cannot swap 0 on left end with any other elems)
- `i` should be compared with p2 finally, since p2 is boundary of 2s