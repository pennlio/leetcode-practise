class Solution:
    # @param A a list of integers
    # @return nothing, sort in place
    def sortColors(self, A):
        '''counting sort '''
        if not A:
            return 
        rcnt = wcnt = bcnt = 0
        for x in A:
            if x == 0:
                rcnt += 1
            elif x == 1:
                wcnt += 1
            else:
                bcnt += 1
        i = 0
        while i < len(A):
            if i < rcnt:
                A[i] = 0
            elif rcnt <= i < rcnt + wcnt:
                A[i] = 1
            else:
                A[i] = 2
            i += 1
        return A

if __name__ == '__main__':
    A = [2,1,2,0,2,1,1,0]
    sl = Solution()
    print sl.sortColors(A)

