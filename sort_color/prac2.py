'''one pass solution'''
class Solution:
    # @param A a list of integers
    # @return nothing, sort in place
    def sortColors(self, A):
        if not A:
            return
        n  = len(A)
        p0 = 0
        p2 = n - 1
        i = 0
        while i <= p2:  # still comapre when i=p2,since p2, p0 are labeled edge
            if A[i] == 0:
                if i == p0: # skip swap when 0 is within the left-end of A
                    p0 += 1
                    i += 1
                    continue
                self.swap(A,p0,i)
                # A[p0], A[i] = A[i], A[p0]
                p0 += 1
            elif A[i] == 2:
                self.swap(A,p2,i)
                # A[p2], A[i] = A[i], A[p2]
                p2 -= 1

            else:
                i += 1
        return A
    
    def swap(self,A,i,j):
        temp = A[i]
        A[i] = A[j]
        A[j] = temp
        return


if __name__ == '__main__':
    # A = [2,1,2,0,2,1,1,0]
    A = [0,0]
    sl = Solution()
    print sl.sortColors(A)

