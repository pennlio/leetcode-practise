#!\bin\python

# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

# use merge-sort
class Solution:
    # @param head, a ListNode
    # @return a ListNode
    def sortList(self, head):
    	if head is None or head.next is None:  
    		return head
    	else:
            (head, half) = self.cutHalf(head)
            # cut into two lists
            list1 = self.sortList(head)
            list2 = self.sortList(half)
            listout = self.ListMerge(list1,list2)
            return listout

    def ListMerge(self, a, b): # just use iterative merge
        start = ListNode(0)
        p = start
        while a and b:
            if a.val <= b.val:
                p.next = a
                a = a.next
            else:
                p.next = b
                b = b.next
            p = p.next
        if a: # b is None:
            p.next = a
        if b:
            p.next = b
        return start.next # includes when a==b==None

    def cutHalf(self, head):
        if not head:
            return (head, head)
        elif head.next is None:
            half = head.next
            head.next = None # cut in half
            return (head, half)
        else:   #runner tech find half
            fast = slow = head
            while fast is not None and fast.next is not None: #ensure fast goes to the final elem
                prev = slow 
                slow = slow.next
                fast = fast.next.next
            prev.next = None  # cut into half
            return (head, slow)

    '''the recursive mergesort takes too much space and time'''
    # def ListMerge(self, a, b): use recursion to merge is over time
    #     if a is None:
    #         return b
    #     elif b is None:
    #         return a  # if any of them is empty
    #     p = a
    #     q = b  # two pointer point to stack top
    #     if p.val <= q.val:
    #         temp = ListNode(p.val)
    #         temp.next = self.ListMerge(p.next, q)  # use recursion to merge lists
    #     else:
    #         temp = ListNode(q.val)
    #         temp.next = self.ListMerge(p,q.next)
    #     return temp


    def buildList(self, value_array):
        if len(value_array) == 0:
            return None
        else:
            value = value_array
            head = ListNode(value[0])
            latest_node = head
            for i in range(1, len(value)):
                new_node = ListNode(value[i])
                latest_node.next = new_node
                latest_node = new_node
        return head

    def printLinklist(self, headA):
        if not headA:
            return None
        p1 = headA
        while p1 != None:
            print p1.val
            p1 = p1.next

if __name__ == '__main__':
    sl = Solution()
    a = [3,5,1,7,8,6,0,65]
    # a = [12]
    head = sl.buildList(a)
    # head, half = sl.sortList(head)
    head = sl.sortList(head)
    sl.printLinklist(head)
    # sl.printLinklist(half)




