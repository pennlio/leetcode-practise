class Solution:
    # @param head, a ListNode
    # @return a ListNode
    def sortList(self, head):
        if not head:
            return None
        if head.next == None:
            return head
        # use merge-sort
        h1, h2 = self.cutHalf(head)
        h1 = self.sortList(h1)
        h2 = self.sortList(h2)
        return self.Merge(h1,h2)
    
    def cutHalf(self, head):
        p1 = head
        p2 = head
        while p2.next != None and p2.next.next!=None:
            p2 = p2.next.next
            p1 = p1.next
        p2 = p1.next # head of 2nd half
        p1.next = None
        return head, p2
    
    def Merge(self, h1, h2):
        sentinel = ListNode(-1)
        p = sentinel
        while h1 and h2:
            if h1.val <= h2.val:
                p.next = h1
                h1 = h1.next
            else:
                p.next = h2
                h2 = h2.next
            p = p.next
        if not h1:
            p.next = h2
        else:
            p.next = h1
        return sentinel.next