class Solution:
    def sortSqure(self, num): #
        if not num: return []
        B = []
        A = []
        for i, x in enumerate(num):
            if x < 0:
                B.append(x**2)
            else:
                A = [x**2 for x in num[i:]] # only fill A when there is elements >= 0
                break
        p1 = 0
        p2 = len(B)-1
        C = []
        while p1 < len(A) and p2 >= 0: # pay attention 
            if A[p1] <= B[p2]:
                C.append(A[p1])
                p1 += 1
            else:
                C.append(B[p2])
                p2 -= 1

        if p1 < len(A):
            return C + A[p1:]
        elif p2 >= 0:
            return C + B[p2::-1]

if __name__ == '__main__':
    sl = Solution()
    num = [-6,-3,-1, 0,0, 1,2,4,5,10]
    num = [-8,-5] 
    num = [-3,-2]
    print sl.sortSqure(num)