class Solution:
    # @param matrix, a list of lists of integers
    # @return a list of integers
    def spiralOrder(self, matrix):
        if not matrix:
            return []
        m = len(matrix) # m row
        n = len(matrix[0]) # n col
        if n == 0:
            return []
        if m == 1:
            return matrix[0]
        if n == 1:
            return [x[0] for x in matrix ]
        count = offset = 0
        result = []
        while count < m*n:
            i = j = offset
            for j in range(offset, n-offset):
                result.append(matrix[i][j])
                count += 1
            if count == m*n: break
            for i in range(offset+1, m-offset):
                result.append(matrix[i][j])
                count += 1
            if count == m*n: break
            for j in range(n-offset-2, offset-1, -1):
                result.append(matrix[i][j])
                count += 1
            if count == m*n: break
            for i in range(m-offset-2, offset, -1):
                result.append(matrix[i][j])
                count += 1
            if count == m*n: break
            offset += 1
        return result

if __name__ == '__main__':
    sl = Solution()
    # m,n = 3,4
    matrix = [
     [ 1, 2, 3,4 ],
     [ 5, 6, 7, 8 ],
     [ 9,10,11,12 ]
    ]
    # matrix = [[1,2,3], [4,5,6]]
    # matrix = [[1,3],[2,4]]
    # matrix = [[1],[2],[3]]
    # a = range(0,n)
    # b = [a for x ]
    print sl.spiralOrder(matrix)
