class Solution:
    # @param matrix, a list of lists of integers
    # @return a list of integers
    '''BUGS'''
    def spiralOrder(self, matrix):
        if not matrix : return []
        m = len(matrix) # row
        n = len(matrix[0]) # col
        if m == 1: 
            return matrix[0]
        elif n == 1: 
            return [x[0] for x in matrix]
        else:
            return self.printOrder(matrix,0)
    
    def printOrder(self, matrix, lv):
        m = len(matrix) # row
        n = len(matrix[0]) # col
        if lv >= min(m,n): return [] #over bound
        result = [matrix[lv][i] for i in range(lv, n-lv)]
        result.extend([matrix[j][n-lv-1] for j in range(lv+1, m-lv)])
        if lv+1 < m-lv-1:
            result.extend([matrix[m-lv-1][i] for i in range(n-lv-2, lv, -1)])
        else:
            result.extend([matrix[j][lv] for j in range(m-lv-1, lv, -1)])
        if lv+1 < n-lv-1:
            result.extend([matrix[j][lv] for j in range(m-lv-1, lv, -1)])
        else:
            result.extend([matrix[m-lv-1][i] for i in range(n-lv-2, lv, -1)])
        result.extend(self.printOrder(matrix, lv+1))
        return result
        # for i in range(n):
            
if __name__ == '__main__':
    # matrix = [[1,2,3],[4,5,6],[7,8,9]]
    matrix = [[2,3]]
    matrix = [[3],[2]]
    # matrix = [[5,6,7,8]]
    # matrix = [[1,2],[3,4]]
    sl = Solution()
    print sl.spiralOrder(matrix)