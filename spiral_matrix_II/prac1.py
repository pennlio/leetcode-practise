class Solution:
    # @return a list of lists of integer
    def generateMatrix(self, n):
        if n == 0:
            return []
        array = range(1,n*n+1)
        A = [[0] * n for x in range(n)]
        return self.generateInside(n,A,array,0,0)
    
    def generateInside(self, n, A,array,start,offset):

        num = 4*(n-1-2*offset)
        if num <= 0:
            if start == n*n - 1: # remain 1
                A[offset][offset] = array[start]
            return A
        else:
            A = self.buildMatrix(n, A, array[start:start+num], offset)
            A = self.generateInside(n, A, array, start+num, offset+1)  # pay attention start from start+num
            return A

    def buildMatrix(self, n, A, vector, offset):
        count = 0
        i = j = offset
        for j in range(offset, n-offset):
            A[i][j] = vector[count]
            count += 1
        for i in range(offset+1, n-offset): # payt attention to four corners, so contnue with offset + 1
            A[i][j] = vector[count]
            count += 1
        for j in range(n-offset-2, offset-1, -1): # pay attention to endpoint
            A[i][j] = vector[count]
            count += 1
        for i in range(n-offset-2, offset, -1): # pay attention to endpoint two overlapped !!!
            A[i][j] = vector[count]
            count += 1
        return A


if __name__ == '__main__':
    sl = Solution()
    n = 0
    print sl.generateMatrix(n)