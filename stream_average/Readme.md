Readme.md

concept
--------
- use max and min heaps 
- if one num is >= current Mean, put it into minHeap (since want know bottom of larger part) and vice versa
- balance heaps after each input and calculate new median


imple:
--------
- use heap: import heapq
- heapq.heapifty is in-place!!
- default heap is min-heap
- O(nlogn)
