# Given that integers are read from a data stream. Find median of elements read so far in efficient way
# use a max and a min heap
import heapq
class Solution:

    def __init__(self):
        self._hmax = []
        self._hmin = []
        heapq.heapify(self._hmax) # min-heap as default; heapify is in-place!!!!
        heapq.heapify(self._hmin) # max-heap
        self._cnt = 0
        self._curMean = 0

    def findMedian(self, num):
        if num >= self._curMean:
            heapq.heappush(self._hmin, num)
        else:
            heapq.heappush(self._hmax, -num)
        self._cnt += 1
        if self._cnt == 1:
            self._curMean = num
            return num
        # balance heap
        if self._cnt%2==0:
            while len(self._hmax) > len(self._hmin):
                heapq.heappush(self._hmin, -heapq.heappop(self._hmax)) # hmax to hmin
            while len(self._hmin) > len(self._hmax):
                heapq.heappush(self._hmax, -heapq.heappop(self._hmin)) # hmax to hmin
            self._curMean = (-self._hmax[0] + self._hmin[0])/2
            return self._curMean
        else:
            while len(self._hmax) > len(self._hmin) + 1:
                heapq.heappush(self._hmin, -heapq.heappop(self._hmax)) # hmax to hmin
            while len(self._hmin) > len(self._hmax) + 1:
                heapq.heappush(self._hmax, -heapq.heappop(self._hmin)) 
            self._curMean = -self._hmax[0] if len(self._hmax) > len(self._hmin) else self._hmin[0]
            return self._curMean

if __name__ == '__main__':
    sl = Solution()
    num = input("Enter a integer: ")
    while type(num) == int :
        print sl.findMedian(num)
        num = input("Enter a integer: ")
