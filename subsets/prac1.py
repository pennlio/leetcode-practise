class Solution:
    # @param S, a list of integer
    # @return a list of lists of integer
    def __init__(self):
        self._subsets = []

    def subsets(self, S):
        self._subsets.append([]) # at least has
        if S:
            S.sort()
            for x in S:
                newSubsets = []
                for y in self._subsets:
                    newSubsets.append(y + [x])
                self._subsets += newSubsets
        return self._subsets

    # def genSubsets(self, S):
if __name__ == '__main__':
    sl = Solution()
    num = [1,4,2]
    print sl.subsets(num)
