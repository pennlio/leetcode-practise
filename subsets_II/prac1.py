class Solution:
    # @param S, a list of integer
    # @return a list of lists of integer
    def __init__(self):
        self._subsets = []

    def subsetsWithDup(self, S):
        self._subsets.append([]) # at least has []
        if S:
            S.sort()
            for i in range(len(S)):
                newSubsets = []
                if i > 0 and S[i] == S[i-1]:
                    iterSet = self._subsets[-lstUptLen:] # if same value, only add S[i] to that length
                else:
                    iterSet = self._subsets
                lstUptLen = len(iterSet)  # record last updated length
                for y in iterSet:
                    newSubsets.append(y + [S[i]])
                self._subsets += newSubsets
        return self._subsets


if __name__ == '__main__':
    sl = Solution()
    num = [1,2,2,2,3]
    print sl.subsetsWithDup(num)