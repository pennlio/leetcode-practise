concept:
---------
- scan S 
- use queue to maintain the valid strings, hash table to check queue status in O(1)
- use flipped index to judge hash table, save hash table reset time


imple:
--------
- i+l2 > len(S): break


Corner Case:
------------
- dup in L
- noise is not as long as l2
- count overlapped strings: like aba as two [a,b]
- 'aaaaaaa' and [aa,aa,aa]

improve:
--------
- state machine