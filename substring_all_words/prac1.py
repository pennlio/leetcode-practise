class Solution:
    # @param S, a string
    # @param L, a list of string
    # @return a list of integer
    def __init__(self):
        self._hash_t = {}
        self._queue = []
    '''flipped version, think hard!'''
    def findSubstring(self, S, L):
        if not S or not L:
            return []
        l1 = len(L)
        l2 = len(L[0])
        # tn = l2*l1
        result = []
        flipped = 1 # postive label
        '''init hash set to store index'''
        for x in L:
            self._hash_t[x] = -1

        i = 0
        while i+l2 <= len(S):
            # pay attetion: here is i+l2 > len(S) not len(S)-1
            if S[i:i+l2] not in self._hash_t:
                i += l2
                continue
            if self.isInTheQueue(S[i:i+l2],flipped):
                self.clearQueue(S[i:i+l2],flipped)
            else:
                self._queue.append(S[i:i+l2])
                self._hash_t[S[i:i+l2]] = flipped # label
                if len(self._queue) == l1:
                    result.append(i-l2*(l1-1))
                    flipped *= -1  # flip label
                    self._queue = []
            i += l2
        return result

    def clearQueue(self, word, flipped):
        while self._queue[0] != word:
            self._hash_t[self._queue.pop(0)] = -flipped
        # self._hash_t[self._queue.pop(0)] = flipped 
        self._queue.pop(0) # pop last word
        self._queue.append(word) # push current word
        return 

    def isInTheQueue(self, word, flipped):
        if flipped == 1:
            return True if self._hash_t[word]== 1 else False
        else:
            return True if self._hash_t[word]== -1 else False

if __name__ == '__main__':
    sl = Solution()
    # S = "barfoothefoobarmanbarfoofoobar"
    # S = 'barfoobarlaaaddlaabaraddfoofoobarfoolaabarbarfoolaa'
    S = 'foobaraddlaa'
    # S = 'lingmindraboofooowingdingbarrwingmonkeypoundcake'
    L = ["foo", "bar", "laa"]
    # L = ["fooo","barr","wing","ding","wing"]
    print sl.findSubstring(S,L)
    


