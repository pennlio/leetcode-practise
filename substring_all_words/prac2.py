class Solution:
    # @param S, a string
    # @param L, a list of string
    # @return a list of integer
    def __init__(self):
        self._hash_t = {}
        self._queue = []
        '''wrong with test cases: after finding a False among L, how to increment the 
        index '''
    def findSubstring(self, S, L):
        if not S or not L:
            return []
        l1 = len(L)
        l2 = len(L[0])
        result = []
        self.resetHash(L)
        i = 0
        while i+l2 <= len(S):
            # pay attetion: here is i+l2 > len(S) not len(S)-1
            if S[i:i+l2] not in self._hash_t: 
                if len(self._queue) != 0:
                    self._queue = []
                    self.resetHash(L)
                # i += l2
                i += 1  # this status check every bit for begining
                continue
            if self.isInTheQueue(S[i:i+l2]):
                self.clearQueue(S[i:i+l2])
            else:
                self._queue.append(S[i:i+l2])
                self._hash_t[S[i:i+l2]] += 1 # label
                if len(self._queue) == l1:
                    result.append(i-l2*(l1-1))
                    # self.resetHash(L)
                    self._hash_t[self._queue.pop(0)] -= 1
                    # i += 1
                    i -= l1*l2
                    continue  # for corner case like 'aaaaaa' and [aa,aa,aa]
            i += l2
        return result

    def resetHash(self, L):
        for x in L:
            if x not in self._hash_t:
                self._hash_t[x] = -1
            else:
                self._hash_t[x] += -1
    def clearQueue(self, word):
        while self._queue[0] != word:
            self._hash_t[self._queue.pop(0)] -= 1
        # self._hash_t[self._queue.pop(0)] = 1
        self._queue.pop(0)
        self._queue.append(word)
        return 

    def isInTheQueue(self, word):
        return True if self._hash_t[word]== 0 else False

if __name__ == '__main__':
    sl = Solution()
    # S = "barfoothefoobarmanbarfoofoobar"
    # S = 'barfoobarlaalaa'
    # S = 'foolaaaddbarfoolaa'
    # L = ["foo", "bar", "laa"]
    # L = ['aa','aa','aa']
    # S = 'aaaaaaaa'
    # S = "aaa"
    # L = ["a","a"]
    # S = 'abababab'
    # L = ["ab","ab","ab"]
    S = 'abaababbaba'
    L = ["ab","ba","ab","ba"]
    # S = 'barfoobarlaalaabarfoofoobarfoolaabarfoolaa'
    # L = ["foo", "bar", "laa", 'laa']
    # S = 'lingmindraboofooowingdingbarrwingmonkeypoundcake'
    # L = ["fooo","barr","wing","ding","wing"]
    print sl.findSubstring(S,L)
    


