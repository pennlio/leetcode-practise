# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    # @param a ListNode
    # @return a ListNode
    def swapPairs(self, head):
        if head == None or head.next == None:
            return head
        p0 = head
        h1 = p1 = head.next
        p2 = p1.next
        p1.next = p0 # swap first pair
        while p2 != None:
            p3 = p2.next
            if p3 != None:
                p0.next = p3 # over link p0 to p3
                p1 = p3  # adv p1 by two places
                p0 = p2 # same as above
                p2 = p1.next # new p3
                p1.next = p0 # swap pair
            else: # end of list
                break
        p0.next = p2
        return h1# cannot return head.next

    def buildList(self, value_array):
        if len(value_array) == 0:
            return None
        else:
            value = value_array
            head = ListNode(value[0])
            latest_node = head
            for i in range(1, len(value)):
                new_node = ListNode(value[i])
                latest_node.next = new_node
                latest_node = new_node
        return head

    def printLinklist(self, headA):
        if not headA:
            return None
        p1 = headA
        while p1 != None:
            print p1.val
            p1 = p1.next

if __name__ == '__main__':
    sl = Solution()
    head =  sl.buildList([1,2,3,4,5,6,7,8])
    # head =  sl.buildList([1,2,3,4])
    head =  sl.buildList([1,2,3])
    # sl.printLinklist(head)
    h1 = sl.swapPairs(head)
    sl.printLinklist(h1)

