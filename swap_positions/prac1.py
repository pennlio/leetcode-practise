class Solution:
    # @ return int
    '''DP from link '''
    # http://www.mitbbs.com/article_t1/JobHunting/32876681_0_1.html
    def __init__(self):
        self._hash = {}
    def swap(self, order):
        if not order: return None
        pairSet = self.findPairElements(order)
        if not pairSet: return 0 # all singles
        return self.swapCount(0, order, pairSet)

    def swapCount(self, i, curOrder, pairSet): # recursively count swaps
        if not curOrder: return float('inf')
        if i > len(curOrder) - 1: 
            return 0
        if curOrder not in self._hash:
            if self.isPaired(curOrder,pairSet):
                self._hash[curOrder]=0
            elif curOrder[i] not in pairSet: # single
                self._hash[curOrder] = self.swapCount(i+1, curOrder, pairSet)
            else: # double
                if i < len(curOrder)-1 and curOrder[i] == curOrder[i+1]: # already paired
                    self._hash[curOrder] = self.swapCount(i+2, curOrder, pairSet)
                else: # need opearation
                    j = self.findPartner(i,curOrder)
                    self._hash[curOrder] =  1 + min(self.swapCount(i+1, self.swapPosition(curOrder, i,j-1), pairSet),self.swapCount(i+1, self.swapPosition(curOrder, i,j+1),pairSet), self.swapCount(i+2, self.swapPosition(curOrder, i+1,j),pairSet)) # select among the best next-step results
        return self._hash[curOrder] 

    def findPartner(self, i, order): # find partner of i, it no partner, return -10, which means there is not parter after i
        if i > len(order)-2:
            return -10
        for j in range(i+1,len(order)):
            if order[j] == order[i]:
                return j
        return - 10

    def swapPosition(self, order, i, j): # swap position of i and j in oder and return the neworder
        if i < 0 or i > len(order) -1 or j< 0  or j > len(order)-1:
            return ''
        elif i == j:return order
        else:
            order = list(order) # cannot change values in a string
            order[i],order[j] = order[j], order[i]
        return ''.join(order)

    def findPairElements(self,order): # findout pair elements from input
        cnt = [0]*26
        for x in order:
            cnt[ord(x)-ord('a')] += 1
        return [chr(ord('a') + i) for i,x in enumerate(cnt) if cnt[i]==2]


    def isPaired(self, order, pairSet): # chech a order, if it is paired, return True
        sets = set(pairSet)
        for i,x in enumerate(order):
            if x in sets:
                if order[i+1]!=order[i]:
                    return False
                sets.remove(x)
        return True if not sets else False

if __name__ == '__main__':
    sl = Solution()
    order = 'adooaeedff'
    print sl.swap(order)


