# Definition for a  binary tree node
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    # @param root, a tree node
    # @return a boolean
    def __init__(self):
        self._buf1 = []
        self._buf2 = []

    def isSymmetric(self, root):
        if not root:
            return True
        self._buf1.append(root)
        while max(self._buf1):  # when not all None
            for x in self._buf1:
                self.BFScheck(x)
            if not self.symArray(self._buf2):
                return False
            else:
                self._buf1 = self._buf2
                self._buf2 = []
        return True

    def BFScheck(self, root):
        if not root:
            self._buf2.append(None)
            return
        else:
            self._buf2.append(root.left)
            self._buf2.append(root.right)
        return

    def symArray(self, a):
        n = len(a)
        b = []  # value array
        for x in a:
            if x == None:
                b.append(x)
            else:
                b.append(x.val)
        i = 0
        j = n-1
        while i <= j:
            if b[i]!= b[j]:
                return False
            else:
                i += 1
                j -= 1
        return True

if __name__ == '__main__':
    sl = Solution()
    root = TreeNode(1)
    root.left = TreeNode(2)
    root.right = TreeNode(2)
    print sl.isSymmetric(root)

