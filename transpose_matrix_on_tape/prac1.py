# programming pearls: col 2.7 
# given a matrix, size known, represented on a tape (records in sequential order), 
# do the transpose on tape

# e.g: matrix = [[1,2,3],[4,5,6],[7,8,9]] on tape is mtape = [1,2,3,4,5,6,7,8,9]
# after transpose, mtr = [[1,4,7],[2,5,8],[3,6,9]], 
# on tape is mtrtape = [1,4,7,2,5,8,3,6,9]
from operator import itemgetter
class Solution():
    def transpose(self, matrix, nRw, nCl):
        preMatrix = self.prependIndex(matrix,nRw,nCl)   # Hint: see the space between records,
        # so can add (row, col) before each record
        preMatrix = sorted(preMatrix, key=itemgetter(1)) # So can sort by #col and #row
        return self.delIndex(preMatrix,nRw,nCl)

    def delIndex(self, preMatrix, nRw, nCl):
        return map(lambda x: x[2], preMatrix)


    def prependIndex(self, matrix, nRw, nCl):
        for i in range(nRw):
            for j in range(nCl):
                matrix[i*nCl+j] = (i,j,matrix[i*nCl+j])
        return matrix

def main():

    sl = Solution()
    matrix = [1,2,3,4,5,6,7,8,9]
    nRw = 3
    nCl = 3
    print sl.transpose(matrix,nRw,nCl)


if __name__ == '__main__':
    main()