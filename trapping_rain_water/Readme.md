Readme.md

concept:
--------
- like the `calculation shape area`, using kind of DP
- check one-by-one the volume of trapping water of each node
- scan left to right and inverse indep. so that recursion may not have deadlock
imple:
------
- O(n)
- too much memory (rely on recursion)

2nd concept:
---------
- note the connected regions share the same water amount
- use 'bar method': find tallest bar and filled areas between


imple:
-------
- Use heap

corner case:
-------
- if len(A) == 1


3 concept:
-----------
- note water level can only rise to wtlevel iff there are two bars at both left and right ends higher than wtlevel; 
- water level only increase as bar indices move from sides to middle
- O(n), one pass solution

summary:
----------
- find out the killer rule: water level can only rise to wtlevel iff there are two bars at both sides that are higher than wtlevel