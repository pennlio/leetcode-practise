import heapq
class Solution:
    '''use heap, but leetcode does not support'''
    # @param A, a list of integers
    # @return an integer
    def trap(self, A):
        if not A: return 0
        barHeap = []
        heapq.heapify(barHeap)
        barHeap = self.buildBarHeap(A, barHeap)
        return self.fillWater(A, barHeap)

    def buildBarHeap(self, A, barHeap):
        n = len(A)
        if n == 1: 
            heapq.heappush(barHeap, (-A[0],0))
        else:
            for i in range(n):
                if i > 0 and i < n-1:
                    if A[i] > A[i-1] and A[i] > A[i+1]:
                        heapq.heappush(barHeap, (-A[i],i))  # store minus to be max-heap\
                elif i == 0:
                    if A[i] > A[i+1]:
                        heapq.heappush(barHeap, (-A[i],i))
                elif i == n-1:
                    if A[i] > A[i-1]:
                        heapq.heappush(barHeap, (-A[i],i))
        return barHeap

    def fillWater(self, A, barHeap):
        filled = 0
        minFilledIndex = len(A)-1
        maxFilledIndex = 0
        while barHeap:
            (barLevel, index) = heapq.heappop(barHeap)
            if index > maxFilledIndex:
                if minFilledIndex <= maxFilledIndex: # skip first extraction
                    for i in range(maxFilledIndex+1, index+1):
                        filled += -barLevel - A[i]
                        # print -barLevel-A[i],i
                maxFilledIndex = index
            if index < minFilledIndex:
                if minFilledIndex <= maxFilledIndex: # skip first extraction 
                    for i in range(index, minFilledIndex):
                        filled += -barLevel - A[i]
                        # print -barLevel-A[i],i
                minFilledIndex = index
        return filled
            

def main():
    sl = Solution()
    A = [0,1,0,2,1,0,1,3,2,1,2,1]
    A = [3,2,9]
    print sl.trap(A)

if __name__ == '__main__':
    main()




