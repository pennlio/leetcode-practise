class Solution:
    # @param A, a list of integers
    # @return an integer
    def trap(self, A):
        if not A: return 0
        low, high = 0, len(A)-1
        wtlevel, rain = 0,0  # init water level to 0, rain to save trapped water so far
        while low <= high:
            if A[low] <= wtlevel:
                rain += wtlevel - A[low]
                low += 1
            elif A[high] <= wtlevel:
                rain += wtlevel - A[high]
                high -= 1
            else: # rise water level since two bars are both higher than current water level
                wtlevel = min(A[low],A[high])
        return rain
                