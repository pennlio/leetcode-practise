class Solution:
    # @param triangle, a list of lists of integers
    # @return an integer
    def __init__(self):
        self._hashST = {}
    def maximumTotal(self, triangle):
        if not triangle:
            return 0
        n = len(triangle) # height of triangle
        return self.subTotal(triangle,0,0)

    def subTotal(self, triangle, nLev, nCol):
        n = len(triangle)
        if nLev == n-1: # last level
            if not triangle[nLev]: # if input [[]]
                return 0
            else:
                return triangle[nLev][nCol]
        else:
            if (nLev, nCol) not in self._hashST:
                a = triangle[nLev][nCol]+self.subTotal(triangle, nLev+1, nCol)
                b = triangle[nLev][nCol]+self.subTotal(triangle, nLev+1, nCol+1)
                self._hashST[(nLev, nCol)] = max(a,b)
            return self._hashST[(nLev, nCol)]

if __name__ == '__main__':
    sl = Solution()
    infile = open('triangle.txt','r')
    triangle = []
    for line in infile:
        nums = line.split()
        nums = [int(x) for x in nums]
        triangle.append(nums)
    print sl.maximumTotal(triangle)
