Two Sum
======
Concept:
---------
- `main1.py`: hash table to find one by one, O(n)
- `main2.py`: look for `n` and `target-n` one by one, O(n^2)
- `main3.py`: sort and then check from both sides,O(nlogn)

Corner case:
----------
- duplicates in array
- `main1` cannot support dup; `main2` and `3`: locate first, remove, locate second
