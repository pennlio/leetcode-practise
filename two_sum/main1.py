#!\bin\python
''' two sum @ 12/23/2014'''
class Solution:
    def twoSum(self, num, target):
        hasht = {}
        for n in num:
            hasht[n] = 1

        for n in num:
            if ((target-n) in hasht):
                num1 = n
                num2 = target-n
                index1 = num.index(num1)
                index2 = num.index(num2)
                if index2 == index1: # cannot be sum of the same number
                    continue
                else:
                    break
        # index1 = num.index(num1)
        # index2 = num.index(num2)
        if index1 > index2:
            return (index2+1, index1+1)
        else:
            return (index1+1, index2+1)

## bug: cannot solve following case, since only store per value once

if __name__ == '__main__':
    sl = Solution()
    # num=[2, 9, 11, 15,7, 5]
    num = [3,2,5,3]
    target= 6
    (index1, index2) = sl.twoSum(num, target)
    print index1, index2