''' @ 12/23/2014 '''
# bug : time complexity
class Solution:
    def twoSum(self, num, target):
        # num_sort = sort(num)
        for n in num:
            index1 = num.index(n)
            temp = list(num) # copy list
            del temp[index1]  # remove avoid dup
            if (target-n) in temp:
                index2 = temp.index(target-n) + 1
                break
            else:
                continue
        if index1 > index2:
            return(index2, index1)
        else:
            return (index1, index2)
    
    # def binFind(num_sort, n, target):
    #     low = num_sort.index(n) + 1
    #     high = num_sort.index(target)
    #     mid = (low+high)/2
    #     if num_sort

if __name__ == '__main__':
    sl = Solution()
    # num=[2, 9, 11, 15,7, 5]
    num = [3,2,5,9,3]
    target= 6
    (index1, index2) = sl.twoSum(num, target)
    print index1, index2