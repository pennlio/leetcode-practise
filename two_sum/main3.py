class Solution:
    def twoSum(self, num, target):
        numO = list(num)
        num.sort()
        n = len(num)
        p1 = 0
        p2 = n-1
        while (p1 != p2):
            if num[p1] + num[p2] == target:
                break
            elif num[p1] + num[p2] > target:
                p2 -= 1
                continue
            else:
                p1 += 1
                continue

        if num[p1] != num[p2]:
            index1 = numO.index(num[p1])
            index2 = numO.index(num[p2])
        else: # if dup, then remove first before locate second
            index1 = numO.index(num[p1])
            del numO[index1]
            index2 = numO.index(num[p2])+1
        if index1 > index2:
            return (index2+1, index1+1)
        else:
            return (index1+1, index2+1)


if __name__ == '__main__':
    sl = Solution()
    num=[2, 9, 11, 15,7, 5]
    # num = [3,2,4]
    # num = [3,2,5,9,3]
    target= 7
    (index1, index2) = sl.twoSum(num, target)
    print index1, index2