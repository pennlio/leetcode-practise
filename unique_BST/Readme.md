concept:
----------
1. discuss cases: h = 1, n or mid_elements
2. recursion: T(n) = T(n-1) + O(n): O(n^2)

improve:
-------
1. use DP to improve time complexity
2. hashtable: only tuple is hashable, else is not
