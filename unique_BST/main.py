'''unique BST/2014/12/28'''

class Solution:
    # @return an integer
    # use recursion: base case
    def __init__(self):
        self._hasht = {}  # hash table to record results
    def numTrees(self, n): 
        if n == 0:
            return 0
        elif n == 1:
            return 1
        else:
            k = self.calKn(n)
            return 2*self.numTrees(n-1) + k

    def calKn(self, n):
        sum = 0
        for x in range(1,n):
            y = n-1-x
            if x > y:
                index = (y,x)
            else:
                index = (x,y)
            if index not in self._hasht:
                s = self.numTrees(x) * self.numTrees(y)
                self._hasht[index] = s
            else:
                s = self._hasht[index]
            sum += s
        return sum


if __name__ == '__main__':
    n = 5
    sl = Solution()
    print sl.numTrees(n)
