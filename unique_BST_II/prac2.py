# Definition for a  binary tree node
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None
class Solution:
    # @return a list of tree node
    def generateTrees(self, n): # order = 0 dec; order = 1, increase
        if n == 0: return [None]
        return self.DPGenerate(1, n)

    def DPGenerate(self, min, max):
        # if n == 0: return [None]
        if min > max: return [None]
        if min == max: return [TreeNode(max)]
        result = []
        for k in range(min,max+1):
            for x in self.DPGenerate(min, k-1):
                for y in self.DPGenerate(k+1, max):
                    root = TreeNode(k)
                    root.left = x
                    root.right = y
                    result.append(root)
        return result
            
if __name__ == '__main__':
    sl  = Solution()
    n = 3
    roots = sl.generateTrees(n)

