class Solution:
    # @return a list of tree node
    def generateTrees(self, n):
        self.cache = {}
        return self.generate(1, n)

    def generate(self, min, max):
        if (min, max) not in self.cache.keys():
            if max < min:
                self.cache[(min, max)] = [None]
                return [None]
            if max == min:
                self.cache[(min, max)] = [TreeNode(max)]
                return [TreeNode(max)]
            nodes = []
            for i in range(min, max+1):
                for left in self.generate(min, i-1):
                    for right in self.generate(i+1, max):
                        root = TreeNode(i)
                        root.left = left
                        root.right = right
                        nodes.append(root)
            self.cache[(min, max)] = nodes
            return nodes
        else:
            return self.cache[(min, max)]