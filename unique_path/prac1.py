class Solution:
    # @return an integer
    def uniquePaths(self, m, n):
        if m <= 1 and n <= 1:
            return 1
        else:
            total = m+n-2
            a = min(m,n)-1
            return self.calFactorial(total)/self.calFactorial(total-a)/self.calFactorial(a)

    def calFactorial(self, n):
        if n ==0 or n == 1:
            return 1
        else:
            return n*self.calFactorial(n-1)

if __name__ == '__main__':
    sl = Solution()
    m = 1
    n = 1
    print sl.uniquePaths(m,n)

