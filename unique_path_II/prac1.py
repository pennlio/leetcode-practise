class Solution:
    # @param obstacleGrid, a list of lists of integers
    # @return an integer
    def __init__(self):
        self._hash = {}

    def uniquePathsWithObstacles(self, obstacleGrid):
        if not obstacleGrid:
            return 0
        if not obstacleGrid[0]:
            return 0
        return self.walkThru(obstacleGrid,0,0)
        

    def walkThru(self, obstacleGrid, i, j):
        if (i,j) not in self._hash:
            nRow = len(obstacleGrid)
            nCol = len(obstacleGrid[0])
            if obstacleGrid[i][j] == 1:
                self._hash[(i,j)] = 0
                return self._hash[(i,j)]
            if i > nRow - 1 or j > nCol - 1:
                self._hash[(i,j)] = 0
                return self._hash[(i,j)]
            if i == nRow -1 and j == nCol - 1:
                self._hash[(i,j)] = 1
                return self._hash[(i,j)]
            if i == nRow - 1:
                self._hash[(i,j)] = self.walkThru(obstacleGrid, i, j+1)
            elif j == nCol - 1:
                self._hash[(i,j)] = self.walkThru(obstacleGrid, i+1, j)
            else:
                self._hash[(i,j)] = self.walkThru(obstacleGrid, i+1, j) + self.walkThru(obstacleGrid, i, j+1) 
        return self._hash[(i,j)]


if __name__ == '__main__':
    sl = Solution()
    # obstacleGrid = [[0,1,0],[0,0,0],[0,0,0]]
    # obstacleGrid = [[0,0,0,0,0]]
    obstacleGrid = [[0,0,0],[0,0,0],[0,0,0]]
    print sl.uniquePathsWithObstacles(obstacleGrid)


