# Definition for a  binary tree node
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

'''this recursive comparing sub-structure not valid: e.g {10, 8, 15, 7, 12, #,#}'''
class Solution:
    # @param root, a tree node
    # @return a boolean
    def isValidBST(self, root):
        if not root:
            return True
        if root.left != None:
            if root.left.val >= root.val:
                return False
            vleft = self.isValidBST(root.left)
        else:
            vleft = True
        if root.right != None:
            if root.right.val <= root.val:
                return False
            vright = self.isValidBST(root.right)
        else:
            vright = True
        if vleft and vright:  # both true return true
            return True
        else:
            return False

if __name__ == '__main__':
    sl = Solution()
    root = TreeNode(9)
    root.left = TreeNode(6)
    root.right = TreeNode(10)
    # root = None
    print sl.isValidBST(root)