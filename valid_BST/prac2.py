# Definition for a  binary tree node
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    # @param root, a tree node
    # @return a boolean
    def __init__(self):
        self._lastnum = float('-inf')

    def isValidBST(self, root):
        if not root:
            return True
        lv = self.isValidBST(root.left)
        if root.val<= self._lastnum:
            return False
        else:
            self._lastnum = root.val
        rv = self.isValidBST(root.right)
        if lv and rv:
            return True
        else:
            return False


if __name__ == '__main__':
    sl = Solution()
    root = TreeNode(10)
    root.left = TreeNode(6)
    root.left.left = TreeNode(3)
    root.left.right = TreeNode(9)
    # root.left = TreeNode(6)
    root.right = TreeNode(15)
    root = None
    print sl.isValidBST(root)
    # def inorderTraverse(self, root):
