class Solution:
    # @param s, a string
    # @return a boolean
    def isPalindrome(self, s):
        if not s:
            return True
        chars = ""
        oA = ord('A')
        oZ = ord('Z')
        o0 = ord('0')
        for x in s:
            if ord(x) in range(oA, oZ+1):
                x = chr(ord(x)+32)
                chars += x
            elif ord(x) in range(oA+32, oZ+33) or ord(x) in range(o0, o0+10):
                chars += x
        y = chars[::-1]
        return True if chars==y else False
        # s.replace(" ","")

if __name__ == '__main__':
    sl = Solution()
    s = "2sAa.as1"
    s = '2a2'
    # s = 'sd:i,ds'
    # s = 'adi,dd'
    # s = 'Aa'
    print sl.isPalindrome(s)