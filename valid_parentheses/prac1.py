class Solution:
    # @return a boolean
    def isValid(self, s):
        if not s: # no input true
            return True
        # var to record left parentheses
        rdStack = 0
        sqStack = 0
        bgStack = 0
        lastactive = []
        for x in s:
            if x == '(':
                rdStack += 1
                lastactive.append(1)
            if x == '[':
                sqStack += 1
                lastactive.append(2)
            if x == '{':
                bgStack += 1
                lastactive.append(3)
            if x == ')':
                if rdStack == 0 or lastactive[-1] != 1:
                    return False
                else:
                    rdStack -= 1
                    lastactive.pop()
            if x == ']':
                if sqStack == 0 or lastactive[-1] != 2:
                    return False
                else:
                    sqStack -= 1
                    lastactive.pop()
            if x == '}':
                #  if lastactive[-1] != 3 or bgStack == 0: must put bgStack == 0 at front
                if bgStack == 0 or lastactive[-1] != 3:
                    return False
                else:
                    bgStack -= 1
                    lastactive.pop()
        return True if len(lastactive) == 0 else False
if __name__ == '__main__':
    sl = Solution()
    s = '{}}'
    print sl.isValid(s)



