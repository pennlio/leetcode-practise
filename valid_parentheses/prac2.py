class Solution:
    # @return a boolean
    def isValid(self, s):
        if not s:
            return True
        # stack1 = stack2 = stack3 = [] # init stacks
        stack = []
        for i in range(len(s)):
            if s[i] == '(': 
                stack.append(s[i])
            elif s[i] == '[':
                stack.append(s[i])
            elif s[i] == '{':
                stack.append(s[i])
            elif s[i] == ')':
                if stack == [] or stack[-1] != '(':
                    return False
                else:
                    stack.pop()
            elif s[i] == ']':
                if stack == [] or stack[-1] != '[':
                    return False
                else:
                    stack.pop()
            else:
                if stack == [] or stack[-1] != '{':
                    return False
                else:
                    stack.pop()
        if stack:
            return False
        else:
            return True
                
if __name__ == '__main__':
    sl = Solution()
    s = '()[]{}'
    # s = '(())'
    print sl.isValid(s)