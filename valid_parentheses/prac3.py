class Solution:
    # @return a boolean
    def isValid(self, s):
        if not s: return True
        stack = []
        for x in s:
            if x == '{' or x == '(' or x == '[':
                stack.append(x)
            else:
                if not stack: 
                    return False
                if (x == '}' and stack[-1] == '{') or (x == ')' and stack[-1] == '(') or (x == ']' and stack [-1] == '['):
                    stack.pop()
                else:
                    return False
        return False if stack else True