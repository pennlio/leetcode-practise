class Solution:
    # @param board, a 9x9 2D array
    # @return a boolean
    def isValidSudoku(self, board):
        if self.verifyRow(board) and self.verifyCol(board) and self.verifyBlob(board):
            return True
        else:
            return False

    def verifyRow(self, board):
        for i in range(len(board)):
            hasht = {}
            for j in range(len(board[0])):
                if board[i][j] == '.':
                    continue
                else:
                    if board[i][j] not in hasht:
                        hasht[board[i][j]] = True
                    else:
                        return False
        return True

    def verifyCol(self, board):
        for i in range(len(board[0])):
            hasht = {}
            for j in range(len(board)):
                if board[j][i] == '.':
                    continue
                else:
                    if board[j][i] not in hasht:
                        hasht[board[j][i]] = True
                    else:
                        return False
        return True

    def verifyBlob(self, board):
        centers = [[1,1],[4,4],[7,7],[1,4],[1,7],[4,1],[4,7],[7,1],[7,4]]
        for x in centers:
            hasht = {}
            for i in range(-1,2):
                for j in range(-1,2):
                    if board[x[0]+i][x[1]+j] == '.':
                        continue
                    if board[x[0]+i][x[1]+j] not in hasht:
                        hasht[board[x[0]+i][x[1]+j]] = True
                    else:
                        return False
        return True

if __name__ == '__main__':
    sl = Solution()
    # board = [range(1,10) for i in range(0,9)]
    board = ["......5..",".........",".........","93..2.4..","..7...3..",".........","...34....",".....3...",".....52.."]
    print sl.isValidSudoku(board)


