class Solution:
    # @param s, a string
    # @param dict, a set of string
    # @return a boolean
    def __init__(self):
        self._hash = {}
        self._dict = {}
    def wordBreak(self, s, dict):
        if not s or not dict:
            return False
        n = len(s)
        for x in dict:
            self._dict[x] = True
        return self.DPsearch(s, 0, n-1)

    def DPsearch(self, s, low, high):
        if low > high:
            return True
        if (low,high) in self._hash:
            return self._hash[(low,high)]
        sect = start = low
        while sect <= high:
            if s[start:sect+1] in self._dict:
                if self.DPsearch(s,sect+1,high):
                    self._hash[(low,high)] = True
                    return True
            sect += 1
        self._hash[(low,high)] = False
        return False

def main():
    sl = Solution()
    s = "leetcodeadrray"
    dict = ['leet','code','ad','ray','ad']
    print sl.wordBreak(s, dict)

if __name__ == '__main__':
    main()
