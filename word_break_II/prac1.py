class Solution:
    # @param s, a string
    # @param dict, a set of string
    # @return a list of strings
    '''TLE when baaaaaaaaaaaaaaaaaaaaaaaaaaaa case'''
    def __init__(self):
        self._dict = {}
        self._hash = {}
    def wordBreak(self, s, dict):
        if not s or not dict:
            return []
        n = len(s)
        for x in dict:
            self._dict[x] = True
        result = self.buildSentence(s, 0, n-1)
        return map(lambda x: ' '.join(x), result)

    def buildSentence(self, s, low, high): # return list of lists
        if low > high:
            return [[]] # final return, should be [[]], o/w no result will be returned
        if (low, high) not in self._hash:
            result = []
            sep = high # backward check
            while sep >= low:
                if s[low:sep+1] in self._dict:
                    for y in self.buildSentence(s, sep+1, high):
                        result.append([s[low:sep+1]]+y)
                sep -= 1
            self._hash[(low, high)] = result        
        return self._hash[(low,high)]
def main():
    sl = Solution()
    s = 'catsanddogdd'
    s = 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'
    # dict = ['cat','cats','and','sand','dog','d','dd']
    dict = ["a","aa","aaa","aaaa","aaaaa","aaaaaa","aaaaaaa","aaaaaaaa","aaaaaaaaa","aaaaaaaaaa"]
    print sl.wordBreak(s,dict)

if __name__ == '__main__':
    main()
