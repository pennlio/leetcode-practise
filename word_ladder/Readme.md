Readme.md

concept:
-------
- BFS among starting words and dictionary


imple:
------
- BFS: an queue and a hashtable is OK
- put a None into queue to label end of that level
- !!! to find match word, two ways: 
    - one is to scan the canditate set for start, which takes O(n^2) time; 
    - another is alterante start word and locate it in candidate set, taking O(n*26*len(start))