class Solution:
    # @param start, a string
    # @param end, a string
    # @param dict, a set of string
    # @return a list of lists of string
    def findLadders(self, start, end, dict):
        if start == end:
            return [[start]]
        letters = 'abcdefghijklmnopqrstuvwxyz'
        # usedWords = set() # define set
        
        candidateWords = [start, None]
        # usedWords.add(start)
        self._parents_tracker = {}
        # leveltracker = 
        result = []
        backTrackHash = {}

        j = 0 # scan each word while keeping records
        while True:
            x = candidateWords[j]
            print candidateWords, x
            # print result
            if x == None:
                if result:
                    break
                elif j == len(candidateWords)-1:# end of the queue
                    return [] # no such seq
                candidateWords.append(None)
            else:
                for i in range(len(x)):
                    for c in letters:
                        matchWord = x[:i] + c + x[i+1:] # generate matchWord
                        if matchWord == end: # find
                            if matchWord in backTrackHash: # add backtrack
                                backTrackHash[matchWord].append(x)
                            else:
                                backTrackHash[matchWord] = [x]
                            result.append(matchWord)
                            continue # 
                        if matchWord in dict and matchWord not in usedWords:
                            candidateWords.append(matchWord)
                            usedWords.add(matchWord)
                            if matchWord in backTrackHash : # add backtrack
                                backTrackHash[matchWord].append(x) 
                            else:
                                backTrackHash[matchWord] = [x]
            j += 1
        return self.backTrack(start, end, backTrackHash)
    
    def backTrack(self, start, end, backTrackHash):
        result = [[end]]
        while True:
            newresult = []
            for y in result:
                for z in backTrackHash[y[-1]]:
                    nn = list(y)
                    nn.append(z)
                    newresult.append(nn)
                result = newresult
            if z == start:
                for x in result:
                    x.reverse() # reverse
                return result

if __name__ == '__main__':
    sl = Solution()
    
    start = 'a'
    end = 'c'
    dict = ['a','b','c']
    end = "cog"
    start = "hit"
    dict = ["hot","dot","dog","lot","log",'hig','']
    start = "red"
    end = "tax"
    dict = ("ted","tex","red","tax","tad","den","rex","pee")

    print sl.findLadders(start, end, dict)
