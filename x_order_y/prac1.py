class Solution:
    def xOrderY(self, x, y):
        x, y = float(x), float(y)
        if y == 0: return 1
        if y == 1: return x
        if x == 0:
            return None if y < 0 else 0
        if y < 0:
            minus = 1
            y = -y
        else:
            minus = 0
        res = self.xOrderY(x, y//2)* self.xOrderY(x,y//2) * self.xOrderY(x,y%2)
        return res if not minus else 1/res

if __name__ == '__main__':
    sl = Solution()
    x = 5
    y = -2
    print sl.xOrderY(x,y)

