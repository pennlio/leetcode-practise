class Solution:
    # @return a string
    def convert(self, s, nRows):
        if not s or nRows == 0:
            return ''
        if nRows == 1:
            return s
        n = len(s)
        if nRows >= n:
            return s
        output = ''
        for i in range(nRows):
            thisRow = ''
            if i == (nRows-1)/2:
                for j in range(i,n, (nRows-1)/2+1):
                    thisRow += s[j]
                # jump by (nRows-1)/2 + 1
            else:
                for j in range(i,n,nRows+1):
                    thisRow += s[j]
                # jump by nRows
            output += thisRow
        return output

'''timeout!! does not understand the questions!!!'''

if __name__ == '__main__':
    sl = Solution()
    nRows = 22
    s = 'ABCDEFGHIJKLMN'
    print sl.convert(s,nRows)


        