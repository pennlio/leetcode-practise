class Solution:
    # @return a string
    def convert(self, s, nRows):
        n = len(s)
        if not s or nRows < 0:
            return ''
        if nRows == 1 or nRows > n:
            return s
        j = 1
        rising = True
        rowOutput = {}  # store per-row result after convert
        for i in range(n):
            if j not in rowOutput:
                rowOutput[j] = s[i]
            else:
                rowOutput[j] += s[i]
            if rising:
                if j == nRows:
                    rising = False
                    j -= 1
                else:
                    j += 1
            else:
                if j == 1:
                    rising = True
                    j += 1
                else:
                    j -= 1
        # combine result
        result = ''
        for i in range(1,nRows+1):
            result += rowOutput[i]
        return result

if __name__ == '__main__':
    sl = Solution()
    nRows = 2
    s = 'ABCDEFGH'
    print sl.convert(s,nRows)

